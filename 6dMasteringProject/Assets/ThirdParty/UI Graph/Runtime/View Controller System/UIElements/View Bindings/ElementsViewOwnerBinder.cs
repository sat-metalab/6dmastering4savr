﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Reflection;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public class ElementsViewOwnerBinder : IViewBindable
    {
        void IViewBindable.Bind(Object obj)
        {
            var owner = obj as ElementsController;
            if (CanBindToOwner(owner, out Error error) == false)
            {
                error.LogWithPrefix("Cannot bind to object.");
                return;
            }

            BindReferencesToOwner(owner);
        }

        public bool CanBindFieldToElement(FieldInfo field, VisualElement element, out Error error)
        {
            var identifier = field.Name;
            if (element == null)
            {
                error = new Error(string.Format("View reference '{0}' was not found in the view and will not be bound.", identifier), Error.Severity.Warning);
                return false;
            }

            System.Type elementType = element.GetType();
            if (field.FieldType.IsAssignableFrom(elementType) == false)
            {

                error = new Error(string.Format("View reference '{0}' has a different type to its element and will not be bound.", identifier), Error.Severity.Warning);
                return false;
            }

            error = null;
            return true;
        }

        private void BindReferencesToOwner(ElementsController owner)
        {
            var view = owner.View;
            FieldInfo[] viewReferenceFields = ViewReferenceAttributeCollector.CollectViewReferenceAttributedFields(owner);
            foreach (FieldInfo viewReferenceField in viewReferenceFields)
            {
                string identifier = viewReferenceField.Name;
                VisualElement target = view.Q(identifier);
                if (CanBindFieldToElement(viewReferenceField, target, out Error error))
                {
                    viewReferenceField.SetValue(owner, target);
                }
                else
                {
                    error.Log();
                }
            }
        }

        private bool CanBindToOwner(ElementsController owner, out Error error)
        {
            if (owner == null)
            {
                error = new Error("Object passed to bind was not an elements controller.");
                return false;
            }

            if (owner.ViewIsLoaded == false)
            {
                error = new Error("The view has not been loaded.");
                return false;
            }

            error = null;
            return true;
        }
    }
}
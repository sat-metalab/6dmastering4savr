﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    // This class exists in the runtime assembly in order to be stored in the view resource object. However, its data only exists in the editor assembly.
    [System.Serializable]
    public class ElementsViewOwnerBindingsEditorData
    {
#if UNITY_EDITOR
        public ElementsViewReferenceData[] viewReferenceDatas;

        public OwnerData ownerData;

        [System.Serializable]
        public class OwnerData
        {
            public ElementsController asset;

            public OwnerData(ElementsController asset)
            {
                this.asset = asset;
            }
        }
#endif
    }
}
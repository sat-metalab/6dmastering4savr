﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Linq;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph
{
    public static class TemplateContainerExtensions
    {
        public static VisualElement BreakOut(this TemplateContainer template)
        {
            VisualElement root;
            if (template.childCount == 1)
            {
                var parent = template.parent;

                root = template.Children().First();
                root.RemoveFromHierarchy();
                template.RemoveFromHierarchy();

                if (parent != null)
                {
                    parent.Add(root);
                }
            }
            else
            {
                Error.Log("Cannot break out of a template with multiple child elements.");
                root = template;
            }

            return root;
        }
    }
}
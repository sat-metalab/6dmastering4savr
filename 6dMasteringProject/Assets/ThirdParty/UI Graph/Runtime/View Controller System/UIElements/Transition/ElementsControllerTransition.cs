﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// When an Elements Controller transition is to be performed, UI Graph creates a transition object to describe the transition. This object is passed to methods that require it, such as <see cref="ViewController{TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph}.PrepareForGraphTransition(GraphTransition{TViewController})"/>, providing you with the relevant transition information.
    /// </summary>
    public class ElementsControllerTransition : ViewControllerTransition<ElementsController, ElementsControllerTransition, ElementsControllerTransitionContext, ElementsControllerTransitionAnimatorProvider, IElementsControllerTransitionProgressProvider, ElementsControllerTransitionAnimationDriver, ElementsControllerTransitionProgressProvider>
    {
        // Parameterless constructor is used by the generic view controller.
        public ElementsControllerTransition() { }
        public ElementsControllerTransition(ViewControllerTransitionIdentifier identifier, ElementsController toViewController, ElementsController fromViewController, ElementsController ownerViewController, bool animated, bool interactive = false, Stack<ElementsController> intermediaryViewControllers = null)
        {
            Initialize(identifier, toViewController, fromViewController, ownerViewController, animated, interactive, intermediaryViewControllers);
        }

        public override void Initialize(ViewControllerTransitionIdentifier identifier, ElementsController toViewController, ElementsController fromViewController, ElementsController ownerViewController, bool animated, bool interactive = false, Stack<ElementsController> intermediaryViewControllers = null)
        {
            context = new ElementsControllerTransitionContext(identifier, toViewController, fromViewController, ownerViewController, animated, interactive, intermediaryViewControllers);
        }

        protected override ElementsControllerTransitionAnimatorProvider TransitionAnimatorProvider
        {
            get
            {
                return context.OwnerViewController.transitionAnimatorProvider;
            }
        }

        protected override IElementsControllerTransitionProgressProvider InteractiveTransitionProgressProvider
        {
            get
            {
                return context.OwnerViewController.interactiveTransitionProgressProvider;
            }
        }
    }
}
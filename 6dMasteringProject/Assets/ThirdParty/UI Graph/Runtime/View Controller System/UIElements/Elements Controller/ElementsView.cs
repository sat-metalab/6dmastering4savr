﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph
{
    public class ElementsView : VisualElement, IView<ElementsView, ElementsWindow>, ITransform
    {
        protected const string FitToParentUssClass = "fit-to-parent";

        bool IView<ElementsView, ElementsWindow>.Visible
        {
            get
            {
                return visible;
            }

            set
            {
                visible = value;
            }
        }

        void IView<ElementsView, ElementsWindow>.Add(ElementsView child)
        {
            Add(child);
        }

        Vector3 ITransform.Position
        {
            get
            {
                return transform.position;
            }

            set
            {
                transform.position = value;
            }
        }

        Quaternion ITransform.Rotation
        {
            get
            {
                return transform.rotation;
            }

            set
            {
                transform.rotation = value;
            }
        }

        Vector3 ITransform.Scale
        {
            get
            {
                return transform.scale;
            }

            set
            {
                transform.scale = value;
            }
        }

        Matrix4x4 ITransform.Matrix
        {
            get
            {
                return Transform.Matrix;
            }
        }

        public ElementsWindow Window
        {
            get
            {
                return GetFirstAncestorOfType<ElementsWindow>();
            }
        }

        public float Alpha
        {
            get
            {
                StyleFloat opacity = style.opacity;
                return opacity.value;
            }

            set
            {
                style.opacity = value;
            }
        }

        public bool Interactable
        {
            get
            {
                return (pickingMode == PickingMode.Position);
            }

            set
            {
                pickingMode = (value) ? PickingMode.Position : PickingMode.Ignore;
            }
        }

        public ITransform Transform
        {
            get
            {
                return this as ITransform;
            }

            set
            {
                ITransform transform = Transform;
                transform.Position = value.Position;
                transform.Rotation = value.Rotation;
                transform.Scale = value.Scale;
            }
        }

        public Rect Rect
        {
            get
            {
                return contentRect;
            }
        }

        public void FitToParent()
        {
            if (ClassListContains(FitToParentUssClass) == false)
            {
                AddToClassList(FitToParentUssClass);
            }
        }

        public void Unload()
        {
            RemoveFromHierarchy();
        }

        public class Factory : UxmlFactory<ElementsView> { }
    }
}
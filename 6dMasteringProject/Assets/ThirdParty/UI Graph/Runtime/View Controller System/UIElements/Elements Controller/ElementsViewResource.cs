﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.UIElements;

namespace Pelican7.UIGraph
{
    public partial class ElementsViewResource : ViewResource<ElementsView, ElementsWindow>
    {
        public VisualTreeAsset uxml;
        public StyleSheet styleSheet;

        public override ElementsView Load(out IViewBindable viewOwnerBindings)
        {
            VisualElement root = CloneUxmlWithStyleSheet(uxml, styleSheet);
            var view = root as ElementsView;
            if (view == null)
            {
                view = new ElementsView();
                view.Add(root);
                Error.Log("Root element of UXML document was not an ElementsView. One has been created.", Error.Severity.Warning);
            }

            viewOwnerBindings = new ElementsViewOwnerBinder() as IViewBindable;
            return view as ElementsView;
        }

        private VisualElement CloneUxmlWithStyleSheet(VisualTreeAsset uxml, StyleSheet styleSheet = null, bool breakOutOfTemplate = true)
        {
            VisualElement root;

            TemplateContainer template = uxml.CloneTree();
            if (breakOutOfTemplate)
            {
                root = template.BreakOut();
            }
            else
            {
                root = template;
            }

            if (styleSheet != null)
            {
                root.styleSheets.Add(styleSheet);
            }

            return root;
        }
    }

    public partial class ElementsViewResource : ViewResource<ElementsView, ElementsWindow>
    {
#if UNITY_EDITOR
        // Owner bindings are not required at runtime for elements views. Binding is done using UQuery. They exist only for in-editor validation and inspectors.
        public ElementsViewOwnerBindingsEditorData ownerBindingsEditorData;

        public VisualElement CloneView()
        {
            return CloneUxmlWithStyleSheet(uxml, styleSheet);
        }

        public override void OpenView()
        {
            AssetDatabase.OpenAsset(uxml);
            AssetDatabase.OpenAsset(styleSheet);
        }

        public override void AddGraphablesToViewElements()
        {
            throw new System.NotImplementedException("ElementsController graphing is not currently supported.");
        }

        public override IGraphableEmbedViewObject[] GraphableEmbedViewObjectsInView()
        {
            throw new System.NotImplementedException("ElementsController graphing is not currently supported.");
        }

        public override IGraphableTransitionViewObject[] GraphableTransitionViewObjectsInView()
        {
            throw new System.NotImplementedException("ElementsController graphing is not currently supported.");
        }

        public override bool ContainsAsset(string assetPath)
        {
            string uxmlPath = AssetDatabase.GetAssetPath(uxml);
            string styleSheetPath = AssetDatabase.GetAssetPath(styleSheet);
            return (assetPath.Equals(uxmlPath) || assetPath.Equals(styleSheetPath));
        }
#endif
    }
}
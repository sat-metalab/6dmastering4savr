﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System;
using System.Text;
using UnityEngine;

namespace Pelican7.UIGraph
{
    public class Error
    {
        private readonly string description;
        private readonly Severity severity;

        public Error(string description, Severity severity = Severity.Error)
        {
            this.description = description;
            this.severity = severity;
        }

        [Flags]
        public enum Severity
        {
            Log = 1,
            Warning = 2,
            Error = 4
        }

        public static void Log(string description, Severity severity = Severity.Error)
        {
            var error = new Error(description, severity);
            error.Log();
        }

        public void Log()
        {
            LogWithPrefix(string.Empty);
        }

        public void LogWithPrefix(string prefix)
        {
            var stringBuilder = new StringBuilder(prefix.Length + description.Length + 1); // +1 for a space between.
            stringBuilder.Append(prefix);
            if (string.IsNullOrEmpty(prefix) == false)
            {
                stringBuilder.Append(" ");
            }
            stringBuilder.Append(description);
            string fullDescription = stringBuilder.ToString();

            switch (severity)
            {
                case Severity.Log:
                {
                    Debug.Log(fullDescription);
                    break;
                }

                case Severity.Warning:
                {
                    Debug.LogWarning(fullDescription);
                    break;
                }

                case Severity.Error:
                {
                    Debug.LogError(fullDescription);
                    break;
                }
            }
        }
    }
}
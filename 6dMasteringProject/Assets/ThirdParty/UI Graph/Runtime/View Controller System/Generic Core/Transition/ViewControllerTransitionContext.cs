﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The view controller transition context provides detailed information about a view controller transition, such as the view controllers involved.
    /// <para>
    /// You will never use an instance of this class directly. Instead you will be given a concrete subclass, <see cref="CanvasControllerTransitionContext"/> or <see cref="ElementsControllerTransitionContext"/>, depending upon the workflow you are using.
    /// </para>
    /// </summary>
    /// <typeparam name="TViewController"></typeparam>
    public abstract partial class ViewControllerTransitionContext<TViewController>
    {
        public ViewControllerTransitionContext(ViewControllerTransitionIdentifier identifier, TViewController toViewController, TViewController fromViewController, TViewController ownerViewController, bool animated, bool interactive, Stack<TViewController> intermediaryViewControllers = null)
        {
            Identifier = identifier;
            ToViewController = toViewController;
            FromViewController = fromViewController;
            OwnerViewController = ownerViewController;
            Animated = animated;
            Interactive = interactive;

            if (intermediaryViewControllers != null)
            {
                IntermediaryViewControllers = intermediaryViewControllers;
            }
        }

        /// <summary>
        /// The transition's identifier.
        /// </summary>
        public ViewControllerTransitionIdentifier Identifier { get; }
        /// <summary>
        /// The view controller being transitioned to. In a Present transition, this is the view controller being presented. In an Dismiss transition, this is the view controller whom is being dismissed to. 
        /// </summary>
        public TViewController ToViewController { get; }
        /// <summary>
        /// The view controller being transition from. In a Present transition, this is the view controller whom initiated the presentation. In an Dismiss transition, this is the view controller being dismissed. 
        /// </summary>
        public TViewController FromViewController { get; }
        /// <summary>
        /// The view controller whom owns the transition. For containment transitions, the owner is the parent whom is performing the transition. In Present and Dismiss transitions, the owner is the view controller being presented or dismissed.
        /// </summary>
        public TViewController OwnerViewController { get; }
        /// <summary>
        /// Is the transition animated?
        /// </summary>
        public bool Animated { get; }
        /// <summary>
        /// Is the transition interactive?
        /// </summary>
        public bool Interactive { get; }
        /// <summary>
        /// Any intermediary view controllers associated with the transition. For example, intermediary view controllers are present when dismissing back to a view controller lower in the stack than the current view controller's presenter.
        /// </summary>
        public Stack<TViewController> IntermediaryViewControllers { get; } = new Stack<TViewController>();
    }

    public abstract partial class ViewControllerTransitionContext<TViewController> : ITransitionContext
        where TViewController : ITransitionContextViewController
    {
        bool ITransitionContext.Animated
        {
            get
            {
                return Animated;
            }
        }

        bool ITransitionContext.Interactive
        {
            get
            {
                return Interactive;
            }
        }

        void IAppearable.BeginAppearanceTransition(bool isAppearing)
        {
            bool toIsAppearing = isAppearing;
            bool fromIsAppearing = (isAppearing == false);
            (ToViewController as IAppearable)?.BeginAppearanceTransition(toIsAppearing);
            (FromViewController as IAppearable)?.BeginAppearanceTransition(fromIsAppearing);
        }

        void IAppearable.EndAppearanceTransition()
        {
            (ToViewController as IAppearable)?.EndAppearanceTransition();
            (FromViewController as IAppearable)?.EndAppearanceTransition();
        }

        void ITransitionContext.ShowToViewController()
        {
            var toViewController = ToViewController;
            if (toViewController != null)
            {
                toViewController.SetViewVisible(true);
            }
        }

        void ITransitionContext.HideRelevantViewControllerIfNecessary(bool completed)
        {
            var fromViewController = FromViewController;
            var toViewController = ToViewController;
            if ((fromViewController != null) && (toViewController != null))
            {
                if (completed)
                {
                    // In completed presentation and replacement transitions, hide the from view controller if the to view controller is opaque. In completed dismissal transitions, do nothing.
                    if (Identifier.Type != ViewControllerTransitionIdentifier.TransitionType.Dismissal)
                    {
                        if (toViewController.Opaque)
                        {
                            fromViewController.SetViewVisible(false);
                        }
                    }
                }
                else
                {
                    // In uncompleted dismissal transitions, hide the to view controller if the from view controller is opaque. In uncompleted presentation and replacement transitions, do nothing.
                    if (Identifier.Type == ViewControllerTransitionIdentifier.TransitionType.Dismissal)
                    {
                        if (fromViewController.Opaque)
                        {
                            toViewController.SetViewVisible(false);
                        }
                    }
                }
            }
        }
    }
}
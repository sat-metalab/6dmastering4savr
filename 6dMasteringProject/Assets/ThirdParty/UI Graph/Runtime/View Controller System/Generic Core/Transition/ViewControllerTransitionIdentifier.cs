﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A transition identifier is used to identify a view controller transition. Generally, you create public, static ViewControllerTransitionIdentifier fields in your container view controller for each of its custom transitions. This identifier can then be used to identify the transition from any script, as well as exposed to the Graphing and Transition Sequence Animator systems.
    /// <para>
    /// You must specify a globally unique identifier for your transition identifier, which usually follows the format 'TypeName.TransitionName', such as CanvasController.Present.
    /// </para>
    /// <para>
    /// You must specify a <see cref="ViewControllerTransitionIdentifier.TransitionType"/> for your transition identifier, which informs the graphing system how to treat your transition. The options are <see cref="ViewControllerTransitionIdentifier.TransitionType.Presentation"/>, <see cref="ViewControllerTransitionIdentifier.TransitionType.Dismissal"/>, and <see cref="ViewControllerTransitionIdentifier.TransitionType.Replacement"/>.
    /// </para>
    /// <para>
    /// You may optionally specify a display name for your transition identifier. This will be displayed in the editor by both the Graphing and Transition Sequence Animator systems when exposing the transition identifier.
    /// </para>
    /// </summary>
    public class ViewControllerTransitionIdentifier
    {
        private readonly string displayName;

        /// <summary>
        /// The type of transition identifier.
        /// </summary>
        public enum TransitionType
        {
            /// <summary>
            /// A Presentation transition type describes any transition in which one or more view controllers are presented.
            /// </summary>
            Presentation,
            /// <summary>
            /// A Dismissal transition type describes any transition in which one or more view controllers are dismissed.
            /// </summary>
            Dismissal,
            /// <summary>
            /// A Replacement transition type describes any transition in which one or more view controllers are replaced. For example, a navigation controller's 'SetRoot' transition is a Replacement transition.
            /// </summary>
            Replacement
        }

        /// <summary>
        /// The transition identifier's globally unique identifier.
        /// </summary>
        public string Guid { get; }
        /// <summary>
        /// The transition identifier's type.
        /// </summary>
        public TransitionType Type { get; }
        /// <summary>
        /// The transition identifier's display name. Used in the editor by the Graphing and Transition Sequence Animator systems.
        /// </summary>
        public string DisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(displayName))
                {
                    return Guid;
                }

                return displayName;
            }
        }

        /// <summary>
        /// Create a new view controller transition identifier. Generally, you create public, static ViewControllerTransitionIdentifier fields in your container view controller for each of its custom transitions.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="type"></param>
        /// <param name="displayName"></param>
        public ViewControllerTransitionIdentifier(string guid, TransitionType type, string displayName = null)
        {
            Guid = guid;
            Type = type;
            this.displayName = displayName;
        }
    }
}
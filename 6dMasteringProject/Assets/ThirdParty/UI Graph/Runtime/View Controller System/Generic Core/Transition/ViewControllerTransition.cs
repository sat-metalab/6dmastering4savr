﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine.Events;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// When a view controller transition is to be performed, UI Graph creates a transition object to describe the transition. This object is passed to methods that require it, such as <see cref="ViewController{TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph}.PrepareForGraphTransition(GraphTransition{TViewController})"/>, providing you with the relevant transition information. <para>You will never interface directly with an instance of this class. Instead you will be given a concrete subclass, <see cref="CanvasControllerTransition"/> or <see cref="ElementsControllerTransition"/>, depending upon the workflow you are using.</para>
    /// </summary>
    /// <typeparam name="TViewController"></typeparam>
    /// <typeparam name="TTransition"></typeparam>
    /// <typeparam name="TTransitionContext"></typeparam>
    /// <typeparam name="TTransitionAnimatorProvider"></typeparam>
    /// <typeparam name="TTransitionProgressProvider"></typeparam>
    /// <typeparam name="TTransitionAnimationDriver"></typeparam>
    /// <typeparam name="TTransitionAnimationDefaultProgressProvider"></typeparam>
    public abstract partial class ViewControllerTransition< TViewController, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider>
        where TTransition : ViewControllerTransition<TViewController, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider>
        where TTransitionContext : ITransitionContext
        where TTransitionAnimatorProvider : class, IViewControllerTransitionAnimatorProvider<TTransitionContext>
        where TTransitionProgressProvider : class, IViewControllerTransitionProgressProvider<TTransitionContext>
        where TTransitionAnimationDriver : ViewControllerTransitionAnimationDriver<TTransitionContext, TTransitionAnimationDefaultProgressProvider>, new()
        where TTransitionAnimationDefaultProgressProvider : class, IViewControllerTransitionProgressProvider<TTransitionContext>, new()
    {
        protected TTransitionContext context;

        private readonly ValueChangeDirection driverProgressChangeDirection = new ValueChangeDirection();
        private TTransitionAnimationDriver animationDriver;

        public ViewControllerTransitionBeganEvent OnTransitionBegan = new ViewControllerTransitionBeganEvent();
        public ViewControllerTransitionProgressEvent OnTransitionProgressUpdated = new ViewControllerTransitionProgressEvent();
        public ViewControllerTransitionFinishedEvent OnTransitionWillFinish = new ViewControllerTransitionFinishedEvent();
        public ViewControllerTransitionFinishedEvent OnTransitionDidFinish = new ViewControllerTransitionFinishedEvent();

        /// <summary>
        /// The transition's animator, as provided by the owning view controller's <see cref="ViewController{TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph}.transitionAnimatorProvider"/>
        /// </summary>
        public IViewControllerTransitionAnimator<TTransitionContext> Animator { get; private set; }
        /// <summary>
        /// The transition's context. The context provides detailed information about the transition, such as the view controllers involved.
        /// </summary>
        public TTransitionContext Context
        {
            get
            {
                return context;
            }
        }

        protected abstract TTransitionAnimatorProvider TransitionAnimatorProvider { get; }
        protected abstract TTransitionProgressProvider InteractiveTransitionProgressProvider { get; }

        private TTransition AsTransition
        {
            get
            {
                return this as TTransition;
            }
        }

        public abstract void Initialize(ViewControllerTransitionIdentifier identifier, TViewController toViewController, TViewController fromViewController, TViewController ownerViewController, bool animated, bool interactive, Stack<TViewController> intermediaryViewControllers = null);

        public void Perform(UnityAction<TTransition> OnTransitionBegan = null, UnityAction<TTransition, bool> OnTransitionWillFinish = null, UnityAction<TTransition, bool> OnTransitionDidFinish = null)
        {
            AddCallbackListeners(OnTransitionBegan, OnTransitionWillFinish, OnTransitionDidFinish);
            PerformTransition();
        }

        public void ForceImmediateCompletion()
        {
            animationDriver?.ForceImmediateCompletion();
        }

        public void ForceImmediateCancellation()
        {
            animationDriver?.ForceImmediateCancellation();
        }

        private void AddCallbackListeners(UnityAction<TTransition> OnTransitionBegan, UnityAction<TTransition, bool> OnTransitionWillFinish, UnityAction<TTransition, bool> OnTransitionDidFinish)
        {
            if (OnTransitionBegan != null)
            {
                this.OnTransitionBegan.AddListener(OnTransitionBegan);
            }

            if (OnTransitionWillFinish != null)
            {
                this.OnTransitionWillFinish.AddListener(OnTransitionWillFinish);
            }

            if (OnTransitionDidFinish != null)
            {
                this.OnTransitionDidFinish.AddListener(OnTransitionDidFinish);
            }
        }

        private void PerformTransition()
        {
            BeginTransition();

            bool animated = context.Animated;
            if (animated == false)
            {
                FinishTransition(true);
                return;
            }

            RunTransitionAnimation();
        }

        private void BeginTransition()
        {
            InvokeBeginAppearanceMethods();
            context.ShowToViewController();
            OnTransitionBegan?.Invoke(AsTransition);
        }

        private void InvokeBeginAppearanceMethods()
        {
            context.BeginAppearanceTransition(true);
        }

        private void InvokeBeginAppearanceMethodsForCancellation()
        {
            context.BeginAppearanceTransition(false);
        }

        private void RunTransitionAnimation()
        {
            if (ConfigureAnimator(out Error error) == false)
            {
                // Complete the transition with no animation if something went wrong configuring the animator.
                error.Log();
                FinishTransition(true);
                return;
            }

            TTransitionProgressProvider progressProvider = null;
            if (context.Interactive)
            {
                progressProvider = InteractiveTransitionProgressProvider;
                if (progressProvider == null)
                {
                    Error.Log("View controller did not provide a progress provider for an interactive transition. This transition will animate without interactivity.", Error.Severity.Warning);
                }
            }

            float duration = Animator.TransitionDuration(context);
            ViewControllerTransitionTimeUpdateMode timeUpdateMode = Animator.TransitionTimeUpdateMode(context);
            animationDriver = new TTransitionAnimationDriver();
            animationDriver.Initialize(this as IViewControllerTransitionAnimationDriverResponder, context, duration, timeUpdateMode, progressProvider);
            animationDriver.Run();
        }

        private bool ConfigureAnimator(out Error error)
        {
            var transitionAnimatorProvider = TransitionAnimatorProvider;
            Animator = transitionAnimatorProvider?.AnimatorForTransition(context);
            if (Animator == null)
            {
                error = new Error("View controller did not provide an animator for an animated transition. This transition will complete with no animation.", Error.Severity.Warning);
                return false;
            }

            Animator.ConfigureTransitionAnimation(context);
            error = null;
            return true;
        }

        private void FinishTransition(bool completed)
        {
            Animator?.OnTransitionFinished(context, completed);
            OnTransitionWillFinish?.Invoke(AsTransition, completed);
            context.HideRelevantViewControllerIfNecessary(completed);

            InvokeEndAppearanceMethods();
            OnTransitionDidFinish?.Invoke(AsTransition, completed);
        }

        private void InvokeEndAppearanceMethods()
        {
            context.EndAppearanceTransition();
        }

        public class ViewControllerTransitionBeganEvent : UnityEvent<TTransition> { }
        public class ViewControllerTransitionProgressEvent : UnityEvent<TTransition, float> { }
        public class ViewControllerTransitionFinishedEvent : UnityEvent<TTransition, bool> { }
    }

    public abstract partial class ViewControllerTransition<TViewController, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider> : IViewControllerTransitionAnimationDriverResponder
    {
        void IViewControllerTransitionAnimationDriverResponder.OnDriverProgressUpdated(float progress01)
        {
            // If the driver's progress changes direction, invoke appearance methods.
            bool hasChangedDirection = driverProgressChangeDirection.UpdateValue(progress01);
            if (hasChangedDirection)
            {
                if (driverProgressChangeDirection.Direction == ValueChangeDirection.ChangeDirection.Decrease)
                {
                    InvokeBeginAppearanceMethodsForCancellation();
                }
                else
                {
                    InvokeBeginAppearanceMethods();
                }
            }

            Animator.UpdateTransitionAnimation(context, progress01);
            OnTransitionProgressUpdated.Invoke(AsTransition, progress01);
        }

        void IViewControllerTransitionAnimationDriverResponder.OnDriverCompleted()
        {
            bool completed = true;
            FinishTransition(completed);
        }

        void IViewControllerTransitionAnimationDriverResponder.OnDriverCancelled()
        {
            bool completed = false;
            FinishTransition(completed);
        }
    }
}
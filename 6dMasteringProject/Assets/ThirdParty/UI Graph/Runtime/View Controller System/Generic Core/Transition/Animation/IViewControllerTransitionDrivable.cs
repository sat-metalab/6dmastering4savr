﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A transition drivable is used to drive a transition's progress, such as when implementing an interactive transition.
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public interface IViewControllerTransitionDrivable<TContext>
    {
        /// <summary>
        /// The transition's duration.
        /// </summary>
        float Duration { get; }
        /// <summary>
        /// The transition's time update mode.
        /// </summary>
        ViewControllerTransitionTimeUpdateMode TimeUpdateMode { get; }
        /// <summary>
        /// The transition's current progress, from zero to one. Drive the transition's progress with this value.
        /// </summary>
        float Progress01 { get; set; }
        /// <summary>
        /// The transition's direction.
        /// </summary>
        ViewControllerTransitionAnimationDriverDirection Direction { get; set; }
        /// <summary>
        /// The transition's context.
        /// </summary>
        TContext Context { get; }
        /// <summary>
        /// The transition's completion curve. Upon calling <see cref="CompleteTransition"/> or <see cref="CancelTransition"/>, UI Graph will complete the remainder of the transition using the remaining duration and the completion curve.
        /// </summary>
        AnimationCurve CompletionCurve { set; }

        /// <summary>
        /// Complete the transition. Invoke this method when you want to complete the transition. UI Graph will complete the remainder of the transition using the remaining duration and the completion curve.
        /// </summary>
        void CompleteTransition();
        /// <summary>
        /// Cancel the transition. Invoke this method when you want to cancel the transition. UI Graph will complete the remainder of the transition to zero progress using the remaining duration and the completion curve.
        /// </summary>
        void CancelTransition();
    }
}
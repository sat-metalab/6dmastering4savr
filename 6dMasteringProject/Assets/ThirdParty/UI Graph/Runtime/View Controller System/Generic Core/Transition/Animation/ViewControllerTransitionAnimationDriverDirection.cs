﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The direction of the transition driver.
    /// </summary>
    public enum ViewControllerTransitionAnimationDriverDirection
    {
        /// <summary>
        /// Progress updates are increasing.
        /// </summary>
        Forwards,
        /// <summary>
        /// Progress updates are decreasing.
        /// </summary>
        Reversed
    }
}
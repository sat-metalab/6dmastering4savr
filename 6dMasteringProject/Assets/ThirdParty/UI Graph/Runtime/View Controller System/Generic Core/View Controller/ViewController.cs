﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// <para>
    /// A view controller manages a single view of content in your user-interface. It manages the life-cycle of its view and is responsible for instantiating and destroying its view as required, such as upon presentation or dismissal.
    /// </para>
    /// <para>
    /// Do not create instances of this class directly. Instead, use one of the concrete base classes, <see cref="CanvasController"/> or <see cref="ElementsController"/>, depending on the workflow you are using.
    /// </para>
    /// </summary>
    /// <typeparam name="TViewController"></typeparam>
    /// <typeparam name="TView"></typeparam>
    /// <typeparam name="TWindow"></typeparam>
    /// <typeparam name="TViewResource"></typeparam>
    /// <typeparam name="TTransition"></typeparam>
    /// <typeparam name="TTransitionContext"></typeparam>
    /// <typeparam name="TTransitionAnimatorProvider"></typeparam>
    /// <typeparam name="TTransitionProgressProvider"></typeparam>
    /// <typeparam name="TTransitionAnimationDriver"></typeparam>
    /// <typeparam name="TTransitionAnimationDefaultProgressProvider"></typeparam>
    /// <typeparam name="TTransitionData"></typeparam>
    /// <typeparam name="TGraph"></typeparam>
    public abstract partial class ViewController<TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph> : ScriptableObject
        where TViewController : ViewController<TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph>
        where TView : IView<TView, TWindow>
        where TWindow : TView
        where TViewResource : ViewResource<TView, TWindow>
        where TTransition : ViewControllerTransition<TViewController, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider>, new()
        where TTransitionContext : ViewControllerTransitionContext<TViewController>
        where TTransitionAnimatorProvider : class, IViewControllerTransitionAnimatorProvider<TTransitionContext>
        where TTransitionProgressProvider : class, IViewControllerTransitionProgressProvider<TTransitionContext>
        where TTransitionAnimationDriver : ViewControllerTransitionAnimationDriver<TTransitionContext, TTransitionAnimationDefaultProgressProvider>, new()
        where TTransitionAnimationDefaultProgressProvider : class, IViewControllerTransitionProgressProvider<TTransitionContext>, new()
        where TTransitionData : ViewControllerInvokeTransitionData<TViewController>, new()
        where TGraph : class
    {
        /// <summary>
        /// The view controller's title. This will be displayed by standard containers, such as the navigation and tab bar view controllers.
        /// </summary>
        public string title;
        /// <summary>
        /// Is the view controller opaque? When a view controller is presented, its presenter is hidden if it is opaque. When opaque is set to false, i.e. the view controller is transparent, the presenter of this view controller will not be hidden upon presentation.
        /// </summary>
        public bool opaque = true;
        /// <summary>
        /// The view controller's transition animator provider is responsible for providing transition animators when this view controller performs a transition. You may use your own provider or alter the animators returned by the default providers to customize a view controller's transition animations.
        /// </summary>
        public TTransitionAnimatorProvider transitionAnimatorProvider;
        /// <summary>
        /// The view controller's interactive transition progress provider is responsible for providing progress updates during an interactive transition.
        /// </summary>
        public TTransitionProgressProvider interactiveTransitionProgressProvider;

        private readonly ViewControllerAppearance appearance = new ViewControllerAppearance();
        private readonly List<TViewController> children = new List<TViewController>();
        private readonly Dictionary<ViewControllerTransitionIdentifier, TTransition> childTransitions = new Dictionary<ViewControllerTransitionIdentifier, TTransition>();
#pragma warning disable 0649
        [SerializeField] private TViewResource viewResource;
#pragma warning restore 0649
        private TView view;
        private TViewController presentingViewController;
        private TTransition modalTransition;

        /// <summary>
        /// The OnWillPerformChildTransition event is called just before the view controller performs a transition between two of its child view controllers.
        /// </summary>
        public WillPerformChildTransitionEvent OnWillPerformChildTransition = new WillPerformChildTransitionEvent();
        /// <summary>
        /// The OnDidPerformChildTransition event is called just after the view controller has performed a transition between two of its child view controllers.
        /// </summary>
        public DidPerformChildTransitionEvent OnDidPerformChildTransition = new DidPerformChildTransitionEvent();
        
        private readonly UnityEvent OnLoadView = new UnityEvent();

        /// <summary>
        /// The view controller's view. If the view is not loaded, it will be instantiated. Use <see cref="ViewIsLoaded"/> to determine whether the view has been loaded.
        /// </summary>
        public TView View
        {
            get
            {
                LoadViewIfRequired();
                return view;
            }

            set
            {
                if (ViewIsLoaded)
                {
                    Error.Log("Cannot set view. View is already loaded.");
                    return;
                }

                view = value;
            }
        }

        /// <summary>
        /// Is the view controller's view loaded?
        /// </summary>
        public bool ViewIsLoaded
        {
            get
            {
                return ((view == null || view.Equals(null)) == false);
            }
        }

        /// <summary>
        /// The view controller's presented view controller. If the view controller has not presented a view controller, returns null.
        /// </summary>
        /// <remarks>
        /// Note that presented view controllers do not include child view controllers.
        /// </remarks>
        public TViewController PresentedViewController { get; protected set; }
        /// <summary>
        /// The view controller's presenting view controller – its presenter. If no view controller presented this view controller, returns null. This is true for root view controllers.
        /// </summary>
        public TViewController PresentingViewController
        {
            get
            {
                var rootAncestor = RootAncestor;
                return rootAncestor.presentingViewController;
            }

            set
            {
                if (presentingViewController != null)
                {
                    Error.Log("A view controller's presenting view controller cannot be changed.");
                    return;
                }

                presentingViewController = value;
            }
        }

        /// <summary>
        /// The view controller's presentation stack starting from the receiving view controller. Beginning at the receiving view controller, this method visits each view controller's <see cref="PresentedViewController"/> to form the presentation stack. UI Graph uses this method when dismissing to destroy any intermediary view controllers if necessary.
        /// </summary>
        public Stack<TViewController> PresentedViewControllerStack
        {
            get
            {
                return ComputePresentedViewControllerStack(true);
            }
        }

#if UNITY_EDITOR
        /// <summary>
        /// The view controller's view resource.
        /// </summary>
        public TViewResource ViewResource
        {
            get
            {
                return viewResource;
            }

            set
            {
                viewResource = value;
            }
        }
#endif

        protected abstract ViewControllerTransitionIdentifier PresentTransitionIdentifier { get; }
        protected abstract ViewControllerTransitionIdentifier DismissTransitionIdentifier { get; }
        /// <summary>
        /// The view controller's parent view controller. If this view controller has been added as a child of another view controller, returns its parent view controller. Otherwise, returns null.
        /// </summary>
        protected TViewController Parent { get; private set; }
        /// <summary>
        /// The view controller's root ancestor or top-most parent.
        /// </summary>
        protected TViewController RootAncestor
        {
            get
            {
                var rootAncestor = This;
                var p = rootAncestor.Parent;
                while (p != null)
                {
                    rootAncestor = p;
                    p = p.Parent;
                }

                return rootAncestor;
            }
        }

        /// <summary>
        /// Is the view controller performing a transition between child view controllers?
        /// </summary>
        protected bool IsPerformingTransitionBetweenChildren
        {
            get
            {
                return childTransitions.Count > 0;
            }
        }

        private TViewController This
        {
            get
            {
                return this as TViewController;
            }
        }

        private bool IsPerformingModalTransition
        {
            get
            {
                return (modalTransition != null);
            }
        }

        /// <summary>
        /// Add <paramref name="viewController"/> as a child of this view controller. This method configures the parent/child relationship between the two view controllers. You are still responsible for embedding its view, as required.
        /// </summary>
        /// <param name="viewController"></param>
        public void AddChild(TViewController viewController)
        {
            viewController.Parent = This;
            children.Add(viewController);
        }

        /// <summary>
        /// Remove the view controller from its parent view controller. This method configures the parent/child relationship between this view controller and its parent. You are still responsible for removing its view, as required.
        /// </summary>
        public void RemoveFromParent()
        {
            if (Parent == null)
            {
                return;
            }

            Parent.RemoveChild(This);
        }

        /// <summary>
        /// Invoke a view controller transition in this view controller or any of its parents. Beginning a the receiving view controller, this method will iterate up the parent hierarchy until it reaches a view controller whom can perform the transition. If multiple view controllers that can perform the transition exist within the parent hierarchy, the view controller closest in depth to the receiver will be used. If no view controller is found to perform the transition, an error will be logged to the console.
        /// </summary>
        /// <seealso cref="ReceiverForTransition(ViewControllerTransitionIdentifier)"/>
        /// <param name="transitionIdentifier"></param>
        /// <param name="transitionData"></param>
        /// <param name="completion"></param>
        public void InvokeTransition(ViewControllerTransitionIdentifier transitionIdentifier, TTransitionData transitionData, System.Action completion = null)
        {
            // Intercept the Present & Dismiss transitions manually, rather than using the ReceiverForTransition/PerformInvokedTransition pattern that subclasses use. This means that view controller subclasses aren't required to call base.CanPerformTransition and base.PerformInvokedTransition in their implementations.
            if (transitionIdentifier.Equals(PresentTransitionIdentifier))
            {
                var toViewController = transitionData.ToViewController;
                var animated = transitionData.Animated;
                var interactive = transitionData.Interactive;
                Present(toViewController, animated, interactive, completion);
            }
            else if (transitionIdentifier.Equals(DismissTransitionIdentifier))
            {
                var animated = transitionData.Animated;
                var interactive = transitionData.Interactive;
                Dismiss(animated, interactive, completion);
            }
            else
            {
                var receiver = ReceiverForTransition(transitionIdentifier);
                if (receiver != null)
                {
                    receiver.PerformInvokedTransition(transitionIdentifier, transitionData, completion);
                }
                else
                {
                    Error.Log(string.Format("No view controller in the hierarchy can perform the transition {0}.", transitionIdentifier), Error.Severity.Log);
                }
            }
        }

        /// <summary>
        /// A convenience method for calling <see cref="InvokeTransition(ViewControllerTransitionIdentifier, TTransitionData, System.Action)"/> with, optionally, a view controller, animated flag, and completion action.
        /// </summary>
        /// <param name="transitionIdentifier"></param>
        /// <param name="viewController"></param>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void InvokeTransition(ViewControllerTransitionIdentifier transitionIdentifier, TViewController viewController = null, bool animated = true, System.Action completion = null)
        {
            var transitionData = new TTransitionData
            {
                ToViewController = viewController,
                Animated = animated,
                Interactive = false
            };
            InvokeTransition(transitionIdentifier, transitionData, completion);
        }

        /// <summary>
        /// A convenience method for calling <see cref="InvokeTransition(ViewControllerTransitionIdentifier, TTransitionData, System.Action)"/> with interactive == true and optionally a view controller and completion action.
        /// </summary>
        /// <param name="transitionIdentifier"></param>
        /// <param name="viewController"></param>
        /// <param name="completion"></param>
        public void InvokeTransitionInteractively(ViewControllerTransitionIdentifier transitionIdentifier, TViewController viewController = null, System.Action completion = null)
        {
            var transitionData = new TTransitionData
            {
                ToViewController = viewController,
                Animated = true,
                Interactive = true
            };
            InvokeTransition(transitionIdentifier, transitionData, completion);
        }

        /// <summary>
        /// Returns the first view controller in the view controller's parent hierarchy whom can perform the <paramref name="transitionIdentifier"/>, determined by querying each view controller's <see cref="ImplementsTransition(ViewControllerTransitionIdentifier)"/> method. Returns null if no such view controller was found. This method is also used by <see cref="InvokeTransition(ViewControllerTransitionIdentifier, TTransitionData, System.Action)"/> to locate the receiving view controller.
        /// </summary>
        /// <param name="transitionIdentifier"></param>
        /// <returns>The first view controller in the view controller's parent hierarchy whom can perform the <paramref name="transitionIdentifier"/>, or null if none was found.</returns>
        public TViewController ReceiverForTransition(ViewControllerTransitionIdentifier transitionIdentifier)
        {
            // Intercept the Present & Dismiss transitions manually, rather than using the ReceiverForTransition/PerformInvokedTransition pattern that subclasses use. This means that view controller subclasses aren't required to call base.ImplementsTransition and base.PerformInvokedTransition in their implementations.
            if (transitionIdentifier.Equals(PresentTransitionIdentifier) || transitionIdentifier.Equals(DismissTransitionIdentifier))
            {
                return RootAncestor;
            }

            TViewController receiver = null;

            TViewController viewController = This;
            while (viewController != null)
            {
                if (viewController.ImplementsTransition(transitionIdentifier))
                {
                    receiver = viewController;
                    break;
                }

                viewController = viewController.Parent;
            }

            return receiver;
        }

        /// <summary>
        /// Present <paramref name="viewController"/>. The specified view controller will be added to the view controller's window, causing its view to be loaded, and subsequently presented.
        /// <para>
        /// If this method is called on a child view controller, it will automatically be forwarded to its <see cref="RootAncestor"/>.
        /// </para>
        /// </summary>
        /// <remarks>
        /// <paramref name="viewController"/> should be an instance of a view controller asset, instantiated with <c>Instantiate(viewControllerAsset)</c> prior to passing it to this method.
        /// </remarks>
        /// <param name="viewController"></param>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void Present(TViewController viewController, bool animated = true, System.Action completion = null)
        {
            Present(viewController, animated, false, completion);
        }

        /// <summary>
        /// Present <paramref name="viewController"/> interactively. Prior to calling this method, you should set the view controller's <see cref="interactiveTransitionProgressProvider"/> to an appropriate object. If no <see cref="interactiveTransitionProgressProvider"/> has been configured, this view controller will be presented without interactivity.
        /// <para>
        /// If this method is called on a child view controller, it will automatically be forwarded to its <see cref="RootAncestor"/>.
        /// </para>
        /// </summary>
        /// <seealso cref="Present(TViewController, bool, System.Action)"/>
        /// <param name="viewController"></param>
        /// <param name="completion"></param>
        public void PresentInteractively(TViewController viewController, System.Action completion = null)
        {
            Present(viewController, true, true, completion);
        }

        /// <summary>
        /// Dismiss this view controller's top-most <see cref="PresentedViewController"/>. If any intermediary canvas controllers exist between this canvas controller and the top-most presented one, they will be hidden and subsequently destroyed. Only the top-most canvas controller and this canvas controller will be animated during the transition.
        /// <para>
        /// If this canvas controller has no <see cref="PresentedViewController"/>, it will be dismissed by its <see cref="PresentingViewController"/>. 
        /// </para>
        /// <para>
        /// If this method is called on a child view controller, it will automatically be forwarded to its <see cref="RootAncestor"/>.
        /// </para>
        /// </summary>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void Dismiss(bool animated = true, System.Action completion = null)
        {
            Dismiss(animated, false, completion);
        }

        /// <summary>
        /// Dismiss this view controller's top-most <see cref="PresentedViewController"/> interactively. Prior to calling this method, you should set the view controller's <see cref="interactiveTransitionProgressProvider"/> to an appropriate object. If no <see cref="interactiveTransitionProgressProvider"/> has been configured, the dismissal will proceed without interactivity.
        /// <para>
        /// If this method is called on a child view controller, it will automatically be forwarded to its <see cref="RootAncestor"/>.
        /// </para>
        /// </summary>
        /// <param name="completion"></param>
        public void DismissInteractively(System.Action completion = null)
        {
            Dismiss(true, true, completion);
        }

        /// <summary>
        /// Destroy this view controller instance. If the view controller's view has been loaded, it will be unloaded.
        /// </summary>
        public virtual void Destroy()
        {
            Destroy(this);
        }

        /// <summary>
        /// Can <paramref name="viewController"/> currently be presented by this view controller?
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="error"></param>
        /// <returns>True if <paramref name="viewController"/> can be presented. False otherwise.</returns>
        public bool CanPresentViewController(TViewController viewController, out Error error)
        {
            if (CanPerformModalTransition(out error) == false)
            {
                return false;
            }

            if (viewController == null)
            {
                error = new Error("The provided view controller is null.");
                return false;
            }

            if (PresentedViewController != null)
            {
                error = new Error(string.Format("Presenter already has a presented view controller ('{0}').", PresentedViewController));
                return false;
            }

            if (view.Window == null)
            {
                error = new Error("Presenter's view is not part of a window hierarchy.");
                return false;
            }

            error = null;
            return true;
        }

        /// <summary>
        /// Can this view controller dismiss its presented view controller?
        /// </summary>
        /// <param name="error"></param>
        /// <returns>True if this view controller can dismiss a view controller. False otherwise.</returns>
        public bool CanDismissViewController(out Error error)
        {
            if (CanPerformModalTransition(out error) == false)
            {
                return false;
            }

            if (Parent == null)
            {
                if ((PresentedViewController == null) && (PresentingViewController == null))
                {
                    error = new Error("Dismisser is a root view controller and has no presented view controller. To dismiss a root view controller, call dismiss on the window instead.");
                    return false;
                }
            }

            error = null;
            return true;
        }

        /// <summary>
        /// ViewDidLoad is called immediately after the view controller's view has been instantiated and is therefore called only once during your view controller's life-cycle. Override this method to perform additional one-time view setup.
        /// </summary>
        protected virtual void ViewDidLoad() { }
        /// <summary>
        /// ViewWillAppear is called prior to the view appearing on-screen and before the transition has begun. Override this method to be notified just before the view is about to appear on-screen. 
        /// </summary>
        protected virtual void ViewWillAppear() { }
        /// <summary>
        /// ViewDidAppear is called after the view has appeared on-screen and after the transition has completed. Override this method to be notified just after the view has appeared on-screen. 
        /// </summary>
        protected virtual void ViewDidAppear() { }
        /// <summary>
        /// ViewWillDisappear is called prior to the view disappearing off-screen and before the transition has begun. Override this method to be notified just before the view is about to disappear off-screen. 
        /// </summary>
        protected virtual void ViewWillDisappear() { }
        /// <summary>
        /// ViewDidDisappear is called after the view has disappeared off-screen and after the transition has completed. Override this method to be notified just after the view has disappeared off-screen. 
        /// </summary>
        protected virtual void ViewDidDisappear() { }

        /// <summary>
        /// Loads the view controller's view from its resource. You do not need to call this method directly. It is called internally when the view controller's view is required.
        /// <para>
        /// Override this method if you wish to create your view entirely from code instead of using a view resource.
        /// </para>
        /// </summary>
        protected virtual void LoadView()
        {
            if (viewResource == null)
            {
                Error.Log("Cannot load view from resource. The view resource is not set.");
                return;
            }

            view = viewResource.Load(out IViewBindable viewOwnerBindings);
            viewOwnerBindings?.Bind(this);
        }

        protected virtual void Awake() { }
        protected virtual void OnEnable() { }
        protected virtual void OnDisable() { }
        protected virtual void OnDestroy()
        {
            if (ViewIsLoaded)
            {
                view.Unload();
            }

            CancelAllChildTransitions();
            DestroyChildren();
            RemoveFromParent();
        }

        /// <summary>
        /// Container view controllers override this method to return true for their own transition types.
        /// </summary>
        /// <param name="transitionIdentifier"></param>
        /// <returns>True if this view controller implements the transition identified by <paramref name="transitionIdentifier"/></returns>
        protected virtual bool ImplementsTransition(ViewControllerTransitionIdentifier transitionIdentifier)
        {
            return false;
        }

        /// <summary>
        /// Container view controllers override this method to forward to their appropriate transition methods. When <see cref="InvokeTransition(ViewControllerTransitionIdentifier, TTransitionData, System.Action)"/> is called, this method will be called on the view controller that returned true to <see cref="ImplementsTransition"/> to perform the transition.
        /// </summary>
        /// <param name="transitionIdentifier"></param>
        /// <param name="transitionData"></param>
        /// <param name="completion"></param>
        protected virtual void PerformInvokedTransition(ViewControllerTransitionIdentifier transitionIdentifier, TTransitionData transitionData, System.Action completion) { }

        /// <summary>
        /// Container view controllers may call this method to execute a transition between children. Both the to and from view controllers must be members of this view controller's 'children' list. This method will automatically make this view controller the 'owner' of the transition. This method will store the transition internally throughout its life-cycle. Upon completion it will be destroyed. When a view controller is destroyed, any executing transitions will be cancelled.
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="toViewController"></param>
        /// <param name="fromViewController"></param>
        /// <param name="animated"></param>
        /// <param name="interactive"></param>
        /// <param name="intermediaryViewControllers"></param>
        /// <param name="OnTransitionBegan"></param>
        /// <param name="OnTransitionWillFinish"></param>
        /// <param name="OnTransitionDidFinish"></param>
        protected void PerformChildTransition(ViewControllerTransitionIdentifier identifier, TViewController toViewController, TViewController fromViewController, bool animated, bool interactive, Stack<TViewController> intermediaryViewControllers = null, UnityAction<TTransition> OnTransitionBegan = null, UnityAction<TTransition, bool> OnTransitionWillFinish = null, UnityAction<TTransition, bool> OnTransitionDidFinish = null)
        {
            if (CanPerformChildTransition(identifier, toViewController, fromViewController, out Error error) == false)
            {
                error.LogWithPrefix("Cannot perform transition between child view controllers.");
                return;
            }

            TTransition transition = new TTransition();
            TViewController ownerViewController = This;
            transition.Initialize(identifier, toViewController, fromViewController, ownerViewController, animated, interactive, intermediaryViewControllers);

            childTransitions.Add(identifier, transition);
            WillPerformChildTransition(transition);
            OnWillPerformChildTransition.Invoke(transition);
            transition.Perform(OnTransitionBegan, OnTransitionWillFinish, OnTransitionDidFinish: (t, completed) =>
            {
                var context = t.Context;
                OnTransitionDidFinish?.Invoke(t, completed);
                context.OwnerViewController.DidPerformChildTransition(t, completed);
                context.OwnerViewController.OnDidPerformChildTransition.Invoke(t, completed);
                context.OwnerViewController.childTransitions.Remove(context.Identifier);
            });
        }

        /// <summary>
        /// Override this method to be notified just before the view controller performs a transition between two of its child view controllers.
        /// </summary>
        /// <param name="transition"></param>
        protected virtual void WillPerformChildTransition(TTransition transition) { }
        /// <summary>
        /// Override this method to be notified just after the view controller has performed a transition between two of its child view controllers.
        /// </summary>
        /// <param name="transition"></param>
        /// <param name="completed"></param>
        protected virtual void DidPerformChildTransition(TTransition transition, bool completed) { }

        /// <summary>
        /// Retrieve any child transition in progress. If a transition between child view controllers is in progress, returns true and populates <paramref name="transition"/> with the transition object. Otherwise, returns false and <paramref name="transition"/> will be null. This can be useful if your container only supports one transition at a time, such as the Tab Bar and Stack controllers. 
        /// </summary>
        /// <param name="transition"></param>
        /// <returns>True if a child transition is in progress. False otherwise.</returns>
        protected bool TryGetAnyChildTransition(out TTransition transition)
        {
            var enumerator = childTransitions.GetEnumerator();
            enumerator.MoveNext();
            transition = enumerator.Current.Value;
            return (transition != null);
        }

        /// <summary>
        /// Retrieve a specific child transition in progress. If a transition between child view controllers of the specified <paramref name="identifier"/> is in progress, returns true and populates <paramref name="transition"/> with the transition object. Otherwise, returns false and <paramref name="transition"/> will be null. This can be useful if your container supports multiple concurrent child transitions.
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="transition"></param>
        /// <returns>True if a child transition with the specified identifier is in progress. False otherwise.</returns>
        protected bool TryGetChildTransition(ViewControllerTransitionIdentifier identifier, out TTransition transition)
        {
            return childTransitions.TryGetValue(identifier, out transition);
        }

        /// <summary>
        /// Query the view controller's parent hierarchy for the first ancestor of type <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>The closest ancestor view controller of type <typeparamref name="T"/></returns>
        protected T FirstAncestorOfType<T>()
            where T : TViewController
        {
            T ancestorOfType = null;

            TViewController parent = Parent;
            while (parent != null)
            {
                if (parent is T)
                {
                    ancestorOfType = (T)parent;
                    break;
                }

                parent = parent.Parent;
            }

            return ancestorOfType;
        }

        private void LoadViewIfRequired()
        {
            if (ViewIsLoaded)
            {
                return;
            }

            LoadView();
            OnLoadView.Invoke();
            ViewDidLoad();
        }

        private void Present(TViewController viewController, bool animated, bool interactive, System.Action completion)
        {
            if (ForwardPresentToRootAncestorIfNecessary(viewController, animated, interactive, completion))
            {
                return;
            }

            if (CanPresentViewController(viewController, out Error error) == false)
            {
                error.LogWithPrefix("Cannot present view controller.");
                return;
            }

            // Configure relationships and add the presented view to the window.
            PresentedViewController = viewController;
            viewController.PresentingViewController = This;
            AddViewControllerViewToWindow(viewController);

            // In Present/Dismiss transitions, the 'owner' is reversed to be the view controller being presented/dismissed so it can provide the animator.
            var ownerViewController = viewController;
            PerformModalTransition(PresentTransitionIdentifier, viewController, This, ownerViewController, animated, interactive, OnTransitionDidFinish: (transition, completed) =>
            {
                var context = transition.Context;
                if (completed == false)
                {
                    // Undo the previously configured relationships and destroy the presented view controller.
                    context.FromViewController.PresentedViewController = null;
                    context.ToViewController.Destroy();
                }

                context.FromViewController.modalTransition = null;

                if (completed)
                {
                    completion?.Invoke();
                }
            });
        }

        private void Dismiss(bool animated, bool interactive, System.Action completion)
        {
            if (ForwardDismissToRootAncestorIfNecessary(animated, interactive, completion))
            {
                return;
            }

            if (ForwardDismissToPresenterIfNecessary(animated, interactive, completion))
            {
                return;
            }

            if (CanDismissViewController(out Error error) == false)
            {
                error.LogWithPrefix("Cannot dismiss view controller.");
                return;
            }

            var intermediaryViewControllers = ComputePresentedViewControllerStack(false);
            var fromViewController = intermediaryViewControllers.Pop();

            // In Present/Dismiss transitions, the 'owner' is reversed to be the view controller being presented/dismissed so it can provide the animator.
            var ownerViewController = fromViewController;
            PerformModalTransition(DismissTransitionIdentifier, This, fromViewController, ownerViewController, animated, interactive, intermediaryViewControllers,
                OnTransitionBegan: (transition) =>
                {
                    // Hide all intermediary view controllers.
                    var context = transition.Context;
                    foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                    {
                        intermediaryViewController.AsAppearable.BeginAppearanceTransition(false);
                        intermediaryViewController.View.Visible = false;
                        intermediaryViewController.AsAppearable.EndAppearanceTransition();
                    }
                },
                OnTransitionDidFinish: (transition, completed) =>
                {
                    var context = transition.Context;
                    if (completed)
                    {
                        // Destroy all intermediary view controllers, configure relationships, and destroy the 'from' view controller.
                        foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                        {
                            intermediaryViewController.Destroy();
                        }

                        context.ToViewController.PresentedViewController = null;
                        context.FromViewController.Destroy();
                    }
                    else
                    {
                        // Intermediary view controllers appear back on screen without being destroyed.
                        foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                        {
                            intermediaryViewController.AsAppearable.BeginAppearanceTransition(true);
                            intermediaryViewController.View.Visible = true;
                            intermediaryViewController.AsAppearable.EndAppearanceTransition();
                        }
                    }

                    context.ToViewController.modalTransition = null;

                    if (completed)
                    {
                        completion?.Invoke();
                    }
                });
        }

        private Stack<TViewController> ComputePresentedViewControllerStack(bool includeThis)
        {
            var stack = new Stack<TViewController>();
            if (includeThis)
            {
                stack.Push(This);
            }

            var presentedViewController = RootAncestor.PresentedViewController;
            while (presentedViewController != null)
            {
                stack.Push(presentedViewController);
                presentedViewController = presentedViewController.PresentedViewController;
            }

            return stack;
        }

        private void RemoveChild(TViewController viewController)
        {
            children.Remove(viewController);
            viewController.Parent = null;
        }

        private void AddViewControllerViewToWindow(TViewController viewController)
        {
            var window = view.Window;
            window.Add(viewController.View);
            viewController.View.FitToParent();
        }

        private void PerformModalTransition(ViewControllerTransitionIdentifier identifier, TViewController toViewController, TViewController fromViewController, TViewController ownerViewController, bool animated, bool interactive, Stack<TViewController> intermediaryViewControllers = null, UnityAction<TTransition> OnTransitionBegan = null, UnityAction<TTransition, bool> OnTransitionWillFinish = null, UnityAction<TTransition, bool> OnTransitionDidFinish = null)
        {
            modalTransition = new TTransition();
            modalTransition.Initialize(identifier, toViewController, fromViewController, ownerViewController, animated, interactive, intermediaryViewControllers);
            modalTransition.Perform(OnTransitionBegan, OnTransitionWillFinish, OnTransitionDidFinish);
        }

        private void CancelAllChildTransitions()
        {
            foreach (var childTransition in new List<TTransition>(childTransitions.Values))
            {
                childTransition.ForceImmediateCancellation();
            }
        }

        private void DestroyChildren()
        {
            for (int i = children.Count - 1; i >= 0; i--)
            {
                TViewController viewController = children[i];
                viewController.Destroy();
            }
        }

        private bool ForwardPresentToRootAncestorIfNecessary(TViewController viewController, bool animated, bool interactive, System.Action completion)
        {
            bool handled = false;
            if (Parent != null)
            {
                RootAncestor.Present(viewController, animated, interactive, completion);
                handled = true;
            }

            return handled;
        }

        private bool ForwardDismissToRootAncestorIfNecessary(bool animated, bool interactive, System.Action completion)
        {
            bool handled = false;
            if (Parent != null)
            {
                RootAncestor.Dismiss(animated, interactive, completion);
                handled = true;
            }

            return handled;
        }

        private bool ForwardDismissToPresenterIfNecessary(bool animated, bool interactive, System.Action completion)
        {
            bool handled = false;
            if ((PresentedViewController == null) && (PresentingViewController != null))
            {
                PresentingViewController.Dismiss(animated, interactive, completion);
                handled = true;
            }

            return handled;
        }

        private bool CanPerformModalTransition(out Error error)
        {
            if (ViewIsLoaded == false)
            {
                error = new Error("Transition owner's view has not been loaded.");
                return false;
            }

            if (IsPerformingModalTransition)
            {
                error = new Error("Transition owner is already performing a Presentation/Dismissal transition.", Error.Severity.Log);
                return false;
            }

            if ((PresentingViewController != null) && (PresentingViewController.IsPerformingModalTransition))
            {
                error = new Error("View controller is currently involved in a Presentation/Dismissal transition.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }

        private bool CanPerformChildTransition(ViewControllerTransitionIdentifier identifier, TViewController toViewController, TViewController fromViewController, out Error error)
        {
            if (toViewController != null)
            {
                if (children.Contains(toViewController) == false)
                {
                    error = new Error("The to-view-controller is not a child view controller.");
                    return false;
                }

                if (toViewController.ViewIsLoaded == false)
                {
                    error = new Error("The to-view-controller's view has not been loaded. Add it to the hierarchy before performing a transition.");
                    return false;
                }
            }

            if (fromViewController != null)
            {
                if (children.Contains(fromViewController) == false)
                {
                    error = new Error("The from-view-controller is not a child view controller.");
                    return false;
                }

                if (fromViewController.ViewIsLoaded == false)
                {
                    error = new Error("The from-view-controller's view has not been loaded. Add it to the hierarchy before performing a transition.");
                    return false;
                }
            }

            if (TryGetChildTransition(identifier, out var transition))
            {
                error = new Error("The owner view controller is already performing a child transition with this identifier.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }

        // Child transition events are intentionally not serializable to prevent them being editable in the inspector. They should be subscribed to on the instantiated instance, not the scriptable object template.
        public class WillPerformChildTransitionEvent : UnityEvent<TTransition> { }
        public class DidPerformChildTransitionEvent : UnityEvent<TTransition, bool> { }
    }

    // View controller appearance.
    public abstract partial class ViewController<TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph> : ScriptableObject, IViewControllerAppearance
    {
        IViewControllerAppearance IViewControllerAppearance.Parent
        {
            get
            {
                return Parent as IViewControllerAppearance;
            }
        }

        List<IViewControllerAppearance> IViewControllerAppearance.Children
        {
            get
            {
                var iChildren = new List<IViewControllerAppearance>(children);
                return iChildren;
            }
        }

        ViewControllerAppearance IViewControllerAppearance.Appearance
        {
            get
            {
                return appearance;
            }
        }

        bool IViewControllerAppearance.ViewIsLoaded
        {
            get
            {
                return ViewIsLoaded;
            }
        }

        void IViewControllerAppearance.ViewWillAppear()
        {
            ViewWillAppear();
        }

        void IViewControllerAppearance.ViewDidAppear()
        {
            ViewDidAppear();
        }

        void IViewControllerAppearance.ViewWillDisappear()
        {
            ViewWillDisappear();
        }

        void IViewControllerAppearance.ViewDidDisappear()
        {
            ViewDidDisappear();
        }

        void IAppearable.BeginAppearanceTransition(bool isAppearing)
        {
            appearance.BeginTransition(this as IViewControllerAppearance, isAppearing);
        }

        void IAppearable.EndAppearanceTransition()
        {
            appearance.EndTransition(this as IViewControllerAppearance);
        }

        public IAppearable AsAppearable
        {
            get
            {
                return this as IAppearable;
            }
        }
    }

    public abstract partial class ViewController<TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph> : ScriptableObject, ITransitionContextViewController
    {
        bool ITransitionContextViewController.Opaque
        {
            get
            {
                return opaque;
            }
        }

        void ITransitionContextViewController.SetViewVisible(bool visible)
        {
            View.Visible = visible;
        }
    }

#if UNITY_EDITOR
    public interface IViewController
    {
        IViewResource ViewResource { get; }
    }

    public abstract partial class ViewController<TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph> : ScriptableObject, IViewController
    {
        IViewResource IViewController.ViewResource
        {
            get
            {
                return viewResource;
            }
        }
    }
#endif
}
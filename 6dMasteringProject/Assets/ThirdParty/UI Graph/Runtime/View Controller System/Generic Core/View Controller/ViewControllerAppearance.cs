﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    internal class ViewControllerAppearance
    {
        public enum AppearanceState
        {
            NotVisible,
            Disappearing,
            Appearing,
            Visible
        }

        public AppearanceState State { get; private set; } = AppearanceState.NotVisible;
        public bool ExcludedFromAppearancePropagation { get; set; } = false;

        public void BeginTransition(IViewControllerAppearance viewController, bool isAppearing)
        {
            if (CanBeginTransition(viewController, isAppearing, out bool hasAnyParentWithAppearanceStateNotVisible) == false)
            {
                if (hasAnyParentWithAppearanceStateNotVisible)
                {
                    UpdateAppearancePropagationExclusion(isAppearing);
                }
                return;
            }

            if (isAppearing)
            {
                State = AppearanceState.Appearing;
                viewController.ViewWillAppear();
            }
            else
            {
                State = AppearanceState.Disappearing;
                viewController.ViewWillDisappear();
            }

            ForwardBeginTransitionToChildrenIfRequired(viewController.Children, isAppearing);
        }

        public void EndTransition(IViewControllerAppearance viewController)
        {
            if (CanEndTransition(viewController) == false)
            {
                return;
            }

            switch (State)
            {
                case AppearanceState.Appearing:
                {
                    State = AppearanceState.Visible;
                    viewController.ViewDidAppear();
                    break;
                }

                case AppearanceState.Disappearing:
                {
                    State = AppearanceState.NotVisible;
                    viewController.ViewDidDisappear();
                    break;
                }

                default: break;
            }

            ForwardEndTransitionToChildrenIfRequired(viewController.Children);
        }

        private bool CanBeginTransition(IViewControllerAppearance viewController, bool isAppearing, out bool hasAnyParentWithAppearanceStateNotVisible)
        {
            hasAnyParentWithAppearanceStateNotVisible = HasAnyParentWithAppearanceState(viewController, AppearanceState.NotVisible);
            if (viewController.ViewIsLoaded == false)
            {
                return false;
            }

            bool isDisappearing = (isAppearing == false);
            if ((isAppearing && ((State == AppearanceState.Visible) || (State == AppearanceState.Appearing))) ||
                (isDisappearing && ((State == AppearanceState.NotVisible) || (State == AppearanceState.Disappearing))))
            {
                return false;
            }

            if (isAppearing && hasAnyParentWithAppearanceStateNotVisible)
            {
                return false;
            }

            return true;
        }

        private void ForwardBeginTransitionToChildrenIfRequired(List<IViewControllerAppearance> children, bool isAppearing)
        {
            // Exclude 'not visible' children from appearance propagation.
            foreach (var child in children)
            {
                if (isAppearing)
                {
                    if (child.Appearance.ExcludedFromAppearancePropagation == false)
                    {
                        child.BeginAppearanceTransition(isAppearing);
                    }
                }
                else
                {
                    if (child.Appearance.State == AppearanceState.Visible || child.Appearance.State == AppearanceState.Appearing)
                    {
                        child.BeginAppearanceTransition(isAppearing);
                    }
                    else
                    {
                        child.Appearance.ExcludedFromAppearancePropagation = true;
                    }
                }
            }
        }

        private bool CanEndTransition(IViewControllerAppearance viewController)
        {
            if (viewController.ViewIsLoaded == false)
            {
                return false;
            }

            if ((State != AppearanceState.Appearing) &&
                (State != AppearanceState.Disappearing))
            {
                return false;
            }

            return true;
        }

        private void ForwardEndTransitionToChildrenIfRequired(List<IViewControllerAppearance> children)
        {
            foreach (var child in children)
            {
                if (child.Appearance.ExcludedFromAppearancePropagation == false)
                {
                    child.EndAppearanceTransition();
                }
                else if (State == AppearanceState.Visible)
                {
                    child.Appearance.ExcludedFromAppearancePropagation = false;
                }
            }
        }

        private bool HasAnyParentWithAppearanceState(IViewControllerAppearance viewController, AppearanceState appearanceState)
        {
            bool hasAnyParentWithAppearanceState = false;

            var p = viewController.Parent;
            while (p != null)
            {
                if (p.Appearance.State == appearanceState)
                {
                    hasAnyParentWithAppearanceState = true;
                    break;
                }

                p = p.Parent;
            }

            return hasAnyParentWithAppearanceState;
        }

        private void UpdateAppearancePropagationExclusion(bool isAppearing)
        {
            // If a view controller attempts to begin an appearance transition on a child view controller whose parent is 'not visible', its 'exclusion from appearance propagation' flag is cleared. This causes the child view controller to correctly receive will/didAppear callbacks when the parent is subsequently shown and forwards its appearance methods to its children.
            // If a view controller attempts to begin a disappearance transition on a child view controller whose parent is 'not visible', then its 'exclusion from appearance propagation' flag is enabled. This causes the child view controller to correctly no longer receive will/didAppear callbacks when the parent is subsequently shown and forwards its appearance methods to its children. However, the child view controller will not receive will/didDisappear callbacks in this scenario.
            if (isAppearing)
            {
                ExcludedFromAppearancePropagation = false;
            }
            else
            {
                ExcludedFromAppearancePropagation = true;
            }
        }
    }
}
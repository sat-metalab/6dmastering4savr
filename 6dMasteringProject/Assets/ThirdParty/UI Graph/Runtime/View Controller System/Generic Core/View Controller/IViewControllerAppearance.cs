﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    internal interface IViewControllerAppearance : IAppearable
    {
        IViewControllerAppearance Parent { get; }
        List<IViewControllerAppearance> Children { get; }
        ViewControllerAppearance Appearance { get; }
        bool ViewIsLoaded { get; }

        void ViewWillAppear();
        void ViewDidAppear();
        void ViewWillDisappear();
        void ViewDidDisappear();
    }
}
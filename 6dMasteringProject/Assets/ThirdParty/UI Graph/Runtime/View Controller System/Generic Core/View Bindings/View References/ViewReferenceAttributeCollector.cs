﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Reflection;
using UnityEngine;

namespace Pelican7.UIGraph
{
    public static class ViewReferenceAttributeCollector
    {
        public static FieldInfo[] CollectViewReferenceAttributedFields(Object obj)
        {
            return ObjectAttributeCollector.CollectAttributedFieldsOnObject<ViewReferenceAttribute>(obj, ViewReferenceAttribute.FieldBindingFlags);
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System;
using System.Reflection;
using System.Text;

namespace Pelican7.UIGraph
{
    public static class MethodInfoExtensions
    {
        public static string GenerateIdentifier(this MethodInfo methodInfo)
        {
            ParameterInfo[] parameters = methodInfo.GetParameters();
            var parametersStringBuilder = new StringBuilder();
            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo parameter = parameters[i];
                parametersStringBuilder.Append(parameter.ParameterType.AssemblyQualifiedName);

                if (i < (parameters.Length - 1))
                {
                    parametersStringBuilder.Append(", ");
                }
            }

            string methodName = methodInfo.Name;
            return string.Format("{0}({1})", methodName, parametersStringBuilder.ToString());
        }

        public static string GenerateDisplayName(this MethodInfo methodInfo)
        {
            ParameterInfo[] parameters = methodInfo.GetParameters();
            var parametersStringBuilder = new StringBuilder();
            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo parameter = parameters[i];
                parametersStringBuilder.Append(parameter.ParameterType.Name);

                if (i < (parameters.Length - 1))
                {
                    parametersStringBuilder.Append(", ");
                }
            }

            string methodName = methodInfo.Name;
            return string.Format("{0}({1})", methodName, parametersStringBuilder.ToString());
        }

        public static string[] AssemblyQualifiedParameterTypes(this MethodInfo methodInfo)
        {
            ParameterInfo[] parameters = methodInfo.GetParameters();
            string[] assemblyQualifiedParameterNames = new string[parameters.Length];

            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo parameter = parameters[i];
                assemblyQualifiedParameterNames[i] = parameter.ParameterType.AssemblyQualifiedName;
            }

            return assemblyQualifiedParameterNames;
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A base class for transition animators in the UI Canvas workflow. Transition animators animate the transitions between view controllers.
    /// </summary>
    public abstract class CanvasControllerTransitionAnimator : ScriptableObject, IViewControllerTransitionAnimator<CanvasControllerTransitionContext>
    {
        public abstract float TransitionDuration(CanvasControllerTransitionContext context);
        public abstract ViewControllerTransitionTimeUpdateMode TransitionTimeUpdateMode(CanvasControllerTransitionContext context);
        public abstract void ConfigureTransitionAnimation(CanvasControllerTransitionContext context);
        public abstract void UpdateTransitionAnimation(CanvasControllerTransitionContext context, float progress01);
        public virtual void OnTransitionFinished(CanvasControllerTransitionContext context, bool completed) { }
    }
}
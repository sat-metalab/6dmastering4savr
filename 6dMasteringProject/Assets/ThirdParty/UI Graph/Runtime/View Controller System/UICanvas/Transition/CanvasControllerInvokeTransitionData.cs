﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A CanvasControllerInvokeTransitionData is used to pass transition data to the <see cref="ViewController{TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph}.InvokeTransition(ViewControllerTransitionIdentifier, TTransitionData, System.Action)"/> method directly.
    /// <para>
    /// You may specify your own subclass if you have custom data you wish to pass. See the <see cref="TabBarCanvasController"/> for an example of this.
    /// </para>
    /// </summary>
    public class CanvasControllerInvokeTransitionData : ViewControllerInvokeTransitionData<CanvasController>
    {
        // Parameterless constructor is used by the generic view controller.
        public CanvasControllerInvokeTransitionData() { }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A base class for transition animator providers in the UI Canvas workflow. Transition Animator providers provide animators to the view controller system when a transition is to be performed.
    /// </summary>
    public abstract class CanvasControllerTransitionAnimatorProvider : ScriptableObject, IViewControllerTransitionAnimatorProvider<CanvasControllerTransitionContext>
    {
        public abstract IViewControllerTransitionAnimator<CanvasControllerTransitionContext> AnimatorForTransition(CanvasControllerTransitionContext context);
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// When a Canvas Controller transition is to be performed, UI Graph creates a transition object to describe the transition. This object is passed to methods that require it, such as <see cref="ViewController{TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph}.PrepareForGraphTransition(GraphTransition{TViewController})"/>, providing you with the relevant transition information.
    /// </summary>
    public class CanvasControllerTransition : ViewControllerTransition<CanvasController, CanvasControllerTransition, CanvasControllerTransitionContext, CanvasControllerTransitionAnimatorProvider, ICanvasControllerTransitionProgressProvider, CanvasControllerTransitionAnimationDriver, CanvasControllerTransitionProgressProvider>
    {
        // Parameterless constructor is used by the generic view controller.
        public CanvasControllerTransition() { }
        public CanvasControllerTransition(ViewControllerTransitionIdentifier identifier, CanvasController toViewController, CanvasController fromViewController, CanvasController ownerViewController, bool animated, bool interactive = false, Stack<CanvasController> intermediaryViewControllers = null)
        {
            Initialize(identifier, toViewController, fromViewController, ownerViewController, animated, interactive, intermediaryViewControllers);
        }

        public override void Initialize(ViewControllerTransitionIdentifier identifier, CanvasController toViewController, CanvasController fromViewController, CanvasController ownerViewController, bool animated, bool interactive = false, Stack<CanvasController> intermediaryViewControllers = null)
        {
            context = new CanvasControllerTransitionContext(identifier, toViewController, fromViewController, ownerViewController, animated, interactive, intermediaryViewControllers);
        }

        protected override CanvasControllerTransitionAnimatorProvider TransitionAnimatorProvider
        {
            get
            {
                return context.OwnerViewController.transitionAnimatorProvider;
            }
        }

        protected override ICanvasControllerTransitionProgressProvider InteractiveTransitionProgressProvider
        {
            get
            {
                return context.OwnerViewController.interactiveTransitionProgressProvider;
            }
        }
    }
}
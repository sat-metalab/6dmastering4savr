﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The ICanvasControllerTransitionProgressProvider interface is used when performing an interactive transition. Implement the ICanvasControllerTransitionProgressProvider interface to manually drive the transition yourself, such as from a input swipe's position. Supply your progress provider object to UI Graph via the view controller's <see cref="ViewController{TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph}.interactiveTransitionProgressProvider"/>.
    /// </summary>
    public interface ICanvasControllerTransitionProgressProvider : IViewControllerTransitionProgressProvider<CanvasControllerTransitionContext> { }
}
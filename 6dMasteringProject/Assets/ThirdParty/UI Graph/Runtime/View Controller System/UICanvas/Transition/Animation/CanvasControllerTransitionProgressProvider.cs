﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections;
using UnityEngine;

namespace Pelican7.UIGraph
{
    public class CanvasControllerTransitionProgressProvider : ICanvasControllerTransitionProgressProvider, IViewControllerTransitionCompletionProgressProvider
    {
        private CanvasControllerTransitionProgressProviderBehaviour behaviour;
        private IEnumerator routine;
        private IViewControllerTransitionDrivable<CanvasControllerTransitionContext> drivable;

        void IViewControllerTransitionProgressProvider<CanvasControllerTransitionContext>.ProvideProgressToDrivable(IViewControllerTransitionDrivable<CanvasControllerTransitionContext> drivable)
        {
            this.drivable = drivable;
            Run();
        }

        void IViewControllerTransitionProgressProvider<CanvasControllerTransitionContext>.CancelProgressUpdatesToDrivable(IViewControllerTransitionDrivable<CanvasControllerTransitionContext> drivable)
        {
            StopRoutine();
        }

        public float Duration
        {
            get
            {
                return drivable.Duration;
            }
        }

        public ViewControllerTransitionAnimationDriverDirection Direction
        {
            get
            {
                return drivable.Direction;
            }
        }

        public AnimationCurve ProgressCurve { get; set; }

        private ViewControllerTransitionTimeUpdateMode TimeUpdateMode
        {
            get
            {
                return drivable.TimeUpdateMode;
            }
        }

        private CanvasControllerTransitionProgressProviderBehaviour Behaviour
        {
            get
            {
                if (behaviour == null || behaviour.isBeingDestroyed)
                {
                    GameObject gameObject = new GameObject("Routine");
                    behaviour = gameObject.AddComponent<CanvasControllerTransitionProgressProviderBehaviour>();
                    Object.DontDestroyOnLoad(gameObject);
                }

                return behaviour;
            }
        }

        private void Run()
        {
            routine = RunRoutine();
            Behaviour.StartCoroutine(routine);
        }

        private IEnumerator RunRoutine()
        {
            float startProgress = drivable.Progress01;
            float completedProgress = (Direction.IsForwards()) ? 1f : 0f;
            float remainingProgress = completedProgress - startProgress;
            float time = startProgress * Duration;
            while ((time <= Duration) && (time >= 0f))
            {
                float progress01 = time / Duration;

                // The progress curve is applied over the remaining progress only. This means that when the progress provider picks up an in-progress transition, such as to complete/cancel an interactive transition, the progress curve is applied from that point onwards.
                if (ProgressCurve != null)
                {
                    float curveProgress01 = Mathf.InverseLerp(startProgress, completedProgress, progress01);
                    progress01 = startProgress + (remainingProgress * ProgressCurve.Evaluate(curveProgress01));
                }

                drivable.Progress01 = progress01;

                yield return null;

                float deltaTime = (TimeUpdateMode == ViewControllerTransitionTimeUpdateMode.ScaledTime) ? Time.deltaTime : Time.unscaledDeltaTime;
                time += (Direction.IsForwards()) ? deltaTime : -deltaTime;
            }

            drivable.Progress01 = completedProgress;
            yield return null;

            if (Direction.IsForwards())
            {
                drivable.CompleteTransition();
            }
            else
            {
                drivable.CancelTransition();
            }

            CleanUp();
        }

        private void StopRoutine()
        {
            if (behaviour != null)
            {
                behaviour.StopCoroutine(routine);
                CleanUp();
            }
        }

        private void CleanUp()
        {
            behaviour.Destroy();
            routine = null;
        }

        private class CanvasControllerTransitionProgressProviderBehaviour : MonoBehaviour
        {
            public bool isBeingDestroyed;

            public void Destroy()
            {
                isBeingDestroyed = true;
                Destroy(gameObject);
            }
        }
    }
}
// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// Use a scene transition canvas controller to perform a transition from one scene to another.
    /// <para>
    ///  Upon presentation, a scene transition canvas controller moves the current UI window to a standalone scene and ensures it renders on top of any subsequently loaded UI windows. It then unloads the current scene, loads the scene specified in <see cref="sceneNameToLoad"/>, and dismisses the window from which it was presented.
    ///  </para>
    /// <para>
    /// The scene transition canvas controller should be derived from by an existing canvas controller. You are responsible for configuring the <see cref="sceneNameToLoad"/>, which can be done statically in the inspector or dynamically upon instantiation, such as in <see cref="ViewController{TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph}.PrepareForGraphTransition(GraphTransition{TViewController})"/>.
    /// </para>
    /// <para>
    /// You may override the <see cref="PrepareSceneForUnload(Scene)"/> and <see cref="PrepareLoadedScene(Scene)"/> methods to perform any additional unloading/loading requirements.
    /// </para>
    /// <para>
    /// Ensure that any scene name specified has been added to Unity's 'Scenes In Build' in the Build Settings.
    /// </para>
    /// </summary>
    public class SceneTransitionCanvasController : CanvasController
    {
        private const int MaximumCanvasDepth = 100;

        /// <summary>
        /// The name of the scene to load. Ensure that any scene name specified has been added to Unity's 'Scenes In Build' in the Build Settings.
        /// </summary>
        public string sceneNameToLoad;
        /// <summary>
        /// Should the window in which the SceneTransitionCanvasController resides be animated upon dismissal?
        /// </summary>
        public bool animateWindowDismissal = true;

        /// <summary>
        /// Initialize the SceneTransitionCanvasController with a <paramref name="sceneNameToLoad"/>.
        /// </summary>
        /// <param name="sceneNameToLoad"></param>
        public virtual void Initialize(string sceneNameToLoad)
        {
            this.sceneNameToLoad = sceneNameToLoad;
        }

        /// <summary>
        /// Derived classes can override this method to perform any custom unloading logic. This method is called with the scene to be unloaded, prior to it being unloaded.
        /// </summary>
        /// <param name="scene"></param>
        /// <returns>A yield instruction.</returns>
        protected virtual IEnumerator PrepareSceneForUnload(Scene scene)
        {
            yield return null;
        }

        /// <summary>
        /// Derived classes can override this method to perform any custom loading logic. This method is called with the newly loaded scene, prior to it becoming the active scene and the window being dismissed.
        /// </summary>
        /// <param name="scene"></param>
        /// <returns>A yield instruction.</returns>
        protected virtual IEnumerator PrepareLoadedScene(Scene scene)
        {
            yield return null;
        }

        protected override void ViewDidAppear()
        {
            base.ViewDidAppear();

            if (CanLoadScene(out Error error) == false)
            {
                error.LogWithPrefix("Cannot load scene.");
                return;
            }

            View.StartCoroutine(LoadSceneRoutine(sceneNameToLoad));
        }

        private bool CanLoadScene(out Error error)
        {
            if (string.IsNullOrEmpty(sceneNameToLoad))
            {
                error = new Error("No scene name to load has been set.");
                return false;
            }

            error = null;
            return true;
        }

        private IEnumerator LoadSceneRoutine(string sceneName)
        {
            Scene loadingScene = CreateAndConfigureLoadingScene(out CanvasWindow window);
            yield return UnloadActiveScene();
            yield return LoadSceneAndMakeActive(sceneName);

            window.Dismiss(animateWindowDismissal, () =>
            {
                SceneManager.UnloadSceneAsync(loadingScene);
            });
        }

        private Scene CreateAndConfigureLoadingScene(out CanvasWindow window)
        {
            // Create a new scene to hold the current window whilst we perform the unload/load.
            Scene loadingScene = SceneManager.CreateScene("Loading UI Container");

            // Move the UI window to the newly created scene.
            window = View.Window;
            window.transform.SetParent(null); // Ensure the window is a root object before we move it.
            SceneManager.MoveGameObjectToScene(window.gameObject, loadingScene);

            // Ensure the window stays in front of any UI windows in the scene to be loaded.
            window.Depth = MaximumCanvasDepth;

            return loadingScene;
        }

        private IEnumerator UnloadActiveScene()
        {
            Scene previousScene = SceneManager.GetActiveScene();

            // Allow derived classes to perform any additional unloading requirements.
            yield return PrepareSceneForUnload(previousScene);

            // Unload the previous scene.
            AsyncOperation loadOperation = SceneManager.UnloadSceneAsync(previousScene);
            yield return loadOperation;
        }

        private IEnumerator LoadSceneAndMakeActive(string sceneName)
        {
            // Load the new scene.
            AsyncOperation loadOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            yield return loadOperation;

            Scene loadedScene = SceneManager.GetSceneByName(sceneName);

            // Allow derived classes to perform any additional loading requirements.
            yield return PrepareLoadedScene(loadedScene);

            // Make the newly loaded scene the active scene.
            SceneManager.SetActiveScene(loadedScene);
        }
    }
}
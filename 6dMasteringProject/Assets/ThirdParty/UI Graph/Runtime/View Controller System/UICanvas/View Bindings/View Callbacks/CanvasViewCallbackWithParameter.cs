﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A generic base class for canvas view callbacks that expose a method with a single parameter of type <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class CanvasViewCallbackWithParameter<T> : CanvasViewCallback
    {
        public void Invoke(T parameter)
        {
            InvokeOnTarget(new object[] { parameter });
        }
    }
}
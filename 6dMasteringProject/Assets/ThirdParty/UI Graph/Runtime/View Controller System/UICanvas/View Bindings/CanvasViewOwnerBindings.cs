﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.Events;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The CanvasViewOwnerBindings component stores bindings between a view controller and its view, such as <see cref="CanvasViewReference"/> and <see cref="CanvasViewCallback"/> objects. The CanvasViewOwnerBindings component will automatically update its <see cref="viewReferences"/> and <see cref="viewCallbacks"/> to reflect its view controller's script file.
    /// </summary>
    public class CanvasViewOwnerBindings : MonoBehaviour, IViewBindable
    {
        /// <summary>
        /// A list of <see cref="CanvasViewReference"/> objects. This list is automatically populated with all of the view controller's fields that are decorated with the [ViewReference] attribute.
        /// </summary>
        public CanvasViewReference[] viewReferences;
        /// <summary>
        /// A list of <see cref="CanvasViewCallback"/> components. This list is automatically populated with all of the view controller's methods that are decorated with the [ViewCallback] attribute.
        /// </summary>
        public CanvasViewCallback[] viewCallbacks;
#if UNITY_EDITOR
        public OwnerData ownerData;
#endif

        /// <summary>
        /// This event is invoked when the view is bound to its owning view controller upon load.
        /// </summary>
        public BindingEvent OnBoundToOwner;

        void IViewBindable.Bind(Object obj)
        {
            var owner = obj as CanvasController;
            if (CanBindToOwner(owner, out Error error) == false)
            {
                error.LogWithPrefix("Cannot bind to object.");
                return;
            }

            BindReferencesToOwner(owner);
            BindCallbacksToOwner(owner);
            OnBoundToOwner.Invoke(owner);
        }

        /// <summary>
        /// Retrieve the <see cref="CanvasViewReference"/> with the specified <paramref name="identifier"/>.
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns>The <see cref="CanvasViewReference"/> with the specified <paramref name="identifier"/>, or null if none exists.</returns>
        public CanvasViewReference ReferenceWithIdentifier(string identifier)
        {
            CanvasViewReference reference = null;
            foreach (CanvasViewReference r in viewReferences)
            {
                if (r.Identifier.Equals(identifier))
                {
                    reference = r;
                    break;
                }
            }

            return reference;
        }

        /// <summary>
        /// Retrieve the <see cref="CanvasViewCallback"/> with the specified <paramref name="identifier"/>.
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns>The <see cref="CanvasViewCallback"/> with the specified <paramref name="identifier"/>, or null if none exists.</returns>
        public CanvasViewCallback CallbackWithIdentifier(string identifier)
        {
            CanvasViewCallback callback = null;
            foreach (CanvasViewCallback c in viewCallbacks)
            {
                if ((c != null) && c.Identifier.Equals(identifier))
                {
                    callback = c;
                    break;
                }
            }

            return callback;
        }

        private void BindReferencesToOwner(CanvasController owner)
        {
            var viewReferenceFields = ViewReferenceAttributeCollector.CollectViewReferenceAttributedFields(owner);
            foreach (var viewReferenceField in viewReferenceFields)
            {
                string identifier = viewReferenceField.Name;
                var reference = ReferenceWithIdentifier(identifier);
                if (reference != null)
                {
                    if (reference.HasObjectReference)
                    {
                        viewReferenceField.SetValue(owner, reference.objectReference);
                    }
                    else
                    {
                        Error.Log(string.Format("View reference '{0}' has no object reference and will not be bound.", identifier), Error.Severity.Warning);
                    }
                }
                else
                {
                    Error.Log(string.Format("View reference '{0}' was not found in the view and will not be bound.", identifier), Error.Severity.Warning);
                }
            }
        }

        private void BindCallbacksToOwner(CanvasController owner)
        {
            var viewCallbackMethods = ViewCallbackAttributeCollector.CollectViewCallbackAttributedMethods(owner);
            foreach (var viewCallbackMethod in viewCallbackMethods)
            {
                string identifier = viewCallbackMethod.GenerateIdentifier();
                var callback = CallbackWithIdentifier(identifier) as IViewBindable;
                if (callback != null)
                {
                    callback.Bind(owner);
                }
                else
                {
                    Error.Log(string.Format("View callback '{0}' was not found in the view and will not be bound.", identifier), Error.Severity.Warning);
                }
            }
        }

        private bool CanBindToOwner(CanvasController owner, out Error error)
        {
            if (owner == null)
            {
                error = new Error("Object passed to bind was not a canvas controller.");
                return false;
            }

            if (owner.ViewIsLoaded == false)
            {
                error = new Error("The view has not been loaded.");
                return false;
            }

            error = null;
            return true;
        }

#if UNITY_EDITOR
        [System.Serializable]
        public class OwnerData
        {
            public CanvasController asset;

            public OwnerData(CanvasController asset)
            {
                this.asset = asset;
            }
        }
#endif

        [System.Serializable]
        public class BindingEvent : UnityEvent<CanvasController> { }
    }
}
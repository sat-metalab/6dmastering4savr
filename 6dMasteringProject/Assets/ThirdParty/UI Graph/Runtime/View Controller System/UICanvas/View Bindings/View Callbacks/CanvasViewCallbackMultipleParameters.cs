﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A canvas view callback that exposes a method with multiple parameters.
    /// </summary>
    /// <remarks>
    /// Note that many standard Unity UI components, such as UI.Button, cannot call methods with multiple parameters from the inspector. Instead, you may invoke it from script.
    /// </remarks>
    public class CanvasViewCallbackMultipleParameters : CanvasViewCallback
    {
        public void Invoke(params object[] parameters)
        {
            InvokeOnTarget(parameters);
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Reflection;
using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A canvas view callback exposes a canvas controller method, decorated with the <see cref="ViewCallbackAttribute"/>, to its view. You do not need to create CanvasViewCallback components in your view. One will be created automatically for every method in the corresponding view controller decorated with the <see cref="ViewCallbackAttribute"/>.
    /// </summary>
    public abstract class CanvasViewCallback : MonoBehaviour, IViewBindable
    {
        private Object target;
#pragma warning disable 0649
        [SerializeField] private SerializedMethodInfo serializedMethodInfo;
#pragma warning restore 0649

        void IViewBindable.Bind(Object obj)
        {
            target = obj;
        }

        /// <summary>
        /// The view callback's identifier.
        /// </summary>
        public string Identifier
        {
            get
            {
                return serializedMethodInfo.Identifier;
            }
        }

        /// <summary>
        /// The view callback's display name.
        /// </summary>
        public string DisplayName
        {
            get
            {
                return serializedMethodInfo.DisplayName;
            }
        }

        public void Initialize(MethodInfo methodInfo)
        {
            serializedMethodInfo = new SerializedMethodInfo(methodInfo);
        }

        /// <summary>
        /// Invoke the view callback on <see cref="target"/> with the provided <paramref name="parameters"/>.
        /// </summary>
        /// <param name="parameters"></param>
        protected void InvokeOnTarget(params object[] parameters)
        {
            if (target == null)
            {
                Error.Log("Cannot invoke view callback. It has not been bound.");
                return;
            }

            System.Type targetType = target.GetType();
            if (serializedMethodInfo.TryGetMethodOnType(targetType, out MethodInfo method))
            {
                method.Invoke(target, parameters);
            }
            else
            {
                Error.Log(string.Format("The method '{0}' was not invoked as no method exists with the invoked parameters.", serializedMethodInfo.DisplayName));
            }
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A canvas view callback that exposes a method with a single float parameter.
    /// </summary>
    public class CanvasViewCallbackFloat : CanvasViewCallbackWithParameter<float> { }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System;
using System.Reflection;
using UnityEngine;

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public class SerializedMethodInfo
    {
#pragma warning disable 0649
        [SerializeField] private string identifier;
        [SerializeField] private string methodName;
        [SerializeField] private string[] assemblyQualifiedParameterTypes;
        [SerializeField] private string displayName;
#pragma warning restore 0649

        public SerializedMethodInfo(MethodInfo methodInfo)
        {
            identifier = methodInfo.GenerateIdentifier();
            methodName = methodInfo.Name;
            assemblyQualifiedParameterTypes = methodInfo.AssemblyQualifiedParameterTypes();
            displayName = methodInfo.GenerateDisplayName();
        }

        public string Identifier
        {
            get
            {
                return identifier;
            }
        }

        public string DisplayName
        {
            get
            {
                return displayName;
            }
        }

        public bool TryGetMethodOnType(Type type, out MethodInfo methodInfo)
        {
            methodInfo = type.GetMethod(methodName, ViewCallbackAttribute.MethodBindingFlags, null, GetParameterTypes(), null);
            return (methodInfo != null);
        }

        private Type[] GetParameterTypes()
        {
            int length = assemblyQualifiedParameterTypes.Length;
            Type[] parameterTypes = new Type[length];
            for (int i = 0; i < length; i++)
            {
                string assemblyQualifiedParameterType = assemblyQualifiedParameterTypes[i];
                Type parameterType = Type.GetType(assemblyQualifiedParameterType);
                parameterTypes[i] = parameterType;
            }

            return parameterTypes;
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The Canvas Controller class is the base view controller type for working with Unity's UI Canvas workflow. It manages a view built with Unity's UI Canvas and stores this view in a Prefab asset.
    /// </summary>
    public partial class CanvasController : ViewController<CanvasController, CanvasView, CanvasWindow, CanvasViewResource, CanvasControllerTransition, CanvasControllerTransitionContext, CanvasControllerTransitionAnimatorProvider, ICanvasControllerTransitionProgressProvider, CanvasControllerTransitionAnimationDriver, CanvasControllerTransitionProgressProvider, CanvasControllerInvokeTransitionData, CanvasGraph>
    {
        /// <summary>
        /// The transition identifier for the Canvas Controller's Present transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier PresentTransition = new ViewControllerTransitionIdentifier("CanvasController.Present", ViewControllerTransitionIdentifier.TransitionType.Presentation, "Present");
        /// <summary>
        /// The transition identifier for the Canvas Controller's Dismiss transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier DismissTransition = new ViewControllerTransitionIdentifier("CanvasController.Dismiss", ViewControllerTransitionIdentifier.TransitionType.Dismissal, "Dismiss To");

        sealed protected override ViewControllerTransitionIdentifier PresentTransitionIdentifier
        {
            get
            {
                return PresentTransition;
            }
        }

        sealed protected override ViewControllerTransitionIdentifier DismissTransitionIdentifier
        {
            get
            {
                return DismissTransition;
            }
        }

        sealed public override void Destroy()
        {
            base.Destroy();
        }
    }

    public partial class CanvasController : ViewController<CanvasController, CanvasView, CanvasWindow, CanvasViewResource, CanvasControllerTransition, CanvasControllerTransitionContext, CanvasControllerTransitionAnimatorProvider, ICanvasControllerTransitionProgressProvider, CanvasControllerTransitionAnimationDriver, CanvasControllerTransitionProgressProvider, CanvasControllerInvokeTransitionData, CanvasGraph>
    {
        sealed protected override void BindActivationListenerToViewObjects(IGraphableTransitionViewObjectListener viewObjectActivationListener)
        {
            IGraphableTransitionViewObject[] graphableViewObjects = View.GetComponentsInChildren<IGraphableTransitionViewObject>(true);
            foreach (IGraphableTransitionViewObject graphableViewObject in graphableViewObjects)
            {
                graphableViewObject.BindActivationListener(viewObjectActivationListener);
            }
        }

        sealed protected override IGraphableEmbedViewObject[] FindGraphableEmbedViewObjectsInView()
        {
            return View.GetComponentsInChildren<IGraphableEmbedViewObject>(true);
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A CanvasWindow is a window for use with the UI Canvas workflow. Canvas Controllers place their view within a CanvasWindow upon load. It is responsible for rendering all child views (canvases).
    /// </summary>
    public class CanvasWindow : CanvasView
    {
        /// <summary>
        /// The window's camera
        /// </summary>
        public new Camera camera;

        private CanvasControllerTransition transition;

        /// <summary>
        /// The window's root view controller.
        /// </summary>
        public CanvasController RootViewController { get; set; }

        /// <summary>
        /// The camera's depth. When using multiple windows, use this value to override their rendering order.
        /// </summary>
        public float Depth
        {
            get
            {
                return camera.depth;
            }

            set
            {
                camera.depth = value;
            }
        }

        private bool IsPerformingTransition
        {
            get
            {
                return (transition != null);
            }
        }

        /// <summary>
        /// Present the window. You must set a root view controller prior to calling this method.
        /// </summary>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void Present(bool animated = false, System.Action completion = null)
        {
            if (CanPerformTransition(out Error error) == false)
            {
                error.LogWithPrefix("Cannot present window.");
                return;
            }

            // Add the root view to the window.
            var rootView = RootViewController.View;
            Add(rootView);
            rootView.FitToParent();

            // Transition to the root view controller.
            transition = new CanvasControllerTransition(CanvasController.PresentTransition, RootViewController, null, RootViewController, animated);
            transition.Perform(OnTransitionDidFinish: (transition, completed) =>
            {
                // Equivalent to "this.transition = null;" without capturing 'this' in lambda expression.
                CanvasControllerTransitionContext context = transition.Context;
                CanvasController owner = context.OwnerViewController;
                CanvasWindow ownerWindow = owner.View.Window;
                ownerWindow.transition = null;

                completion?.Invoke();
            });
        }

        /// <summary>
        /// Dismiss the window. Upon completion the window will be destroyed.
        /// </summary>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void Dismiss(bool animated = false, System.Action completion = null)
        {
            if (CanPerformTransition(out Error error) == false)
            {
                error.LogWithPrefix("Cannot dismiss window.");
                return;
            }

            // Transition the current view controller stack off-screen.
            Stack<CanvasController> viewControllerStack = RootViewController.PresentedViewControllerStack;
            CanvasController topViewController = viewControllerStack.Pop();
            transition = new CanvasControllerTransition(CanvasController.DismissTransition, null, topViewController, topViewController, animated, intermediaryViewControllers: viewControllerStack);
            transition.Perform(OnTransitionBegan: (transition) =>
            {
                // Hide all intermediary view controllers.
                var context = transition.Context;
                foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                {
                    intermediaryViewController.AsAppearable.BeginAppearanceTransition(false);
                    intermediaryViewController.View.Visible = false;
                    intermediaryViewController.AsAppearable.EndAppearanceTransition();
                }
            },
            OnTransitionDidFinish: (transition, completed) =>
            {
                CanvasControllerTransitionContext context = transition.Context;
                CanvasController owner = context.OwnerViewController;
                CanvasWindow ownerWindow = owner.View.Window;

                // Destroy all view controllers in the window.
                context.FromViewController.Destroy();
                foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                {
                    intermediaryViewController.Destroy();
                }

                // Destroy window.
                Destroy(ownerWindow.gameObject);

                completion?.Invoke();
            });;
        }

        private bool CanPerformTransition(out Error error)
        {
            if (RootViewController == null)
            {
                error = new Error("The window's root view controller has not been set.");
                return false;
            }

            if (IsPerformingTransition)
            {
                error = new Error("The window is already transitioning its root view controller.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A CanvasView is a view based upon a Unity UI Canvas. It is stored within a Prefab asset.
    /// </summary>
    public class CanvasView : MonoBehaviour, IView<CanvasView, CanvasWindow>, ITransform
    {
#pragma warning disable 0649
        [SerializeField] private Canvas canvas;
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private CanvasGroup canvasGroup;
#pragma warning restore 0649

        /// <summary>
        /// The view's Canvas component. At runtime, a CanvasView is always embedded within a <see cref="CanvasWindow"/>. As such, this Canvas will become a nested Canvas, inheriting its window's properties, such as Render Mode.
        /// </summary>
        public Canvas Canvas { get => canvas; }
        /// <summary>
        /// The view's RectTransform component.
        /// </summary>
        public RectTransform RectTransform { get => rectTransform; }
        /// <summary>
        /// The view's CanvasGroup component.
        /// </summary>
        public CanvasGroup CanvasGroup { get => canvasGroup; }

        Vector3 ITransform.Position
        {
            get
            {
                return rectTransform.localPosition;
            }

            set
            {
                rectTransform.localPosition = value;
            }
        }

        Quaternion ITransform.Rotation
        {
            get
            {
                return rectTransform.localRotation;
            }

            set
            {
                rectTransform.localRotation = value;
            }
        }

        Vector3 ITransform.Scale
        {
            get
            {
                return rectTransform.localScale;
            }

            set
            {
                rectTransform.localScale = value;
            }
        }

        Matrix4x4 ITransform.Matrix
        {
            get
            {
                ITransform transform = Transform;
                return Matrix4x4.TRS(transform.Position, transform.Rotation, transform.Scale);
            }
        }

        /// <summary>
        /// Is the view visible?
        /// </summary>
        public bool Visible
        {
            get
            {
                return gameObject.activeInHierarchy;

            }

            set
            {
                // Disabling a canvas component causes undefined behaviours on nested child canvases. Specifically, their layout becomes undefined and canvas groups no longer work correctly. Therefore, to toggle a canvas' visibility we change the whole game object's active state.
                gameObject.SetActive(value);
            }
        }

        /// <summary>
        /// The view's window.
        /// </summary>
        public CanvasWindow Window
        {
            get
            {
                var rootCanvas = canvas.rootCanvas;
                var canvasWindow = rootCanvas.GetComponent<CanvasWindow>();
                return canvasWindow;
            }
        }

        /// <summary>
        /// The view's alpha.
        /// </summary>
        public float Alpha
        {
            get
            {
                return CanvasGroup.alpha;
            }

            set
            {
                CanvasGroup.alpha = value;
            }
        }

        /// <summary>
        /// Is the view interactable?
        /// </summary>
        public bool Interactable
        {
            get
            {
                return CanvasGroup.interactable;
            }

            set
            {
                CanvasGroup.interactable = value;
            }
        }

        /// <summary>
        /// The view's transform.
        /// </summary>
        public ITransform Transform
        {
            get
            {
                return this as ITransform;
            }

            set
            {
                ITransform transform = Transform;
                transform.Position = value.Position;
                transform.Rotation = value.Rotation;
                transform.Scale = value.Scale;
            }
        }

        /// <summary>
        /// The view's rect.
        /// </summary>
        public Rect Rect
        {
            get
            {
                return RectTransform.rect;
            }
        }

        /// <summary>
        /// Add <paramref name="child"/> as a child.
        /// </summary>
        /// <param name="child"></param>
        public void Add(CanvasView child)
        {
            RectTransform.Add(child);
        }

        /// <summary>
        /// Fit the view to its parent's rect.
        /// </summary>
        public void FitToParent()
        {
            RectTransform.FillParent();
        }

        /// <summary>
        /// Bring the view to the front of the rendering order, causing it to be rendered first in its siblings.
        /// </summary>
        public void BringToFront()
        {
            RectTransform.SetAsLastSibling();
        }

        /// <summary>
        /// Send the view to the back of the rendering order, causing it to be rendered last in its siblings.
        /// </summary>
        public void SendToBack()
        {
            RectTransform.SetAsFirstSibling();
        }

        /// <summary>
        /// Unload the view.
        /// </summary>
        public void Unload()
        {
            Destroy(gameObject);
        }
    }
}
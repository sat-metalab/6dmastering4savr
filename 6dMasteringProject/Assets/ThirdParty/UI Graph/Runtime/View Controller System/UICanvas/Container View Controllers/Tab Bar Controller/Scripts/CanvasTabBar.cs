﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The tab bar component associated with a <see cref="TabBarCanvasController"/>.
    /// </summary>
    public class CanvasTabBar : MonoBehaviour
    {
        /// <summary>
        /// The tab bar's list of tab bar items.
        /// </summary>
        public List<CanvasTabBarItem> tabBarItems;
        /// <summary>
        /// The tab bar item template. Tab bar items will be instantiated using this template, unless the <see cref="TabBarCanvasController"/> has an <see cref="TabBarCanvasController.itemProvider"/>.
        /// </summary>
        public CanvasTabBarItem tabBarItemTemplate;
        /// <summary>
        /// The container into which instantiated tab bar items will be placed.
        /// </summary>
        public RectTransform tabBarItemsContainer;

        private readonly CanvasTabBarEvent OnTabBarItemClicked = new CanvasTabBarEvent();
        private CanvasTabBarItem selectedItem;

        /// <summary>
        /// Load the tab bar's items. This method instantiates tab bar items for any required indexes that are not already presented in the tab bar items list.
        /// </summary>
        /// <param name="tabBarController"></param>
        public void LoadTabBarItemsForTabBarController(TabBarCanvasController tabBarController)
        {
            CanvasController[] viewControllers = tabBarController.ViewControllers;
            for (int index = 0; index < viewControllers?.Length; index++)
            {
                // Users can pre-define tab bar items in the view. Therefore, only create tab bar items for view controllers that don't have an existing item.
                var tabBarItem = ExistingTabBarItemWithIndex(index);
                var viewController = viewControllers[index];
                if (tabBarItem == null)
                {
                    tabBarItem = TabBarItemForViewControllerAtIndex(tabBarController, viewController, index);
                    tabBarItems.Add(tabBarItem);
                }

                tabBarItem.ConfigureForTabBarWithViewControllerAtIndex(this, viewController, index);
                tabBarItem.BindToClick(OnReceiveTabBarItemClicked);
            }
        }

        /// <summary>
        /// Bind <paramref name="callback"/> to the 'tab bar item clicked' event.
        /// </summary>
        /// <param name="callback"></param>
        public void BindToTabBarItemClicked(UnityAction<CanvasTabBarItem> callback)
        {
            OnTabBarItemClicked.AddListener(callback);
        }

        /// <summary>
        /// Select the tab bar item at <paramref name="index"/>.
        /// </summary>
        /// <param name="index"></param>
        public void SelectTabBarItemAtIndex(int index)
        {
            selectedItem?.SetSelected(false);

            CanvasTabBarItem tabBarItem = ExistingTabBarItemWithIndex(index);
            selectedItem = tabBarItem;
            selectedItem.SetSelected(true);
        }

        /// <summary>
        /// Reload all tab bar items. This method destroys all existing tab bar items and calls <see cref="LoadTabBarItemsForTabBarController(TabBarCanvasController)"/>.
        /// </summary>
        /// <param name="tabBarController"></param>
        public void ReloadTabBarItemsForTabBarController(TabBarCanvasController tabBarController)
        {
            DestroyTabBarItems();
            LoadTabBarItemsForTabBarController(tabBarController);
        }

        private CanvasTabBarItem ExistingTabBarItemWithIndex(int index)
        {
            CanvasTabBarItem tabBarItem = null;
            for (int i = 0; i < tabBarItems?.Count; i++)
            {
                var tbi = tabBarItems[i];
                if (tbi.index == index)
                {
                    tabBarItem = tbi;
                    break;
                }
            }

            return tabBarItem;
        }

        private CanvasTabBarItem TabBarItemForViewControllerAtIndex(TabBarCanvasController tabBarController, CanvasController viewController, int index)
        {
            // If the tabBarController's itemProvider has been set, ask it to provide a tab bar item.
            CanvasTabBarItem tabBarItem = null;
            ITabBarCanvasControllerItemProvider itemProvider = tabBarController.itemProvider;
            if (itemProvider != null)
            {
                tabBarItem = itemProvider.TabBarItemForViewControllerAtIndex(this, viewController, index);
                tabBarItem?.transform.SetParent(tabBarItemsContainer, false);
            }
           
            if (tabBarItem == null)
            {
                tabBarItem = Instantiate(tabBarItemTemplate, tabBarItemsContainer, false);
            }

            return tabBarItem;
        }

        private void OnReceiveTabBarItemClicked(CanvasTabBarItem tabBarItem)
        {
            OnTabBarItemClicked.Invoke(tabBarItem);
        }

        private void DestroyTabBarItems()
        {
            foreach (CanvasTabBarItem tabBarItem in tabBarItems)
            {
                Destroy(tabBarItem.gameObject);
            }
            tabBarItems.Clear();
        }

        [System.Serializable] public class CanvasTabBarEvent : UnityEvent<CanvasTabBarItem> { }
    }
}
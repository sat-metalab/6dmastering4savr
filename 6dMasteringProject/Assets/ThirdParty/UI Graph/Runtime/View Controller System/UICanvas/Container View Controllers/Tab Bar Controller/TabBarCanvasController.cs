﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The TabBarCanvasController is a container view controller designed for switching between content screens with a tab bar interface. It manages a list of content view controllers and offers transitions, in addition to a tab bar interface, for switching between the currently selected/visible view controller.
    /// <para>
    /// By default, the TabBarCanvasController instantiates its tab bar items upon load depending upon its list of content view controllers. You may provide a custom tab bar item prefab to be used. Additionally, tab bar items can be predefined or instantiated manually from script.
    /// </para>
    /// <para>
    /// The first time a content view controller is shown, it is instantiated and embedded within the <see cref="viewControllersContainer"/> transform. Content view controllers are not unloaded until the tab bar itself is unloaded. 
    /// </para>
    /// </summary>
    public partial class TabBarCanvasController : CanvasController
    {
        /// <summary>
        /// The transition identifier for the Tab Bar Canvas Controller's Set Selected Index transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier SetSelectedIndexTransition = new ViewControllerTransitionIdentifier("TabBarCanvasController.SetSelectedIndex", ViewControllerTransitionIdentifier.TransitionType.Replacement);
        /// <summary>
        /// The transition identifier for the Tab Bar Canvas Controller's Set Selected View Controller transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier SetSelectedViewControllerTransition = new ViewControllerTransitionIdentifier("TabBarCanvasController.SetSelectedViewController", ViewControllerTransitionIdentifier.TransitionType.Replacement, "Set Selected View Controller");
        /// <summary>
        /// The transition identifier for the Tab Bar Canvas Controller's Set View Controllers transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier SetViewControllersTransition = new ViewControllerTransitionIdentifier("TabBarCanvasController.SetViewControllers", ViewControllerTransitionIdentifier.TransitionType.Replacement);

        private const int DefaultIndex = 0;
        private const int NoIndex = -1;

        /// <summary>
        /// Animate the transition on tab bar item selection? If true, the transition when selecting a tab bar item will be animated.
        /// </summary>
        public bool animateTransitionOnTabBarItemSelection = false;
        /// <summary>
        /// Block selection whilst in transition? If true, attempting to change the selected index whilst already transitioning the selected index will be blocked. Note that this is only relevant when animating the selection.
        /// </summary>
        public bool blockSelectionWhilstInTransition = true;
        /// <summary>
        /// Assign an item provider to provide tab bar items to the tab bar from script. If null, the necessary items are instantiated by the tab bar using its item template.
        /// </summary>
        public ITabBarCanvasControllerItemProvider itemProvider;
        /// <summary>
        /// The tab bar used for switching the tab bar controller's selected view controller.
        /// </summary>
        [ViewReference] public CanvasTabBar tabBar;

        /// <summary>
        /// The container into which the tab bar controller places its child view controllers.
        /// </summary>
        [ViewReference] protected RectTransform viewControllersContainer;

        /// <summary>
        /// Retrieve the list of view controllers currently displayed by the tab bar controller.
        /// </summary>
        /// <seealso cref="SetViewControllers(CanvasController[], bool, System.Action)"/>
        public CanvasController[] ViewControllers { get; private set; }
        /// <summary>
        /// Retrieve the currently selected index.
        /// </summary>
        public int SelectedIndex { get; private set; } = DefaultIndex;
        /// <summary>
        /// Retrieve the currently selected view controller.
        /// </summary>
        public CanvasController SelectedViewController
        {
            get
            {
                CanvasController selectedViewController = null;
                if (IsValidViewControllerIndex(SelectedIndex))
                {
                    selectedViewController = ViewControllers[SelectedIndex];
                }

                return selectedViewController;
            }
        }

        /// <summary>
        /// Set the tab bar controller's list of view controllers. Use this method to configure the tab bar controller prior to presentation. Additionally, if the tab bar controller's view has already been loaded when this method is called, a replacement transition will be performed, unloading the existing list of view controllers and transitioning to the new list.
        /// </summary>
        /// <param name="viewControllers"></param>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void SetViewControllers(CanvasController[] viewControllers, bool animated = false, System.Action completion = null)
        {
            if (CanSetViewControllers(viewControllers, out Error error) == false)
            {
                error.LogWithPrefix("Cannot set view controllers.");
                return;
            }

            CanvasController[] previousViewControllers = ViewControllers;
            CanvasController previousSelectedViewController = SelectedViewController;

            ViewControllers = viewControllers;

            if (ViewIsLoaded)
            {
                SwitchContentViewControllers(previousViewControllers, previousSelectedViewController, animated, completion);
            }
        }

        /// <summary>
        /// Set the tab bar controller's selected index. This shows the view controller in the <see cref="ViewControllers"/> list at index <paramref name="index"/>.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void SetSelectedIndex(int index, bool animated = false, System.Action completion = null)
        {
            SelectViewControllerAtIndex(SetSelectedIndexTransition, index, animated, completion);
        }

        /// <summary>
        /// Set the tab bar controller's selected view controller. <paramref name="viewController"/> must be a view controller in the <see cref="ViewControllers"/> list.
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void SetSelectedViewController(CanvasController viewController, bool animated = false, System.Action completion = null)
        {
            var index = IndexOfViewController(viewController, out Error error);
            if (index == NoIndex)
            {
                error.LogWithPrefix("Cannot select view controller.");
                return;
            }

            SelectViewControllerAtIndex(SetSelectedViewControllerTransition, index, animated, completion);
        }

        /// <summary>
        /// Can <paramref name="index"/> currently be selected by the tab bar controller?
        /// </summary>
        /// <param name="index"></param>
        /// <param name="error"></param>
        /// <returns>True if the index can be selected. False otherwise.</returns>
        public bool CanSelectIndex(int index, out Error error)
        {
            if (IsValidViewControllerIndex(index) == false)
            {
                error = new Error("Index is out of bounds.");
                return false;
            }

            if (ViewIsLoaded == false)
            {
                error = new Error("The tab bar controller's view has not been loaded.");
                return false;
            }

            if (blockSelectionWhilstInTransition && IsPerformingTransitionBetweenChildren)
            {
                error = new Error("The tab bar controller is currently transitioning and is set to block selection whilst in transition.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }

        /// <summary>
        /// Can <paramref name="viewController"/> currently be selected by the tab bar controller?
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="error"></param>
        /// <returns>True if the view controller can be selected. False otherwise.</returns>
        public bool CanSelectViewController(CanvasController viewController, out Error error)
        {
            int index = IndexOfViewController(viewController, out error);
            if (error != null)
            {
                return false;
            }

            return CanSelectIndex(index, out error);
        }

        /// <summary>
        /// Can the tab bar controller's <see cref="ViewControllers"/> list currently be set to <paramref name="viewControllers"/>?
        /// </summary>
        /// <param name="viewControllers"></param>
        /// <param name="error"></param>
        /// <returns>True if the list of view controllers can be set. False otherwise.</returns>
        public bool CanSetViewControllers(CanvasController[] viewControllers, out Error error)
        {
            if (viewControllers.Length == 0)
            {
                error = new Error("The list of view controllers cannot be empty.", Error.Severity.Log);
                return false;
            }

            if (IsPerformingTransitionBetweenChildren)
            {
                error = new Error("The tab bar controller is currently performing a transition.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }

        protected override void ViewDidLoad()
        {
            base.ViewDidLoad();

            ConfigureTabBar();
            SwitchContentViewController(SetSelectedIndexTransition, NoIndex, SelectedIndex, false, null);
        }

        protected override bool ImplementsTransition(ViewControllerTransitionIdentifier transitionIdentifier)
        {
            return transitionIdentifier.Equals(SetSelectedIndexTransition) || transitionIdentifier.Equals(SetSelectedViewControllerTransition) || transitionIdentifier.Equals(SetViewControllersTransition);
        }

        protected override void PerformInvokedTransition(ViewControllerTransitionIdentifier transitionIdentifier, CanvasControllerInvokeTransitionData transitionData, System.Action completion)
        {
            if (transitionIdentifier.Equals(SetSelectedIndexTransition))
            {
                if (transitionData is SetSelectedIndexInvokeTransitionData setSelectedIndexTransitionData)
                {
                    int index = setSelectedIndexTransitionData.SelectedIndex;
                    bool animated = setSelectedIndexTransitionData.Animated;
                    SetSelectedIndex(index, animated, completion);
                }
                else
                {
                    Error.Log("Tab bar controller cannot perform SetSelectedIndex transition. Transition data was not of type SetSelectedIndexInvokeTransitionData.", Error.Severity.Warning);
                }
            }
            else if (transitionIdentifier.Equals(SetSelectedViewControllerTransition))
            {
                SetSelectedViewController(transitionData.ToViewController, transitionData.Animated, completion);
            }
            else if (transitionIdentifier.Equals(SetViewControllersTransition))
            {
                if (transitionData is SetViewControllersInvokeTransitionData setViewControllersTransitionData)
                {
                    CanvasController[] viewControllers = setViewControllersTransitionData.ViewControllers;
                    bool animated = setViewControllersTransitionData.Animated;
                    SetViewControllers(viewControllers, animated, completion);
                }
                else
                {
                    Error.Log("Tab bar controller cannot perform SetViewControllers transition. Transition data was not of type SetViewControllersInvokeTransitionData.", Error.Severity.Warning);
                }
            }
        }

        private bool IsValidViewControllerIndex(int index)
        {
            return ((ViewControllers != null) &&
                (index >= 0) &&
                (index < ViewControllers.Length));
        }

        private int IndexOfViewController(CanvasController viewController, out Error error)
        {
            var index = -1;
            if (ViewControllers != null)
            {
                for (int i = 0; i < ViewControllers.Length; i++)
                {
                    var vc = ViewControllers[i];
                    if (vc.Equals(viewController))
                    {
                        index = i;
                        break;
                    }
                }
            }

            error = (index == NoIndex) ? new Error("Provided view controller does not exist in the view controllers array.") : null;
            return index;
        }

        private void SelectViewControllerAtIndex(ViewControllerTransitionIdentifier transitionIdentifier, int index, bool animated, System.Action completion)
        {
            if (CanSelectIndex(index, out Error error) == false)
            {
                error.LogWithPrefix(string.Format("Cannot select index '{0}'.", index));
                return;
            }

            if (index == SelectedIndex)
            {
                return;
            }

            if (TryGetAnyChildTransition(out var transition))
            {
                transition.ForceImmediateCompletion();
            }

            int fromIndex = SelectedIndex;
            SelectedIndex = index;
            if (ViewIsLoaded)
            {
                SwitchContentViewController(transitionIdentifier, fromIndex, SelectedIndex, animated, completion);
            }
        }

        private void SwitchContentViewController(ViewControllerTransitionIdentifier transitionIdentifier, int fromIndex, int toIndex, bool animated, System.Action completion)
        {
            if (IsValidViewControllerIndex(toIndex) == false)
            {
                return;
            }

            var toViewController = ViewControllerAtIndexAndLoadIfNecessary(toIndex);
            CanvasController fromViewController = null;
            if (IsValidViewControllerIndex(fromIndex))
            {
                fromViewController = ViewControllers[fromIndex];
            }

            PerformChildTransition(transitionIdentifier, toViewController, fromViewController, animated, false, OnTransitionWillFinish: (transition, completed) =>
            {
                if (completed)
                {
                    var context = transition.Context;
                    context.ToViewController.View.BringToFront();
                }
            },
            OnTransitionDidFinish: (transition, completed) =>
            {
                if (completed)
                {
                    completion?.Invoke();
                }
            });

            tabBar.SelectTabBarItemAtIndex(toIndex);
        }

        private void SwitchContentViewControllers(CanvasController[] previousViewControllers, CanvasController previousSelectedViewController, bool animated, System.Action completion)
        {
            SelectedIndex = 0;

            CanvasController viewControllerToSelect = ViewControllers[SelectedIndex];
            AddViewControllerViewToContentView(viewControllerToSelect);

            tabBar.ReloadTabBarItemsForTabBarController(this);
            tabBar.SelectTabBarItemAtIndex(SelectedIndex);

            // Pass the (deselected) previous view controllers as intermediaries and destroy them upon completion.
            Stack<CanvasController> intermediaryViewControllers = new Stack<CanvasController>(previousViewControllers.Length - 1);
            foreach (CanvasController previousViewController in previousViewControllers)
            {
                if (previousViewController.Equals(previousSelectedViewController) == false)
                {
                    intermediaryViewControllers.Push(previousViewController);
                }
            }

            PerformChildTransition(SetViewControllersTransition, viewControllerToSelect, previousSelectedViewController, animated, false, intermediaryViewControllers, OnTransitionDidFinish: (transition, completed) =>
            {
                var context = transition.Context;
                if (completed)
                {
                    // Destroy all intermediary view controllers and the 'from' view controller.
                    foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                    {
                        intermediaryViewController.Destroy();
                    }
                    context.FromViewController.Destroy();

                    completion?.Invoke();
                }
            });
        }

        private CanvasController ViewControllerAtIndexAndLoadIfNecessary(int index)
        {
            var viewController = ViewControllers[index];
            if (viewController.ViewIsLoaded == false)
            {
                AddViewControllerViewToContentView(viewController);
            }

            return viewController;
        }

        private void AddViewControllerViewToContentView(CanvasController viewController)
        {
            AddChild(viewController);

            var childView = viewController.View;
            viewControllersContainer.Add(childView);
            childView.FitToParent();
            childView.SendToBack();
        }

        private void ConfigureTabBar()
        {
            tabBar.LoadTabBarItemsForTabBarController(this);
            tabBar.BindToTabBarItemClicked(OnTabBarItemClicked);
        }

        private void OnTabBarItemClicked(CanvasTabBarItem tabBarItem)
        {
            SetSelectedIndex(tabBarItem.index, animateTransitionOnTabBarItemSelection);
        }

        /// <summary>
        /// A custom transition data type for use with the <see cref="TabBarCanvasController.SetSelectedIndexTransition"/>.
        /// </summary>
        public class SetSelectedIndexInvokeTransitionData : CanvasControllerInvokeTransitionData
        {
            public int SelectedIndex { get; }

            public SetSelectedIndexInvokeTransitionData(int selectedIndex, bool animated = false) : base()
            {
                SelectedIndex = selectedIndex;
                Animated = animated;
            }
        }

        /// <summary>
        /// A custom transition data type for use with the <see cref="TabBarCanvasController.SetSelectedViewControllerTransition"/>.
        /// </summary>
        public class SetViewControllersInvokeTransitionData : CanvasControllerInvokeTransitionData
        {
            public CanvasController[] ViewControllers { get; }

            public SetViewControllersInvokeTransitionData(CanvasController[] viewControllers, bool animated = false) : base()
            {
                ViewControllers = viewControllers;
                Animated = animated;
            }
        }
    }

    // Graphing support.
    public partial class TabBarCanvasController : CanvasController, IGraphableFieldProvider
    {
        private const string ViewControllersGraphableFieldName = "View Controllers";

        GraphableField[] IGraphableFieldProvider.GraphableFields
        {
            get
            {
                return new GraphableField[]
                {
                    new GraphableField(ViewControllersGraphableFieldName, true)
                };
            }
        }

        void IGraphableFieldProvider.SetGraphableFieldValue(string fieldName, IGraphable[] graphables)
        {
            if (fieldName.Equals(ViewControllersGraphableFieldName))
            {
                ViewControllers = System.Array.ConvertAll(graphables, graphable => (CanvasController)graphable);
            }
        }

        protected override List<GraphableTransitionIdentifier> GraphableTransitionIdentifiers()
        {
            return new List<GraphableTransitionIdentifier>
            {
                new GraphableTransitionIdentifier(SetSelectedViewControllerTransition)
            };
        }

        protected override bool ShouldPerformGraphTransition(GraphTransition<CanvasController> graphTransition, out Error error)
        {
            ViewControllerTransitionIdentifier transitionIdentifier = graphTransition.TransitionIdentifier;
            if (transitionIdentifier.Equals(SetSelectedViewControllerTransition))
            {
                return CanSelectViewController(graphTransition.ToViewController, out error);
            }

            return base.ShouldPerformGraphTransition(graphTransition, out error);
        }
    }
}
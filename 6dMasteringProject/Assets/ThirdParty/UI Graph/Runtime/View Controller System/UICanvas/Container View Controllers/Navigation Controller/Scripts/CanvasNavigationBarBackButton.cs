﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.UI;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The navigation bar back button component associated with a <see cref="NavigationCanvasController"/>. The navigation controller displays a back button to easily return to the previous view controller in the stack.
    /// </summary>
    public class CanvasNavigationBarBackButton : MonoBehaviour
    {
        /// <summary>
        /// The back button's button.
        /// </summary>
        public Button button;
        /// <summary>
        /// The back button's icon.
        /// </summary>
        public Image icon;
        /// <summary>
        /// The back button's alpha transition curve. Used when animating the back button between visible and hidden states.
        /// </summary>
        public AnimationCurve alphaTransitionCurve;

        private NavigationCanvasController navigationController;

        private float IconAlpha
        {
            get
            {
                Color color = icon.color;
                return color.a;
            }

            set
            {

                Color color = icon.color;
                color.a = value;
                icon.color = color;
            }
        }

        /// <summary>
        /// Configure the back button with the <paramref name="navigationController"/>.
        /// </summary>
        /// <param name="navigationController"></param>
        public void Configure(CanvasController navigationController)
        {
            this.navigationController = (NavigationCanvasController)navigationController;
        }

        /// <summary>
        /// Invoke the navigation controller's Pop transition.
        /// </summary>
        /// <param name="animated"></param>
        public void PopViewController(bool animated)
        {
            navigationController.Pop(animated);
        }

        public void TrackNavigationTransition(CanvasControllerTransition transition)
        {
            CanvasControllerTransitionContext context = transition.Context;
            if (context.Animated)
            {
                // In an animated transition, track the transition to cross-fade the icon, if necessary.
                if (TryGetFadeIconValuesForTransition(transition, out float fromAlpha, out float toAlpha))
                {
                    transition.OnTransitionProgressUpdated.AddListener((t, progress01) =>
                    {
                        FadeIcon(fromAlpha, toAlpha, progress01);
                    });
                }
            }
        }

        public void ReloadAfterTransition(CanvasControllerTransition transition, bool completed)
        {
            // After a transition, update the button's interactability and icon.
            var context = transition.Context;
            var navigationController = (NavigationCanvasController)context.OwnerViewController;
            bool backButtonEnabled = (navigationController.ViewControllers.Length > navigationController.LowestAllowedStackCount);
            button.interactable = backButtonEnabled;
            IconAlpha = (backButtonEnabled) ? 1f : 0f;
        }

        private bool TryGetFadeIconValuesForTransition(CanvasControllerTransition transition, out float fromAlpha, out float toAlpha)
        {
            CanvasControllerTransitionContext context = transition.Context;
            var navigationController = (NavigationCanvasController)context.OwnerViewController;
            var rootViewController = navigationController.RootViewController;
            ViewControllerTransitionIdentifier transitionIdentifier = context.Identifier;

            // In a replacement (SetRoot), fade the back button icon out if currently visible.
            if (transitionIdentifier.Type == ViewControllerTransitionIdentifier.TransitionType.Replacement)
            {
                if (Mathf.Approximately(IconAlpha, 0f) == false)
                {
                    fromAlpha = 1f;
                    toAlpha = 0f;
                    return true;
                }
            }
            else
            {
                // If transitioning from the root view controller (and not a replacement), fade the back button icon in.
                if ((rootViewController != null) && rootViewController.Equals(context.FromViewController))
                {
                    fromAlpha = 0f;
                    toAlpha = 1f;
                    return true;
                }
                // If transitioning to the root view controller, fade the back button icon out.
                else if ((rootViewController != null) && rootViewController.Equals(context.ToViewController))
                {
                    fromAlpha = 1f;
                    toAlpha = 0f;
                    return true;
                }
            }

            fromAlpha = 0f;
            toAlpha = 0f;
            return false;
        }

        private void FadeIcon(float startAlpha, float endAlpha, float progress01)
        {
            float easedProgress01 = alphaTransitionCurve.Evaluate(progress01);
            float alpha = Mathf.Lerp(startAlpha, endAlpha, easedProgress01);
            IconAlpha = alpha;
        }
    }
}
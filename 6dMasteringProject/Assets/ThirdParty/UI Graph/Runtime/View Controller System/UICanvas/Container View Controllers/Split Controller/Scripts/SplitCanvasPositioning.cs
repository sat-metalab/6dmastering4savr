﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The split canvas positioning component associated with the <see cref="SplitCanvasController"/>. This component is used to determine the layout of the <see cref="SplitCanvasController.mainViewControllerContainer"/> and <see cref="SplitCanvasController.secondaryViewControllerContainer"/> in the two states – Secondary Visible and Secondary Hidden.
    /// </summary>
    public class SplitCanvasPositioning : MonoBehaviour
    {
        /// <summary>
        /// The layout of the split controller when its <see cref="SplitCanvasController.SecondaryViewController"/> is visible.
        /// </summary>
        public Layout secondaryVisibleLayout;
        /// <summary>
        /// The layout of the split controller when its <see cref="SplitCanvasController.SecondaryViewController"/> is hidden.
        /// </summary>
        public Layout secondaryHiddenLayout;

        /// <summary>
        /// Update the split's layout.
        /// </summary>
        /// <param name="transitionIdentifier"></param>
        /// <param name="splitCanvasController"></param>
        /// <param name="progress01"></param>
        public void UpdateSplit(ViewControllerTransitionIdentifier transitionIdentifier, SplitCanvasController splitCanvasController, float progress01)
        {
            RectTransform mainStartPosition = MainViewStartPositionForTransition(transitionIdentifier);
            RectTransform mainEndPosition = MainViewEndPositionForTransition(transitionIdentifier);
            RectTransformExtensions.LerpUnclamped(splitCanvasController.mainViewControllerContainer, mainStartPosition, mainEndPosition, progress01);

            RectTransform secondaryStartPosition = SecondaryViewStartPositionForTransition(transitionIdentifier);
            RectTransform secondaryEndPosition = SecondaryViewEndPositionForTransition(transitionIdentifier);
            RectTransformExtensions.LerpUnclamped(splitCanvasController.secondaryViewControllerContainer, secondaryStartPosition, secondaryEndPosition, progress01);
        }

        private RectTransform MainViewPosition(bool secondaryVisible)
        {
            Layout layout = LayoutForSecondaryVisibility(secondaryVisible);
            return layout.mainViewPosition;
        }

        private RectTransform SecondaryViewPosition(bool secondaryVisible)
        {
            Layout layout = LayoutForSecondaryVisibility(secondaryVisible);
            return layout.secondaryViewPosition;
        }

        private Layout LayoutForSecondaryVisibility(bool secondaryVisible)
        {
            return (secondaryVisible) ? secondaryVisibleLayout : secondaryHiddenLayout;
        }

        private RectTransform MainViewStartPositionForTransition(ViewControllerTransitionIdentifier identifier)
        {
            return (identifier.Equals(SplitCanvasController.ShowSecondaryViewControllerTransition)) ? MainViewPosition(false) : MainViewPosition(true);
        }

        private RectTransform MainViewEndPositionForTransition(ViewControllerTransitionIdentifier identifier)
        {
            return (identifier.Equals(SplitCanvasController.ShowSecondaryViewControllerTransition)) ? MainViewPosition(true) : MainViewPosition(false);
        }

        private RectTransform SecondaryViewStartPositionForTransition(ViewControllerTransitionIdentifier identifier)
        {
            return (identifier.Equals(SplitCanvasController.ShowSecondaryViewControllerTransition)) ? SecondaryViewPosition(false) : SecondaryViewPosition(true);
        }

        private RectTransform SecondaryViewEndPositionForTransition(ViewControllerTransitionIdentifier identifier)
        {
            return (identifier.Equals(SplitCanvasController.ShowSecondaryViewControllerTransition)) ? SecondaryViewPosition(true) : SecondaryViewPosition(false);
        }

        /// <summary>
        /// A <see cref="SplitCanvasController"/> layout configuration.
        /// </summary>
        [System.Serializable]
        public class Layout
        {
            /// <summary>
            /// The position of the <see cref="SplitCanvasController.mainViewControllerContainer"/>.
            /// </summary>
            public RectTransform mainViewPosition;
            /// <summary>
            /// The position of the <see cref="SplitCanvasController.secondaryViewControllerContainer"/>.
            /// </summary>
            public RectTransform secondaryViewPosition;
        }
    }
}
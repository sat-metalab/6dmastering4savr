﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.Events;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The CanvasNavigationTransitionNotifier component exposes events for the navigation controller's child transitions. This can be used to update scripts in the view in response to navigation controller transitions.
    /// </summary>
    public class CanvasNavigationTransitionNotifier : MonoBehaviour
    {
        /// <summary>
        /// An event raised when the navigation controller will perform a child transition.
        /// </summary>
        public CanvasNavigationControllerWillPerformTransitionEvent OnNavigationControllerWillPerformChildTransition;
        /// <summary>
        /// An event raised when the navigation controller did perform a child transition.
        /// </summary>
        public CanvasNavigationControllerDidPerformTransitionEvent OnNavigationControllerDidPerformChildTransition;

        public void Configure(CanvasController navigationCanvasController)
        {
            navigationCanvasController.OnWillPerformChildTransition.AddListener(WillPerformChildTransition);
            navigationCanvasController.OnDidPerformChildTransition.AddListener(DidPerformChildTransition);
        }

        private void WillPerformChildTransition(CanvasControllerTransition transition)
        {
            OnNavigationControllerWillPerformChildTransition?.Invoke(transition);
        }

        private void DidPerformChildTransition(CanvasControllerTransition transition, bool completed)
        {
            OnNavigationControllerDidPerformChildTransition?.Invoke(transition, completed);
        }

        /// <summary>
        /// An event type for broadcasting that a <see cref="NavigationCanvasController"/> will perform a child transition.
        /// </summary>
        [System.Serializable]
        public class CanvasNavigationControllerWillPerformTransitionEvent : UnityEvent<CanvasControllerTransition> { }
        /// <summary>
        /// An event type for broadcasting that a <see cref="NavigationCanvasController"/> did perform a child transition.
        /// </summary>
        [System.Serializable]
        public class CanvasNavigationControllerDidPerformTransitionEvent : UnityEvent<CanvasControllerTransition, bool> { }
    }
}
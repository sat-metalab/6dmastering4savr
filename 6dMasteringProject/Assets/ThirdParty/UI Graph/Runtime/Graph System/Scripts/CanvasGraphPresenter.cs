﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.Events;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A CanvasGraphPresenter component is used to present a <see cref="GraphResource"/> in a <see cref="CanvasWindow"/>.
    /// </summary>
    public class CanvasGraphPresenter : MonoBehaviour
    {
        /// <summary>
        /// A reference to the graph resource to be presented.
        /// </summary>
        public GraphResource graphResource;
        /// <summary>
        /// A reference to the <see cref="CanvasWindow"/> in which to present the graph's initial view controller.
        /// </summary>
        public CanvasWindow window;
        /// <summary>
        /// Present the graph on awake?
        /// </summary>
        public bool presentOnAwake = true;
        /// <summary>
        /// Animate the graph presentation?
        /// </summary>
        public bool presentAnimated = false;

        /// <summary>
        /// The OnGraphPresented event is raised after the graph's initial view controller has been presented in the <see cref="window"/>.
        /// </summary>
        public CanvasGraphPresentationEvent OnGraphPresented;

        /// <summary>
        /// Present the <see cref="graphResource"/> in the <see cref="window"/>. If <see cref="presentOnAwake"/> is true, this method is called automatically in Awake. If <see cref="presentAnimated"/> is true, the presentation will be animated.
        /// </summary>
        public void PresentGraphInWindow()
        {
            var graph = new CanvasGraph(graphResource);
            CanvasController initialViewController = graph.InstantiateInitialViewController();
            window.RootViewController = initialViewController;
            window.Present(presentAnimated, () =>
            {
                OnGraphPresented?.Invoke(this, initialViewController);
            });
        }

        protected virtual void Awake()
        {
            if (presentOnAwake)
            {
                PresentGraphInWindow();
            }
        }

        /// <summary>
        /// Event type for a graph presentation event.
        /// </summary>
        [System.Serializable]
        public class CanvasGraphPresentationEvent : UnityEvent<CanvasGraphPresenter, CanvasController> { }
    }
}
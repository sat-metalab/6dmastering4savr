﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    public abstract class TransitionEdge : Edge
    {
        public TransitionEdge(TransitionEdgeData data, Dictionary<string, Node> nodes) : base(data, nodes)
        {
            TransitionIdentifier = data.TransitionIdentifier;
            Animated = data.animated;
            Interactive = data.interactive;
            UserIdentifier = data.userIdentifier;
        }

        public bool Animated { get; private set; }
        public bool Interactive { get; private set; }
        public string UserIdentifier { get; private set; }
        public ViewControllerTransitionIdentifier TransitionIdentifier { get; private set; }

        private string TransitionGuid
        {
            get
            {
                return TransitionIdentifier.Guid;
            }
        }

        public override void Invoke(object invoker)
        {
            if (TryPrepareGraphableTransition(invoker, out GraphableTransition transition, out Error error))
            {
                IGraphable targetGraphable = TargetNode.Graphable;
                targetGraphable.PerformGraphableTransition(transition);
            }
            else
            {
                error.LogWithPrefix("Cannot invoke transition edge.");
            }
        }

        private bool TryPrepareGraphableTransition(object invoker, out GraphableTransition graphableTransition, out Error error)
        {
            if (TargetNode.IsInstantiated == false)
            {
                graphableTransition = null;
                error = new Error("Target node has not been instantiated.");
                return false;
            }

            if (SourceNode.IsInstantiated == false)
            {
                graphableTransition = null;
                error = new Error("Source node has not been instantiated.");
                return false;
            }

            if (TransitionIdentifier == null)
            {
                graphableTransition = null;
                error = new Error(string.Format("The transition '{0}' was not found on the target node.", TransitionGuid));
                return false;
            }

            IGraphable destinationGraphable;
            if (TransitionIdentifier.Type == ViewControllerTransitionIdentifier.TransitionType.Dismissal)
            {
                if (DestinationNode.IsInstantiated == false)
                {
                    graphableTransition = null;
                    error = new Error(string.Format("A dismissal requires that the destination node has already been instantiated.", TransitionGuid));
                    return false;
                }

                destinationGraphable = DestinationNode.Graphable;
            }
            else
            {
                destinationGraphable = DestinationNode.Instantiate();
            }

            IGraphable sourceGraphable = SourceNode.Graphable;
            graphableTransition = new GraphableTransition(TransitionIdentifier, sourceGraphable, destinationGraphable, Animated, Interactive, UserIdentifier, invoker);
            error = null;
            return true;
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    public class GraphableTransitionIdentifier
    {
        public GraphableTransitionIdentifier(ViewControllerTransitionIdentifier transitionIdentifier, ViewControllerTransitionIdentifier reverseTransitionIdentifier = null)
        {
            TransitionIdentifier = transitionIdentifier;
            ReverseTransitionIdentifier = reverseTransitionIdentifier;
        }

        public ViewControllerTransitionIdentifier TransitionIdentifier { get; }
        public ViewControllerTransitionIdentifier ReverseTransitionIdentifier { get; }

        public string TransitionGuid
        {
            get
            {
                return TransitionIdentifier.Guid;
            }
        }

        public ViewControllerTransitionIdentifier.TransitionType TransitionType
        {
            get
            {
                return TransitionIdentifier.Type;
            }
        }

        public bool HasReverseTransition
        {
            get
            {
                return (ReverseTransitionIdentifier != null);
            }
        }
    }
}
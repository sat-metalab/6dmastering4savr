﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The base graph type for runtime representations of <see cref="GraphResource"/> assets.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public partial class Graph<T>
        where T : class
    {
        private readonly Node origin;
        private readonly List<Node> nodes;

        public Graph(GraphResource graphResource)
        {
            // Create runtime nodes.
            nodes = new List<Node>(graphResource.nodes.Count);
            var nodeDictionary = new Dictionary<string, Node>();
            foreach (NodeData nodeData in graphResource.nodes)
            {
                Node node = nodeData.CreateRuntimeRepresentation(this);
                nodes.Add(node);
                nodeDictionary.Add(node.Guid, node);

                // Set origin.
                if (nodeData.Equals(graphResource.origin))
                {
                    origin = node;
                }
            }

            // With all runtime nodes created, configure.
            for (int i = 0; i < nodes.Count; i++)
            {
                Node node = nodes[i];
                NodeData nodeData = graphResource.nodes[i];
                nodeData.ConfigureRuntimeRepresentation(node, nodeDictionary);
            }
        }

        /// <summary>
        /// Instantiate the graph's initial view controller.
        /// </summary>
        /// <returns>An instance of the graph's initial view controller or null if none is set.</returns>
        public T InstantiateInitialViewController()
        {
            if (origin == null)
            {
                Error.Log("Cannot instantiate initial view controller. The graph has no origin node set.");
                return null;
            }

            return origin.Instantiate() as T;
        }

        /// <summary>
        /// Instantiate the graph's initial view controller, specifying the type.
        /// </summary>
        /// <typeparam name="TViewController"></typeparam>
        /// <returns>An instance of the graph's initial view controller or null if none is set.</returns>
        public TViewController InstantiateInitialViewController<TViewController>()
            where TViewController : class, T
        {
            return InstantiateInitialViewController() as TViewController;
        }

        /// <summary>
        /// Instantiate the view controller as identified by <paramref name="identifier"/> in the graph.
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns>An instance of the view controller identified by <paramref name="identifier"/> or null if no view controller with that identifier exists.</returns>
        public T InstantiateViewControllerWithIdentifier(string identifier)
        {
            T viewController = null;
            Node node = NodeWithUserIdentifier(identifier);
            if (node != null)
            {
                viewController = node.Instantiate() as T;
            }
            else
            {
                Error.Log(string.Format("No view controller with the identifier '{0}' was found in the graph.", identifier));
            }

            return viewController;
        }

        // Convenience for casting to view controller subclass.
        /// <summary>
        /// Instantiate the view controller as identified by <paramref name="identifier"/> in the graph, specifying the type.
        /// </summary>
        /// <typeparam name="TViewController"></typeparam>
        /// <param name="identifier"></param>
        /// <returns>An instance of the view controller identified by <paramref name="identifier"/> or null if no view controller with that identifier exists.</returns>
        public TViewController InstantiateViewControllerWithIdentifier<TViewController>(string identifier)
            where TViewController : class, T
        {
            return InstantiateViewControllerWithIdentifier(identifier) as TViewController;
        }

        private Node NodeWithUserIdentifier(string identifier)
        {
            Node node = null;
            if (string.IsNullOrEmpty(identifier) == false)
            {
                foreach (Node n in nodes)
                {
                    if (identifier.Equals(n.UserIdentifier))
                    {
                        node = n;
                        break;
                    }
                }
            }

            return node;
        }
    }
}
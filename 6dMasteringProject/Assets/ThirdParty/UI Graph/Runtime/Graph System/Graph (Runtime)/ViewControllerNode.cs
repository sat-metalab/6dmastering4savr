﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph
{
    public partial class ViewControllerNode : Node
    {
        public ViewControllerNode(ViewControllerNodeData node, object graph) : base(node, graph) { }

        public List<GraphableFieldPort> FieldPorts { get; set; } = new List<GraphableFieldPort>();
        public List<GraphableEmbedViewObjectPort> EmbedViewObjectPorts { get; set; } = new List<GraphableEmbedViewObjectPort>();
        public List<GraphableTransitionViewObjectPort> TransitionViewObjectPorts { get; set; } = new List<GraphableTransitionViewObjectPort>();
        public List<ManualTransitionEdge> ManualEdges { get; set; } = new List<ManualTransitionEdge>();

        protected override void Initialize()
        {
            BindToTransitionViewObjectActivationIfNecessary(); // This must happen before any child view controllers are embedded to prevent their activation listener from being overwritten.
            ActivateFieldPortsIfNecessary();
            ActivateEmbedViewObjectPortsIfNecessary();
            BindToGraphableTransitionInvoker();
        }

        private void ActivateFieldPortsIfNecessary()
        {
            if (FieldPorts.Count == 0)
            {
                return;
            }

            var graphableFieldProvider = Graphable as IGraphableFieldProvider;
            foreach (GraphableFieldPort graphableFieldPort in FieldPorts)
            {
                string fieldName = graphableFieldPort.fieldName;
                IGraphable[] graphables = graphableFieldPort.Activate(Graphable);
                if (graphables.Length > 0)
                {
                    graphableFieldProvider.SetGraphableFieldValue(fieldName, graphables);
                }
            }
        }

        private void ActivateEmbedViewObjectPortsIfNecessary()
        {
            if (EmbedViewObjectPorts.Count == 0)
            {
                return;
            }

            List<GraphableEmbedData> graphables = new List<GraphableEmbedData>();
            foreach (GraphableEmbedViewObjectPort graphableEmbedViewObjectPort in EmbedViewObjectPorts)
            {
                IGraphable graphable = graphableEmbedViewObjectPort.Activate(Graphable);
                if (graphable != null)
                {
                    string embedViewObjectGuid = graphableEmbedViewObjectPort.embedViewObjectGuid;
                    graphables.Add(new GraphableEmbedData(graphable, embedViewObjectGuid));
                }
            }

            var graphableEmbedContainer = Graphable as IGraphableEmbedContainer;
            graphableEmbedContainer.EmbedGraphables(graphables);
        }

        private void BindToTransitionViewObjectActivationIfNecessary()
        {
            if (TransitionViewObjectPorts.Count == 0)
            {
                return;
            }

            var graphableTransitionViewObjectContainer = Graphable as IGraphableTransitionViewObjectContainer;
            graphableTransitionViewObjectContainer.BindGraphableTransitionViewObjectActivationListener(this as IGraphableTransitionViewObjectListener);
        }

        private void BindToGraphableTransitionInvoker()
        {
            var graphableTransitionInvoker = Graphable as IGraphableTransitionInvoker;
            graphableTransitionInvoker.BindGraphableTransitionInvokable(this as IGraphableTransitionInvokable);
        }
    }

    public partial class ViewControllerNode : IGraphableTransitionViewObjectListener
    {
        void IGraphableTransitionViewObjectListener.OnGraphableTransitionViewObjectActivated(IGraphableTransitionViewObject transitionViewObject, string activatedTriggerGuid, object invoker)
        {
            HandleTransitionViewObjectActivation(transitionViewObject, activatedTriggerGuid, invoker);
        }

        private void HandleTransitionViewObjectActivation(IGraphableTransitionViewObject transitionViewObject, string activatedTriggerGuid, object invoker)
        {
            string viewObjectGuid = transitionViewObject.Guid;
            GraphableTransitionViewObjectPort transitionViewObjectPort = TransitionViewObjectPortWithObjectGuid(viewObjectGuid);
            if (transitionViewObjectPort == null)
            {
                Error.Log(string.Format("No port found for graphable transition view object with guid '{0}' and trigger guid '{1}'", viewObjectGuid, activatedTriggerGuid));
                return;
            }

            transitionViewObjectPort.Activate(activatedTriggerGuid, invoker);
        }

        private GraphableTransitionViewObjectPort TransitionViewObjectPortWithObjectGuid(string guid)
        {
            GraphableTransitionViewObjectPort transitionViewObjectPort = null;
            foreach (GraphableTransitionViewObjectPort port in TransitionViewObjectPorts)
            {
                if (port.transitionViewObjectGuid.Equals(guid))
                {
                    transitionViewObjectPort = port;
                    break;
                }
            }

            return transitionViewObjectPort;
        }
    }

    public partial class ViewControllerNode : IGraphableTransitionInvokable
    {
        void IGraphableTransitionInvokable.InvokeTransitionWithIdentifier(string userIdentifier, object invoker)
        {
            TransitionEdge manualEdge = ManualEdgeWithUserIdentifier(userIdentifier);
            if (manualEdge == null)
            {
                Error.Log(string.Format("No transition with the identifier '{0}' was found on {1}.", userIdentifier, Graphable));
                return;
            }

            manualEdge.Invoke(invoker);
        }

        private TransitionEdge ManualEdgeWithUserIdentifier(string userIdentifier)
        {
            TransitionEdge edge = null;
            foreach (TransitionEdge manualEdge in ManualEdges)
            {
                if (manualEdge.UserIdentifier.Equals(userIdentifier))
                {
                    edge = manualEdge;
                    break;
                }
            }

            return edge;
        }
    }
}
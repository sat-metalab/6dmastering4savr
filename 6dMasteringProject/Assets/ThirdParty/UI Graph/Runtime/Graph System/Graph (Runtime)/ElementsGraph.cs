﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// An ElementsGraph is the runtime representation of a <see cref="GraphResource"/> asset, built with the UI Elements workflow.
    /// </summary>
    public class ElementsGraph : Graph<ElementsController>
    {
        public ElementsGraph(GraphResource graphResource) : base(graphResource) { }
    }
}
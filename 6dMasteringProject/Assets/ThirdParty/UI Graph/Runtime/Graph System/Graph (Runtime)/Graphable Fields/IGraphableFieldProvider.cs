﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// Container view controllers implement the IGraphableFieldProvider interface to expose fields to the graph editor. When the implementing view controller is added to a graph, it will expose ports for each of its graphable fields, as defined by this interface.
    /// </summary>
    public interface IGraphableFieldProvider : IGraphable
    {
        /// <summary>
        /// A list of <see cref="GraphableField"/> objects, each representing a view controller field to be exposed to the graph editor.
        /// </summary>
        GraphableField[] GraphableFields { get; }
        /// <summary>
        /// Set the appropriate <paramref name="fieldName"/> of your view controller with the connected <paramref name="graphables"/>. This method will be invoked when the view controller is instantiated from a graph.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="graphables"></param>
        void SetGraphableFieldValue(string fieldName, IGraphable[] graphables);
    }
}
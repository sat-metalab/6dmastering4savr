﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A graphable field object is used by a container view controller to expose a field to the graph editor.
    /// </summary>
    public class GraphableField
    {
        /// <summary>
        /// The field's name. This will be displayed in the graph editor.
        /// </summary>
        public string FieldName { get; }
        /// <summary>
        /// Allows multiple values? If true, the exposed port in the graph editor will allow connection to many view controller nodes. Otherwise it will allow connection to a single view controller node. This can be used to expose an array of view controllers to the graph editor, for example.
        /// </summary>
        public bool AllowsMultiple { get; }

        public GraphableField(string fieldName, bool allowsMultiple = false)
        {
            FieldName = fieldName;
            AllowsMultiple = allowsMultiple;
        }
    }
}
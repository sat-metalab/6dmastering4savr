﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The GraphableTransitionViewComponent is an abstract base class for graphable transition view objects – objects in the view that can trigger a graph transition. Derive from this class to create a custom graphable component, specifying the target's type <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract partial class GraphableTransitionViewComponent<T> : GraphableTransitionViewComponentBase
        where T : Object
    {
        public T target;

        /// <summary>
        /// The component's list of triggers. Each trigger will be exposed in the graph editor.
        /// </summary>
        [SerializeField] protected GraphableTransitionViewObjectTrigger[] triggers;

#pragma warning disable 0649
        [SerializeField] private string displayName;
#pragma warning restore 0649
        private IGraphableTransitionViewObjectListener activationListener;

        /// <summary>
        /// Override this method to configure your target to activate the appropriate trigger, as required. For example, the <see cref="GraphableButtonComponent"/> uses this method to bind its single trigger activation to its target button's OnClick event.
        /// </summary>
        protected abstract void BindToTarget();

        /// <summary>
        /// Activate the <paramref name="trigger"/>.
        /// </summary>
        /// <param name="trigger"></param>
        protected void Activate(GraphableTransitionViewObjectTrigger trigger)
        {
            string triggerGuid = trigger.Guid;
            activationListener?.OnGraphableTransitionViewObjectActivated(this as IGraphableTransitionViewObject, triggerGuid, target);
        }

        protected virtual void Awake()
        {
            BindToTarget();
        }

        /// <summary>
        /// Override this method to set the default values of your graphable component in the editor.
        /// </summary>
        protected override void Reset()
        {
            base.Reset();

            displayName = name;
            target = GetComponent<T>();
        }
    }
     
    public abstract partial class GraphableTransitionViewComponent<T> : GraphableTransitionViewComponentBase, IGraphableTransitionViewObject
    {
        string IGraphableViewObject.Guid => Guid;
        string IGraphableViewObject.DisplayName => displayName;
        GraphableTransitionViewObjectTrigger[] IGraphableTransitionViewObject.Triggers => triggers;

        void IGraphableTransitionViewObject.BindActivationListener(IGraphableTransitionViewObjectListener listener)
        {
            activationListener = listener;
        }
    }
}
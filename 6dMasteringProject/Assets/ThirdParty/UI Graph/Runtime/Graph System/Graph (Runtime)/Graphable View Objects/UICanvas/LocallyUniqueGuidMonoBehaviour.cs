﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A component that will maintain a unique identifier within its scene.
    /// </summary>
    public abstract partial class LocallyUniqueGuidMonoBehaviour : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField] private string guid;
#pragma warning restore 0649

        /// <summary>
        /// The component's unique identifier.
        /// </summary>
        public string Guid
        {
            get
            {
                return guid;
            }
        }

        protected virtual void Reset()
        {
            guid = System.Guid.NewGuid().ToString();
        }

        protected virtual void OnValidate()
        {
            ResetIfDuplicateDetected();
        }

        private void ResetIfDuplicateDetected()
        {
            if (Application.isEditor && (Application.isPlaying == false) && gameObject.scene.isLoaded)
            {
                List<LocallyUniqueGuidMonoBehaviour> locallyUniqueGuidComponents = SceneExtensions.FindAllComponentsInScene<LocallyUniqueGuidMonoBehaviour>(gameObject.scene);
                foreach (LocallyUniqueGuidMonoBehaviour locallyUniqueGuidComponent in locallyUniqueGuidComponents)
                {
                    if ((locallyUniqueGuidComponent.Equals(this) == false) && locallyUniqueGuidComponent.Guid.Equals(Guid))
                    {
                        Reset();
                    }
                }
            }
        }
    }
}
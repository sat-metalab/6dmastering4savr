﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public class GraphableTransitionViewObjectPort
    {
        public string transitionViewObjectGuid;
        public List<TransitionViewObjectEdge> edges;

        public GraphableTransitionViewObjectPort(GraphableTransitionViewObjectPortData data, Dictionary<string, Node> nodes)
        {
            transitionViewObjectGuid = data.transitionViewObjectGuid;

            edges = new List<TransitionViewObjectEdge>(data.edges.Count);
            foreach (TransitionViewObjectEdgeData viewObjectEdgeData in data.edges)
            {
                TransitionViewObjectEdge viewObjectEdge = new TransitionViewObjectEdge(viewObjectEdgeData, nodes);
                edges.Add(viewObjectEdge);
            }
        }

        public void Activate(string triggerGuid, object invoker)
        {
            TransitionViewObjectEdge edge = EdgeWithTriggerGuid(triggerGuid);
            if (edge == null)
            {
                Error.Log(string.Format("No edge found for graphable view object with guid '{0}' and trigger guid '{1}'", transitionViewObjectGuid, triggerGuid), Error.Severity.Log);
                return;
            }

            edge.Invoke(invoker);
        }

        private TransitionViewObjectEdge EdgeWithTriggerGuid(string triggerGuid)
        {
            TransitionViewObjectEdge edge = null;
            foreach (TransitionViewObjectEdge e in edges)
            {
                if (e.TriggerGuid.Equals(triggerGuid))
                {
                    edge = e;
                    break;
                }
            }

            return edge;
        }
    }
}
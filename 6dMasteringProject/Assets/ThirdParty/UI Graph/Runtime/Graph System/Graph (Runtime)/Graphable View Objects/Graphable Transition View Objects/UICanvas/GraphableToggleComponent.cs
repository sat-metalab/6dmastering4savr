﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The GraphableToggleComponent exposes UnityEngine.UI.Toggle components to the graph editor. It defines two triggers, "Switch On" and "Switch Off", that are appropriately activated upon the toggle's value being changed.
    /// </summary>
    public class GraphableToggleComponent : GraphableTransitionViewComponent<UnityEngine.UI.Toggle>
    {
        protected override void BindToTarget()
        {
            // Bind to the toggle's onValueChanged callback.
            target.onValueChanged.AddListener(OnToggleValueChanged);
        }

        protected override void Reset()
        {
            base.Reset();

            // Provide two triggers for when the toggle is switched on, and switched off.
            triggers = new GraphableTransitionViewObjectTrigger[]
            {
                new GraphableTransitionViewObjectTrigger("Switch On"),
                new GraphableTransitionViewObjectTrigger("Switch Off")
            };
        }

        private void OnToggleValueChanged(bool isOn)
        {
            // Select a trigger based upon the toggle's state and activate the port.
            var trigger = (isOn) ? triggers[0] : triggers[1];
            Activate(trigger);
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The GraphableEmbedComponent is a component used to embed a child view controller in a container, defined in the graph editor. Its <see cref="displayName"/> will be configured with its game object's name.
    /// </summary>
    public partial class GraphableEmbedComponent : LocallyUniqueGuidMonoBehaviour
    {
        /// <summary>
        /// The embed container. The child view controller's view will be placed in this container.
        /// </summary>
        public RectTransform container;

#pragma warning disable 0649
        [SerializeField] private string displayName;
#pragma warning restore 0649

        protected override void Reset()
        {
            base.Reset();

            displayName = name;
            container = GetComponent<RectTransform>();
        }

        private void EmbedGraphable(IGraphable graphable)
        {
            var viewController = graphable as CanvasController;
            var view = viewController.View;
            container.Add(view);
            view.FitToParent();
        }
    }

    public partial class GraphableEmbedComponent : LocallyUniqueGuidMonoBehaviour, IGraphableEmbedViewObject
    {
        string IGraphableViewObject.Guid => Guid;
        string IGraphableViewObject.DisplayName => displayName;

        void IGraphableEmbedViewObject.Initialize(IGraphable graphable)
        {
            EmbedGraphable(graphable);
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    public interface IGraphableEmbedContainer : IGraphable
    {
        IGraphableEmbedViewObject[] GraphableEmbedViewObjects { get; }
        void EmbedGraphables(List<GraphableEmbedData> graphableEmbedDatas);
    }

    public struct GraphableEmbedData
    {
        public IGraphable graphable;
        public string embedViewObjectGuid;

        public GraphableEmbedData(IGraphable graphable, string embedViewObjectGuid)
        {
            this.graphable = graphable;
            this.embedViewObjectGuid = embedViewObjectGuid;
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public class TransitionViewObjectEdge : TransitionEdge
    {
        public TransitionViewObjectEdge(TransitionViewObjectEdgeData data, Dictionary<string, Node> nodes) : base(data, nodes)
        {
            TriggerGuid = data.triggerGuid;
        }

        public string TriggerGuid { get; }
    }
}
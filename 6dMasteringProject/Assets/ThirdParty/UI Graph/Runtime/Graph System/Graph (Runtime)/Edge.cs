﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    public abstract class Edge
    {
        public Edge(EdgeData data, Dictionary<string, Node> nodes)
        {
            Guid = data.guid;

            string sourceNodeGuid = data.sourceNode.guid;
            if (nodes.TryGetValue(sourceNodeGuid, out Node sourceNode))
            {
                SourceNode = sourceNode;
            }
            else
            {
                Error.Log(string.Format("Error creating edge {0}. Source node with guid {1} was not found.", Guid, sourceNodeGuid));
            }

            string destinationNodeGuid = data.destinationNode.guid;
            if (nodes.TryGetValue(destinationNodeGuid, out Node destinationNode))
            {
                DestinationNode = destinationNode;
            }
            else
            {
                Error.Log(string.Format("Error creating edge {0}. Destination node with guid {1} was not found.", Guid, destinationNodeGuid));
            }

            string targetNodeGuid = data.targetNode.guid;
            if (nodes.TryGetValue(targetNodeGuid, out Node targetNode))
            {
                TargetNode = targetNode;
            }
            else
            {
                Error.Log(string.Format("Error creating edge {0}. Target node with guid {1} was not found.", Guid, targetNodeGuid));
            }
        }

        public string Guid { get; private set; }
        public Node SourceNode { get; private set; }
        public Node DestinationNode { get; private set; }
        public Node TargetNode { get; private set; }

        public abstract void Invoke(object invoker);
    }
}
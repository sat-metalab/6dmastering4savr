﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// An IGraphable represents a resource that can be used in a graph, such as a <see cref="CanvasController"/>.
    /// </summary>
    public interface IGraphable
    {
        IGraphable InstantiateFromResource(IGraphable resource);
        void SetGraph(object graph);
        List<GraphableTransitionIdentifier> GraphableTransitionIdentifiers();
        void PerformGraphableTransition(GraphableTransition transition);

#if UNITY_EDITOR
        bool RequiresRefreshForUpdatedAsset(string assetPath);
#endif
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph
{
    public abstract partial class Node
    {
        private readonly object graph;

        public Node(NodeData node, object graph)
        {
            Guid = node.guid;
            UserIdentifier = node.userIdentifier;
            Resource = node.resource.Value;
            this.graph = graph;
        }

        public string Guid { get; private set; }
        public string UserIdentifier { get; private set; }
        public IGraphable Resource { get; private set; }
        public IGraphable Graphable { get; private set; }

        public bool IsInstantiated
        {
            get
            {
                return (Graphable != null);
            }
        }

        public virtual IGraphable Instantiate()
        {
            Graphable = Resource.InstantiateFromResource(Resource);
            AssignGraphToGraphable();
            Initialize();
            return Graphable;
        }

        protected abstract void Initialize();

        private void AssignGraphToGraphable()
        {
            Graphable.SetGraph(graph);
        }
    }
}
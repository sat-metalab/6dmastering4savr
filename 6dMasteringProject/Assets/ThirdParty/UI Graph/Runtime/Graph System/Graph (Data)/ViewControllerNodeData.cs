﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    public partial class ViewControllerNodeData : NodeData
    {
        public List<GraphableFieldPortData> fieldPorts = new List<GraphableFieldPortData>();
        public List<GraphableEmbedViewObjectPortData> embedViewObjectPorts = new List<GraphableEmbedViewObjectPortData>();
        public List<GraphableTransitionViewObjectPortData> transitionViewObjectPorts = new List<GraphableTransitionViewObjectPortData>();
        public List<ManualTransitionEdgeData> manualEdges = new List<ManualTransitionEdgeData>();

        public override Node CreateRuntimeRepresentation(object graph)
        {
            var node = new ViewControllerNode(this, graph);
            return node;
        }

        public override void ConfigureRuntimeRepresentation(Node node, Dictionary<string, Node> nodes)
        {
            ViewControllerNode viewControllerNode = (ViewControllerNode)node;
            viewControllerNode.FieldPorts = CreateFieldPortsRuntimeRepresentation(nodes);
            viewControllerNode.EmbedViewObjectPorts = CreateEmbedViewObjectPortsRuntimeRepresentation(nodes);
            viewControllerNode.TransitionViewObjectPorts = CreateViewObjectPortsRuntimeRepresentation(nodes);
            viewControllerNode.ManualEdges = CreateManualEdgesRuntimeRepresentation(nodes);
        }

        private List<GraphableFieldPort> CreateFieldPortsRuntimeRepresentation(Dictionary<string, Node> nodes)
        {
            List<GraphableFieldPort> runtimeFieldPorts = new List<GraphableFieldPort>(fieldPorts.Count);
            foreach (GraphableFieldPortData fieldPortData in fieldPorts)
            {
                GraphableFieldPort fieldPort = new GraphableFieldPort(fieldPortData, nodes);
                runtimeFieldPorts.Add(fieldPort);
            }

            return runtimeFieldPorts;
        }

        private List<GraphableEmbedViewObjectPort> CreateEmbedViewObjectPortsRuntimeRepresentation(Dictionary<string, Node> nodes)
        {
            List<GraphableEmbedViewObjectPort> runtimeEmbedViewObjectPorts = new List<GraphableEmbedViewObjectPort>(embedViewObjectPorts.Count);
            foreach (GraphableEmbedViewObjectPortData embedViewObjectPortData in embedViewObjectPorts)
            {
                GraphableEmbedViewObjectPort embedViewObjectPort = new GraphableEmbedViewObjectPort(embedViewObjectPortData, nodes);
                runtimeEmbedViewObjectPorts.Add(embedViewObjectPort);
            }

            return runtimeEmbedViewObjectPorts;
        }

        private List<GraphableTransitionViewObjectPort> CreateViewObjectPortsRuntimeRepresentation(Dictionary<string, Node> nodes)
        {
            List<GraphableTransitionViewObjectPort> runtimeViewObjectPorts = new List<GraphableTransitionViewObjectPort>(transitionViewObjectPorts.Count);
            foreach (GraphableTransitionViewObjectPortData viewObjectPortData in transitionViewObjectPorts)
            {
                GraphableTransitionViewObjectPort viewObjectPort = new GraphableTransitionViewObjectPort(viewObjectPortData, nodes);
                runtimeViewObjectPorts.Add(viewObjectPort);
            }

            return runtimeViewObjectPorts;
        }

        private List<ManualTransitionEdge> CreateManualEdgesRuntimeRepresentation(Dictionary<string, Node> nodes)
        {
            List<ManualTransitionEdge> runtimeManualEdges = new List<ManualTransitionEdge>(manualEdges.Count);
            foreach (ManualTransitionEdgeData manualEdgeData in manualEdges)
            {
                ManualTransitionEdge manualEdge = new ManualTransitionEdge(manualEdgeData, nodes);
                runtimeManualEdges.Add(manualEdge);
            }

            return runtimeManualEdges;
        }
    }

    public partial class ViewControllerNodeData : NodeData
    {
#if UNITY_EDITOR
        public bool fieldPortsVisible = true;
        public bool viewObjectPortsVisible = true;
        public bool manualTransitionsVisible = true;

        public override List<EdgeData> EdgesWithTypes(EdgeData.EdgeType types)
        {
            var edges = new List<EdgeData>();
            foreach (GraphableFieldPortData fieldPort in fieldPorts)
            {
                foreach (FieldEdgeData fieldEdge in fieldPort.edges)
                {
                    if (types.HasFlag(fieldEdge.type))
                    {
                        edges.Add(fieldEdge);
                    }
                }
            }

            foreach (GraphableEmbedViewObjectPortData viewObjectPort in embedViewObjectPorts)
            {
                EmbedViewObjectEdgeData viewObjectEdge = viewObjectPort.edge;
                if (viewObjectEdge != null)
                {
                    if (types.HasFlag(viewObjectEdge.type))
                    {
                        edges.Add(viewObjectEdge);
                    }
                }
            }

            foreach (GraphableTransitionViewObjectPortData viewObjectPort in transitionViewObjectPorts)
            {
                foreach (TransitionViewObjectEdgeData viewObjectEdge in viewObjectPort.edges)
                {
                    if (types.HasFlag(viewObjectEdge.type))
                    {
                        edges.Add(viewObjectEdge);
                    }
                }
            }

            foreach (ManualTransitionEdgeData manualEdge in manualEdges)
            {
                if (types.HasFlag(manualEdge.type))
                {
                    edges.Add(manualEdge);
                }
            }

            return edges;
        }

        public void AddFieldEdgeToFieldPort(FieldEdgeData fieldEdge, string fieldPortName)
        {
            GraphableFieldPortData fieldPort = FieldPortWithName(fieldPortName);
            fieldPort.AddEdge(fieldEdge);
        }

        public  void RemoveFieldEdgeFromCurrentPort(FieldEdgeData fieldEdge)
        {
            string fieldPortName = fieldEdge.fieldPortName;
            GraphableFieldPortData fieldPort = FieldPortWithName(fieldPortName);
            fieldPort.RemoveEdge(fieldEdge);
        }

        public void AddEmbedViewObjectEdgeToEmbedViewObjectPortWithEmbedViewObjectGuid(EmbedViewObjectEdgeData embedViewObjectEdge, string embedViewObjectGuid)
        {
            GraphableEmbedViewObjectPortData embedViewObjectPort = EmbedViewObjectPortWithGuid(embedViewObjectGuid);
            embedViewObjectPort.edge = embedViewObjectEdge;
        }

        public void RemoveEmbedViewObjectEdgeFromCurrentPort(EmbedViewObjectEdgeData embedViewObjectEdge)
        {
            string embedViewObjectGuid = embedViewObjectEdge.embedViewObjectGuid;
            GraphableEmbedViewObjectPortData embedViewObjectPort = EmbedViewObjectPortWithGuid(embedViewObjectGuid);
            embedViewObjectPort.edge = null;
        }

        public void AddTransitionViewObjectEdgeToTransitionViewObjectPortWithTransitionViewObjectGuid(TransitionViewObjectEdgeData transitionViewObjectEdge, string transitionViewObjectGuid)
        {
            GraphableTransitionViewObjectPortData transitionViewObjectPort = TransitionViewObjectPortWithGuid(transitionViewObjectGuid);
            transitionViewObjectPort.AddEdge(transitionViewObjectEdge);
        }

        public void RemoveTransitionViewObjectEdgeFromCurrentPort(TransitionViewObjectEdgeData transitionViewObjectEdge)
        {
            string transitionViewObjectGuid = transitionViewObjectEdge.transitionViewObjectGuid;
            GraphableTransitionViewObjectPortData transitionViewObjectPort = TransitionViewObjectPortWithGuid(transitionViewObjectGuid);
            transitionViewObjectPort.RemoveEdge(transitionViewObjectEdge);
        }

        public void AddManualTransitionEdge(ManualTransitionEdgeData manualTransitionEdge)
        {
            manualEdges.Add(manualTransitionEdge);
        }

        public void RemoveManualTransitionEdge(ManualTransitionEdgeData manualTransitionEdge)
        {
            manualEdges.Remove(manualTransitionEdge);
        }

        public GraphableFieldPortData FieldPortWithName(string fieldPortName)
        {
            GraphableFieldPortData fieldPort = null;
            foreach (var fp in fieldPorts)
            {
                if (fp.fieldName.Equals(fieldPortName))
                {
                    fieldPort = fp;
                    break;
                }
            }

            return fieldPort;
        }

        private GraphableEmbedViewObjectPortData EmbedViewObjectPortWithGuid(string embedViewObjectGuid)
        {
            GraphableEmbedViewObjectPortData embedViewObjectPort = null;
            foreach (var evop in embedViewObjectPorts)
            {
                if (evop.embedViewObjectGuid.Equals(embedViewObjectGuid))
                {
                    embedViewObjectPort = evop;
                    break;
                }
            }

            return embedViewObjectPort;
        }

        private GraphableTransitionViewObjectPortData TransitionViewObjectPortWithGuid(string transitionViewObjectGuid)
        {
            GraphableTransitionViewObjectPortData transitionViewObjectPort = null;
            foreach (var tvop in transitionViewObjectPorts)
            {
                if (tvop.transitionViewObjectGuid.Equals(transitionViewObjectGuid))
                {
                    transitionViewObjectPort = tvop;
                    break;
                }
            }

            return transitionViewObjectPort;
        }
#endif
    }
}
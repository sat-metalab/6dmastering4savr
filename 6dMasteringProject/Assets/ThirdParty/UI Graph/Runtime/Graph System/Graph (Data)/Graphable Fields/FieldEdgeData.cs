﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    public class FieldEdgeData : EdgeData
    {
#if UNITY_EDITOR
        public string fieldPortName;

        public ViewControllerNodeData SourceNode
        {
            get
            {
                return sourceNode as ViewControllerNodeData;
            }
        }

        public override void AddToSourceNode()
        {
            SourceNode.AddFieldEdgeToFieldPort(this, fieldPortName);
        }

        public override void AssignDestinationNodeParentIfNecessary()
        {
            // Field edge's always assign their destination parent.
            destinationNode.parent = targetNode;
        }

        public override void RemoveFromSourceNode()
        {
            SourceNode.RemoveFieldEdgeFromCurrentPort(this);
        }

        public override void NullifyDestinationNodeParentIfNecessary()
        {
            // Field edge's always nullify their destination parent.
            destinationNode.parent = null;
        }
#endif
    }
}
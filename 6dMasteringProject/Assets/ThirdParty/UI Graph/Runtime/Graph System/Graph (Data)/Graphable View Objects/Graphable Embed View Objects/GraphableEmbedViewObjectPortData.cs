﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public class GraphableEmbedViewObjectPortData
    {
        public string embedViewObjectGuid;
        public string embedViewObjectDisplayName;
        public EmbedViewObjectEdgeData edge;

#if UNITY_EDITOR
        public GraphableEmbedViewObjectPortData(IGraphableEmbedViewObject viewObject)
        {
            embedViewObjectGuid = viewObject.Guid;
            embedViewObjectDisplayName = viewObject.DisplayName;
        }
#endif
    }
}
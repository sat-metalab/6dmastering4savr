﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    public abstract partial class TransitionEdgeData : EdgeData
    {
        public string transitionGuid;
        public bool animated = true;
        public bool interactive = false;
        public string userIdentifier;

        public GraphableTransitionIdentifier GraphableTransitionIdentifier
        {
            get
            {
                return GraphableTransitionIdentifierWithGuid(transitionGuid);
            }
        }

        public ViewControllerTransitionIdentifier TransitionIdentifier
        {
            get
            {
                return GraphableTransitionIdentifier.TransitionIdentifier;
            }
        }

        private GraphableTransitionIdentifier GraphableTransitionIdentifierWithGuid(string transitionGuid)
        {
            GraphableTransitionIdentifier graphableTransitionIdentifier = null;

            IGraphable targetGraphable = targetNode.resource.Value;
            List<GraphableTransitionIdentifier> transitionIdentifiers = targetGraphable.GraphableTransitionIdentifiers();
            foreach (GraphableTransitionIdentifier transitionIdentifier in transitionIdentifiers)
            {
                if (transitionIdentifier.TransitionGuid.Equals(transitionGuid))
                {
                    graphableTransitionIdentifier = transitionIdentifier;
                    break;
                }
            }

            return graphableTransitionIdentifier;
        }
    }

    public abstract partial class TransitionEdgeData : EdgeData
    {
#if UNITY_EDITOR
        public string transitionDisplayName;

        public virtual void Initialize(GraphResource graph, NodeData sourceNode, NodeData destinationNode, NodeData targetNode, ViewControllerTransitionIdentifier transitionIdentifier)
        {
            guid = System.Guid.NewGuid().ToString();
            this.graph = graph;
            this.sourceNode = sourceNode;
            this.destinationNode = destinationNode;
            this.targetNode = targetNode;
            type = transitionIdentifier.GetTransitionEdgeType();

            transitionGuid = transitionIdentifier.Guid;
            transitionDisplayName = transitionIdentifier.DisplayName;

            //this.name = string.Format("[ViewObjectEdge] {0} --{1}--> {2} ({3})", sourceNode.resource.Name, transitionGuid, destinationNode.resource.Name, viewObjectEdge.guid);
        }

        public override void AssignDestinationNodeParentIfNecessary()
        {
            if (destinationNode.parent == null)
            {
                ViewControllerTransitionIdentifier transitionIdentifier = TransitionIdentifier;
                if (transitionIdentifier.IsDownstreamContainmentTransition())
                {
                    destinationNode.parent = targetNode;
                }
            }
        }

        public override void NullifyDestinationNodeParentIfNecessary()
        {
            if (destinationNode.parent != null)
            {
                if (graph.HasAnyEdgeWithDestinationNode(destinationNode, this) == false)
                {
                    destinationNode.parent = null;
                }
            }
        }
#endif
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public class ManualTransitionEdgeData : TransitionEdgeData
    {
#if UNITY_EDITOR
        public ViewControllerNodeData SourceNode
        {
            get
            {
                return sourceNode as ViewControllerNodeData;
            }
        }

        public override void AddToSourceNode()
        {
            SourceNode.AddManualTransitionEdge(this);
        }

        public override void RemoveFromSourceNode()
        {
            SourceNode.RemoveManualTransitionEdge(this);
        }

        public override void Initialize(GraphResource graph, NodeData sourceNode, NodeData destinationNode, NodeData targetNode, ViewControllerTransitionIdentifier transitionIdentifier)
        {
            base.Initialize(graph, sourceNode, destinationNode, targetNode, transitionIdentifier);
            name = string.Format("[ManualTransitionEdge] {0} --{1}--> {2} ({3})", sourceNode.resource.Name, transitionGuid, destinationNode.resource.Name, guid);
        }
#endif
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public class GraphableReference
    {
#pragma warning disable 0649
        [SerializeField] private ScriptableObject reference;
#pragma warning restore 0649

        public GraphableReference(IGraphable graphable)
        {
            reference = graphable as ScriptableObject;
        }

        public IGraphable Value
        {
            get
            {
                return reference as IGraphable;
            }
        }

        public ScriptableObject RawValue
        {
            get
            {
                return reference;
            }
        }

        public string Name
        {
            get
            {
                return (reference != null) ? reference.name : "Missing Resource";
            }
        }

        public bool TryGetValue(out IGraphable value)
        {
            value = Value;
            return (value != null);
        }
    }
}
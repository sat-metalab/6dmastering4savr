﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Pelican7.UIGraph
{
    [CreateAssetMenu(fileName = "New Graph", menuName = "UI Graph/Graph", order = MenuItemPriority.Group0)]
    public partial class GraphResource : ScriptableObject
    {
        public NodeData origin;
        public List<NodeData> nodes = new List<NodeData>();
    }

    public partial class GraphResource : ScriptableObject
    {
#if UNITY_EDITOR
        public List<EdgeData> edges = new List<EdgeData>();

        public NodeDataEvent OnNodeAdded = new NodeDataEvent();
        public NodeDataEvent OnNodeRemoved = new NodeDataEvent();
        public EdgeDataEvent OnEdgeAdded = new EdgeDataEvent();
        public EdgeDataEvent OnEdgeRemoved = new EdgeDataEvent();
        public OriginChangedEvent OnOriginChanged = new OriginChangedEvent();

        public class NodeDataEvent : UnityEvent<NodeData> { }
        public class EdgeDataEvent : UnityEvent<EdgeData> { }
        public class OriginChangedEvent : UnityEvent<NodeData, NodeData> { }

        public bool HasAnyEdgeWithDestinationNode(NodeData destinationNode, EdgeData excludedEdge = null)
        {
            return HasAnyEdgeOfTypeWithDestinationNode(EdgeData.EdgeType.AllTypes, destinationNode, excludedEdge);
        }

        public bool HasAnyEdgeOfTypeWithDestinationNode(EdgeData.EdgeType types, NodeData destinationNode, EdgeData excludedEdge = null)
        {
            bool hasAnyEdgeWithDestinationNode = false;
            foreach (EdgeData edge in edges)
            {
                if ((edge.Equals(excludedEdge) == false) && types.HasFlag(edge.type))
                {
                    NodeData destination = edge.destinationNode;
                    if (edge.destinationNode.Equals(destinationNode))
                    {
                        hasAnyEdgeWithDestinationNode = true;
                        break;
                    }
                }
            }

            return hasAnyEdgeWithDestinationNode;
        }
#endif
    }
}
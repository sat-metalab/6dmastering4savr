﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph
{
    public abstract partial class NodeData : ScriptableObject
    {
        public string guid;
        public GraphableReference resource;
        public string userIdentifier;

        public abstract Node CreateRuntimeRepresentation(object graph);
        public abstract void ConfigureRuntimeRepresentation(Node node, Dictionary<string, Node> nodes);
    }

    public abstract partial class NodeData : ScriptableObject
    {
#if UNITY_EDITOR
        public GraphResource graph;
        public NodeData parent;
        public bool nodeSettingsVisible = true;

#pragma warning disable 0649
        [SerializeField] private Vector2 position;
#pragma warning restore 0649

        public Vector2 Position
        {
            get
            {
                return position;
            }
        }

        public abstract List<EdgeData> EdgesWithTypes(EdgeData.EdgeType types);
#endif
    }
}
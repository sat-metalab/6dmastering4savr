﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Pelican7.UIGraph
{
    public static class ObjectAttributeCollector
    {
        public static FieldInfo[] CollectAttributedFieldsOnObject<T>(UnityEngine.Object obj, BindingFlags bindingFlags)
            where T : Attribute
        {
            var fields = new List<FieldInfo>();

            var type = obj.GetType();
            var typeFields = type.GetFields(bindingFlags);
            foreach (var field in typeFields)
            {
                var attribute = field.GetCustomAttribute<T>();
                bool fieldHasAttribute = (attribute != null);
                if (fieldHasAttribute)
                {
                    fields.Add(field);
                }
            }

            return fields.ToArray();
        }
    }
}
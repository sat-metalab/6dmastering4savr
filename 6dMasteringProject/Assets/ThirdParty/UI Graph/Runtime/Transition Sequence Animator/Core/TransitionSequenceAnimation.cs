﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The abstract base type for animations to be used with the Transition Sequence Animator.
    /// </summary>
    /// <typeparam name="TViewController"></typeparam>
    /// <typeparam name="TContext"></typeparam>
    public abstract class TransitionSequenceAnimation<TViewController, TContext> : ScriptableObject, ISequencable<TContext>
    {
        /// <summary>
        /// The animation's view controller identifier. The <see cref="TargetViewController"/>  will be configured with this view controller in <see cref="InitializeWithContext(TContext)"/>.
        /// </summary>
        public ViewControllerIdentifier viewControllerIdentifier;
        /// <summary>
        /// The animation's direction.
        /// </summary>
        public Direction transitionDirection;
        /// <summary>
        /// The animation's curve.
        /// </summary>
        public AnimationCurve curve = new AnimationCurve(new Keyframe[]
        {
            new Keyframe(0f, 0f, 3.00052f, 3.00052f),
            new Keyframe(1f, 1f, 0.05463887f, 0.05463887f)
        });
        // If enabled, the animation will be initialized with zero progress when the transition sequence begins.
        /// <summary>
        /// Initialize with zero progress? If enabled, the animation will be initialized with zero progress when the transition sequence begins, regardless of where the animation lies within the sequence. This can therefore be used to apply the animation with zero progress prior to the animation being reached in the sequence.
        /// </summary>
        public bool initializeWithZeroProgress = true;

        /// <summary>
        /// An animation's view controller identifier. Used to populate the <see cref="TargetViewController"/>.
        /// </summary>
        public enum ViewControllerIdentifier
        {
            /// <summary>
            /// The view controller being transitioned to.
            /// </summary>
            To,
            /// <summary>
            /// The view controller being transitioned from.
            /// </summary>
            From,
            /// <summary>
            /// The view controller whom owns, or is performing, the transition.
            /// </summary>
            Owner,
            /// <summary>
            /// Do not automatically target a view controller.
            /// </summary>
            None
        }

        /// <summary>
        /// The animation's direction. This is used by animators to determine the start and end values of their animations.
        /// </summary>
        public enum Direction
        {
            /// <summary>
            /// The animation is transitioning a view controller on-screen.
            /// </summary>
            TransitionOnScreen,
            /// <summary>
            /// The animation is transitioning a view controller off-screen.
            /// </summary>
            TransitionOffScreen
        }

        /// <summary>
        /// The animation's target view controller. This is automatically populated with the view controller identified by <see cref="viewControllerIdentifier"/>.
        /// </summary>
        protected TViewController TargetViewController { get; private set; }
        /// <summary>
        /// The animation's eased progress. This is calculated using the <see cref="curve"/> field.
        /// </summary>
        protected float EasedProgress01 { get; private set; }

        public void InitializeWithContext(TContext context)
        {
            TargetViewController = TargetViewControllerForContext(context);
            InitializeAnimationWithContext(context);
            if (initializeWithZeroProgress)
            {
                UpdateProgress(0f);
            }
        }

        /// <summary>
        /// Override this method to perform any initialization your animation requires.
        /// </summary>
        /// <param name="context"></param>
        public virtual void InitializeAnimationWithContext(TContext context) { }

        /// <summary>
        /// Override this method to perform your animation with the supplied <paramref name="progress01"/>.
        /// </summary>
        /// <param name="progress01"></param>
        public virtual void UpdateProgress(float progress01)
        {
            EasedProgress01 = curve.Evaluate(progress01);
        }

        /// <summary>
        /// Override this method to perform any clean up your animation requires, if necessary.
        /// </summary>
        /// <param name="completed"></param>
        public virtual void OnTransitionFinished(bool completed) { }

        protected abstract TViewController TargetViewControllerForContext(TContext context);
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    // [Developer Note] Currently for internal use only.
    /// <summary>
    /// The ElementsControllerSequenceProvider is a specific type of <see cref="ElementsControllerTransitionAnimatorProvider"/> for working with the Transition Sequence Animator and UI Elements Workflow. It provides <see cref="ElementsControllerTransitionSequenceAnimator"/> objects to the View Controller system to determine how the transition is animated. It allows you to provide an <see cref="ElementsControllerTransitionSequenceAnimator"/> for a <see cref="ViewControllerTransitionIdentifier"/>.
    /// </summary>
    //[CreateAssetMenu(fileName = "New Sequence Provider", menuName = "UI Graph/UIElements/Transition Sequence Animator/Sequence Provider", order = MenuItemPriority.Group0)]
    public class ElementsControllerSequenceProvider : ElementsControllerTransitionAnimatorProvider
    {
        /// <summary>
        /// The provider's list of animators.
        /// </summary>
        public ElementsControllerSequenceProviderAnimatorData[] animators;

        public override IViewControllerTransitionAnimator<ElementsControllerTransitionContext> AnimatorForTransition(ElementsControllerTransitionContext context)
        {
            ElementsControllerTransitionSequenceAnimator animator = null;

            string guid = context.Identifier.Guid;
            foreach (ElementsControllerSequenceProviderAnimatorData animatorData in animators)
            {
                if (animatorData.transitionGuid.Equals(guid))
                {
                    animator = animatorData.AnimatorForContext(context);
                    break;
                }
            }

            return (animator != null) ? Instantiate(animator) : null;
        }
    }
}
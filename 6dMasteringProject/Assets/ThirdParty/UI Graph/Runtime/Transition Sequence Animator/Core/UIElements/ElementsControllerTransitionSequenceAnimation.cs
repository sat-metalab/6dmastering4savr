﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The base type for animations to be used with the Transition Sequence Animator when working with the UI Elements workflow.
    /// </summary>
    public class ElementsControllerTransitionSequenceAnimation : TransitionSequenceAnimation<ElementsController, ElementsControllerTransitionContext>
    {
        protected override ElementsController TargetViewControllerForContext(ElementsControllerTransitionContext context)
        {
            ElementsController target = null;
            switch (viewControllerIdentifier)
            {
                case ViewControllerIdentifier.To:
                    {
                        target = context.ToViewController;
                        break;
                    }

                case ViewControllerIdentifier.From:
                    {
                        target = context.FromViewController;
                        break;
                    }

                case ViewControllerIdentifier.Owner:
                    {
                        target = context.OwnerViewController;
                        break;
                    }
            }

            return target;
        }
    }
}
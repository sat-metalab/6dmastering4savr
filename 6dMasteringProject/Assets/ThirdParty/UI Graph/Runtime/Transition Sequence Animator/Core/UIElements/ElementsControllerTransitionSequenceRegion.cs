﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The type for sequence regions to be used with the Transition Sequence Animator when working with the UI Elements workflow.
    /// </summary>
    [System.Serializable]
    public class ElementsControllerTransitionSequenceRegion : TransitionSequenceRegion<ElementsControllerTransitionSequenceAnimation, ElementsControllerTransitionContext> { }
}
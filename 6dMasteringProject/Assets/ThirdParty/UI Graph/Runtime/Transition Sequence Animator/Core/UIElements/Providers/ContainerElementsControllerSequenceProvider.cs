﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    // [Developer Note] Currently for internal use only.
    /// <summary>
    /// The ContainerElementsControllerSequenceProvider is a specific type of <see cref="ElementsControllerSequenceProvider"/> for working with container view controllers. In addition to its base class functionality, it allows you to specify a fallback provider. This allows you to specify only your container's transitions and fall back to, for example, the default sequence provider.
    /// </summary>
    //[CreateAssetMenu(fileName = "New Container Sequence Provider", menuName = "UI Graph/UIElements/Transition Sequence Animator/Container Sequence Provider", order = MenuItemPriority.Group0)]
    public class ContainerElementsControllerSequenceProvider : ElementsControllerSequenceProvider
    {
        /// <summary>
        /// The provider's fallback provider. Use the fallback provider to supply animators for the standard present/dismiss transitions.
        /// </summary>
        public ElementsControllerSequenceProvider fallbackProvider;

        public override IViewControllerTransitionAnimator<ElementsControllerTransitionContext> AnimatorForTransition(ElementsControllerTransitionContext context)
        {
            // The animator returned from AnimatorForTransition is an instantiated object instance, so we don't need to instantiate it.
            IViewControllerTransitionAnimator<ElementsControllerTransitionContext> animator = base.AnimatorForTransition(context);
            if (animator == null)
            {
                animator = fallbackProvider.AnimatorForTransition(context);
            }

            return animator;
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The RotateCanvasControllerTransitionAnimation performs a rotation animation of its <see cref="TransitionSequenceAnimation{TViewController, TContext}.TargetViewController"/>.
    /// <para>
    /// Specify the <see cref="startRotationEulerAngles"/> and <see cref="endRotationEulerAngles"/> to determine the view's start and end rotations. For example, a startRotationEulerAngles value of {0, 0, 0} and an endRotationEulerAngles of {0, 180, 0} would flip the view 180° around the Y axis.
    /// </para>
    /// <para>
    /// The <see cref="TransitionSequenceAnimation{TViewController, TContext}.transitionDirection"/> is ignored by this animator.
    /// </para>
    /// </summary>
    [CreateAssetMenu(fileName = "New Rotate Animation", menuName = "UI Graph/UICanvas/Transition Sequence Animator/Rotate Animation", order = MenuItemPriority.Group1)]
    public class RotateCanvasControllerTransitionAnimation : CanvasControllerTransitionSequenceAnimation
    {
        /// <summary>
        /// The animation's start rotation.
        /// </summary>
        public Vector3 startRotationEulerAngles;
        /// <summary>
        /// The animation's end rotation.
        /// </summary>
        public Vector3 endRotationEulerAngles;

        private Quaternion startRotation;
        private Quaternion endRotation;

        public override void InitializeAnimationWithContext(CanvasControllerTransitionContext context)
        {
            base.InitializeAnimationWithContext(context);

            startRotation = Quaternion.Euler(startRotationEulerAngles);
            endRotation = Quaternion.Euler(endRotationEulerAngles);
        }

        public override void UpdateProgress(float progress01)
        {
            base.UpdateProgress(progress01);

            var targetTransform = TargetViewController.View.Transform;
            targetTransform.Rotation = Quaternion.SlerpUnclamped(startRotation, endRotation, EasedProgress01);
        }
    }
}
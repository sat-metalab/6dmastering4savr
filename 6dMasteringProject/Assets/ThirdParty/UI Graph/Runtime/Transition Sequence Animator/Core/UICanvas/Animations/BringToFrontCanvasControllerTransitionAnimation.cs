﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The BringToFrontCanvasControllerTransitionAnimation brings its <see cref="TransitionSequenceAnimation{TViewController, TContext}.TargetViewController"/>'s view to the front of its siblings. It can be used to adjust the render order of a view controller midway through a transition sequence.
    /// <para>
    /// The <see cref="TransitionSequenceAnimation{TViewController, TContext}.transitionDirection"/> and <see cref="TransitionSequenceAnimation{TViewController, TContext}.curve"/> are ignored by this animator.
    /// </para>
    /// </summary>
    [CreateAssetMenu(fileName = "New Bring To Front Animation", menuName = "UI Graph/UICanvas/Transition Sequence Animator/Bring To Front Animation", order = MenuItemPriority.Group1)]
    public class BringToFrontCanvasControllerTransitionAnimation : CanvasControllerTransitionSequenceAnimation
    {
        private bool activated = false;
        private int? viewIndexBeforeAnimation; 

        public override void UpdateProgress(float progress01)
        {
            base.UpdateProgress(progress01);
            if (activated == false)
            {
                Activate();
            }
        }

        public override void OnTransitionFinished(bool completed)
        {
            base.OnTransitionFinished(completed);

            // If the transition was not completed, move the view back to its original index if necessary.
            if (completed == false)
            {
                ReverseIfNecessary();
            }
        }

        private void Activate()
        {
            viewIndexBeforeAnimation = TargetViewController.View.transform.GetSiblingIndex();
            TargetViewController.View.BringToFront();
            activated = true;
        }

        private void ReverseIfNecessary()
        {
            if (viewIndexBeforeAnimation.HasValue)
            {
                TargetViewController.View.transform.SetSiblingIndex(viewIndexBeforeAnimation.Value);
            }
        }

        private void OnValidate()
        {
            initializeWithZeroProgress = false;
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A CanvasControllerTransitionSequenceAnimator is used with the Transition Sequence Animator when working with the UI Canvas workflow. It defines a single transition sequence, made up of a collection of regions, each containing a <see cref="CanvasControllerTransitionSequenceAnimation"/> object.
    /// </summary>
    [CreateAssetMenu(fileName = "New Sequence", menuName = "UI Graph/UICanvas/Transition Sequence Animator/Sequence", order = MenuItemPriority.Group0)]
    public class CanvasControllerTransitionSequenceAnimator : TransitionSequenceAnimator<CanvasControllerTransitionSequenceRegion, CanvasControllerTransitionContext> { }
}
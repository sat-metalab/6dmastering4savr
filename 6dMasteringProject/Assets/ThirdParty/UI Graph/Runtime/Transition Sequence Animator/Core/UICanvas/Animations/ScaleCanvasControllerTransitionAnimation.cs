﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The ScaleCanvasControllerTransitionAnimation performs a scale animation of its <see cref="TransitionSequenceAnimation{TViewController, TContext}.TargetViewController"/>.
    /// <para>
    /// Specify the <see cref="offScreenScale"/> to determine the view's scale value when the view controller is 'off-screen' and a <see cref="TransitionSequenceAnimation{TViewController, TContext}.transitionDirection"/> to determine whether the animator is scaling to or from this value. For example, an offScreenScale value of Vector2.zero and a direction of TransitionOnScreen would scale up from Vector2.zero to Vector2.one.
    /// </para>
    /// </summary>
    [CreateAssetMenu(fileName = "New Scale Animation", menuName = "UI Graph/UICanvas/Transition Sequence Animator/Scale Animation", order = MenuItemPriority.Group1)]
    public class ScaleCanvasControllerTransitionAnimation : CanvasControllerTransitionSequenceAnimation
    {
        /// <summary>
        /// The scale value when the view controller is off-screen.
        /// </summary>
        public Vector2 offScreenScale;

        private Vector2 startScale;
        private Vector2 endScale;

        public override void InitializeAnimationWithContext(CanvasControllerTransitionContext context)
        {
            base.InitializeAnimationWithContext(context);
            startScale = StartScaleForViewController();
            endScale = EndScaleForViewController();
        }

        public override void UpdateProgress(float progress01)
        {
            base.UpdateProgress(progress01);

            var targetTransform = TargetViewController.View.Transform;
            targetTransform.Scale = Vector3.LerpUnclamped(startScale, endScale, EasedProgress01);
        }

        private Vector2 StartScaleForViewController()
        {
            if (transitionDirection == Direction.TransitionOffScreen)
            {
                return Vector2.one;
            }

            return offScreenScale;
        }

        private Vector2 EndScaleForViewController()
        {
            if (transitionDirection == Direction.TransitionOnScreen)
            {
                return Vector2.one;
            }

            return offScreenScale;
        }
    }
}
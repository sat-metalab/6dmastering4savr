﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The FadeCanvasControllerTransitionAnimation performs a fade animation of its <see cref="TransitionSequenceAnimation{TViewController, TContext}.TargetViewController"/>.
    /// <para>
    /// Specify the <see cref="offScreenAlpha"/> to determine the view's alpha value when the view controller is 'off-screen' and a <see cref="TransitionSequenceAnimation{TViewController, TContext}.transitionDirection"/> to determine whether the animator is fading to or from this value. For example, an offScreenAlpha value of zero and a direction of TransitionOnScreen would fade in from zero to one.
    /// </para>
    /// </summary>
    [CreateAssetMenu(fileName = "New Fade Animation", menuName = "UI Graph/UICanvas/Transition Sequence Animator/Fade Animation", order = MenuItemPriority.Group1)]
    public class FadeCanvasControllerTransitionAnimation : CanvasControllerTransitionSequenceAnimation
    {
        /// <summary>
        /// The alpha value when the view controller is off-screen.
        /// </summary>
        public float offScreenAlpha;

        private float startAlpha;
        private float endAlpha;

        public override void InitializeAnimationWithContext(CanvasControllerTransitionContext context)
        {
            base.InitializeAnimationWithContext(context);
            startAlpha = StartAlphaForViewController();
            endAlpha = EndAlphaForViewController();
        }

        public override void UpdateProgress(float progress01)
        {
            base.UpdateProgress(progress01);

            var targetView = TargetViewController.View;
            targetView.Alpha = Mathf.LerpUnclamped(startAlpha, endAlpha, EasedProgress01);
        }

        private float StartAlphaForViewController()
        {
            if (transitionDirection == Direction.TransitionOffScreen)
            {
                return 1f;
            }

            return offScreenAlpha;
        }

        private float EndAlphaForViewController()
        {
            if (transitionDirection == Direction.TransitionOnScreen)
            {
                return 1f;
            }

            return offScreenAlpha;
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The abstract base type for sequence regions to be used with the Transition Sequence Animator.
    /// </summary>
    /// <typeparam name="TAnimation"></typeparam>
    /// <typeparam name="TContext"></typeparam>
    public abstract class TransitionSequenceRegion<TAnimation, TContext> : ISequencable<TContext>
        where TAnimation : Object, ISequencable<TContext>
    {
        /// <summary>
        /// The region's normalized start time, relative to its sequence.
        /// </summary>
        [Range(0f, 1f)] public float startTime01 = 0f;
        /// <summary>
        /// The region's normalized end time, relative to its sequence.
        /// </summary>
        [Range(0f, 1f)] public float endTime01 = 1f;

#pragma warning disable 0649
        [SerializeField] private TAnimation animation;
#pragma warning restore 0649
        private bool completed = false;

        private TAnimation Animation { get; set; }

        public void InitializeWithContext(TContext context)
        {
            // Instantiate an instance of the referenced animation object.
            Animation = Object.Instantiate(animation);
            Animation.InitializeWithContext(context);
        }

        public void UpdateProgress(float progress01)
        {
            if ((progress01 < startTime01) || completed)
            {
                return;
            }

            var regionProgress01 = Mathf.InverseLerp(startTime01, endTime01, progress01);

            // If start and end time are equal, InverseLerp will always return zero. Make it one when greater than end time.
            if (Mathf.Approximately(startTime01, endTime01))
            {
                regionProgress01 = (progress01 > endTime01) ? 1f : 0f;
            }

            Animation.UpdateProgress(regionProgress01);

            if (regionProgress01 >= 1f)
            {
                completed = true;
            }
        }

        public void OnTransitionFinished(bool completed)
        {
            Animation.OnTransitionFinished(completed);
        }
    }
}
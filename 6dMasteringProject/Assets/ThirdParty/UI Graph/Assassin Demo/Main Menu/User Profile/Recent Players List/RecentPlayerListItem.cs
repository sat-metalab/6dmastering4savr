// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using TMPro;
using UnityEngine.UI;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class RecentPlayerListItem : ListViewItem
    {
        public TextMeshProUGUI nameLabel;
        public TextMeshProUGUI lastSeenLabel;
        public Image avatar;

        public void ConfigureWithItem(RecentPlayerDataItem item)
        {
            nameLabel.SetText(item.name);
            lastSeenLabel.SetText(item.lastSeen);
            avatar.color = item.avatar;
        }
    }
}
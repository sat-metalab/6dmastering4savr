// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using TMPro;
using UnityEngine.UI;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class FriendListItem : ListViewItem
    {
        public TextMeshProUGUI nameLabel;
        public Image avatar;

        public void ConfigureWithItem(FriendDataItem item)
        {
            nameLabel.SetText(item.name);
            avatar.color = item.avatar;
        }
    }
}
// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph.Demos.Assassin
{
    public partial class FriendsListCanvasController : CanvasController
    {
        [ViewReference] public ListView listView;
        public FriendsListDataModel dataModel;
        public FriendListItem itemTemplate;

        protected override void ViewDidLoad()
        {
            base.ViewDidLoad();
            listView.DataSource = this;
            listView.ReloadData();
        }
    }

    public partial class FriendsListCanvasController : CanvasController, ListView.IListViewDataSource
    {
        int ListView.IListViewDataSource.NumberOfItemsForListView(ListView list)
        {
            return dataModel.ItemCount;
        }

        ListViewItem ListView.IListViewDataSource.ListViewItemForIndex(ListView list, int index)
        {
            FriendListItem item = Instantiate(itemTemplate);

            FriendDataItem dataItem = dataModel[index];
            item.ConfigureWithItem(dataItem);

            return item;
        }
    }
}
// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class PrimaryOptionsCanvasController : CanvasController
    {
        [ViewReference] public PrimaryOptionButton initialSelectedOption;

        private PrimaryOptionButton selectedOptionButton;

        [ViewCallback]
        public void OnOptionSelected(PrimaryOptionButton optionButton)
        {
            // Deselect the current option, if necessary.
            selectedOptionButton?.SetSelected(false);

            // Select the new option.
            selectedOptionButton = optionButton;
            selectedOptionButton.SetSelected(true);
        }

        protected override void ViewDidLoad()
        {
            base.ViewDidLoad();
            OnOptionSelected(initialSelectedOption);
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(initialSelectedOption.gameObject);
        }
    }
}
// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using TMPro;
using UnityEngine;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class OptionListItem : ListViewItem
    {
        public TextMeshProUGUI nameLabel;
        public TextMeshProUGUI valueLabel;
        public GameObject selectedBackground;

        public void ConfigureWithItem(OptionDataItem item)
        {
            nameLabel.SetText(item.name);

            string valueText = (item.value) ? "[ON]" : "[OFF]";
            valueLabel.SetText(valueText);
        }

        public void SetSelected(bool selected)
        {
            selectedBackground.SetActive(selected);
        }
    }
}
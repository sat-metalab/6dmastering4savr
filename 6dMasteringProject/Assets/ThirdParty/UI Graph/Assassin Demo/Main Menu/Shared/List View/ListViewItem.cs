// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class ListViewItem : MonoBehaviour
    {
        public ListViewItemEvent OnSelected;
        public ListViewItemEvent OnDeselected;

        public void Select()
        {
            OnSelected.Invoke(this);
        }

        public void Deselect()
        {
            OnDeselected.Invoke(this);
        }

        [System.Serializable]
        public class ListViewItemEvent : UnityEvent<ListViewItem> { }
    }
}
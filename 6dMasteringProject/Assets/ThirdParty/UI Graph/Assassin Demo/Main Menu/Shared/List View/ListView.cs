// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.Events;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class ListView : MonoBehaviour
    {
        private ListViewItem[] items;

        public ListViewSelectionEvent OnListViewSelectedItemAtIndex;

        public IListViewDataSource DataSource { get; set; }
        public int SelectedIndex { get; private set; }

        public void ReloadData()
        {
            DestroyAllItemsIfNecessary();

            int itemCount = DataSource.NumberOfItemsForListView(this);
            items = new ListViewItem[itemCount];
            for (int i = 0; i < itemCount; i++)
            {
                ListViewItem item = DataSource.ListViewItemForIndex(this, i);
                item.transform.SetParent(transform, false);
                item.OnSelected.AddListener(OnItemSelected);
                items[i] = item;
            }
        }

        public void SelectItemAtIndex(int index)
        {
            var item = items[index];
            item.Select();
        }

        private void OnItemSelected(ListViewItem item)
        {
            int index = System.Array.IndexOf(items, item);
            SelectedIndex = index;
            OnListViewSelectedItemAtIndex.Invoke(this, item, index);
        }

        private void DestroyAllItemsIfNecessary()
        {
            if (items != null)
            {
                for (int i = 0; i < items.Length; i++)
                {
                    var item = items[i];
                    Destroy(item.gameObject);
                }
            }
        }

        public interface IListViewDataSource
        {
            int NumberOfItemsForListView(ListView list);
            ListViewItem ListViewItemForIndex(ListView list, int index);
        }

        [System.Serializable]
        public class ListViewSelectionEvent : UnityEvent<ListView, ListViewItem, int> { }
    }
}
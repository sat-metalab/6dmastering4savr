// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class ScaleContentTransitionAnimation : CanvasControllerTransitionSequenceAnimation
    {
        public Vector2 offScreenScale;

        private Transform targetTransform;
        private Vector2 startScale;
        private Vector2 endScale;

        public override void InitializeAnimationWithContext(CanvasControllerTransitionContext context)
        {
            base.InitializeAnimationWithContext(context);

            AlertCanvasController alertCanvasController = TargetViewController as AlertCanvasController;
            if (alertCanvasController == null)
            {
                Error.Log("ScaleContentTransitionAnimation expected an AlertCanvasController.");
                return;
            }

            targetTransform = alertCanvasController.content;
            startScale = StartScaleForViewController();
            endScale = EndScaleForViewController();
        }

        public override void UpdateProgress(float progress01)
        {
            base.UpdateProgress(progress01);
            targetTransform.localScale = Vector3.LerpUnclamped(startScale, endScale, EasedProgress01);
        }

        private Vector2 StartScaleForViewController()
        {
            if (transitionDirection == Direction.TransitionOffScreen)
            {
                return Vector2.one;
            }

            return offScreenScale;
        }

        private Vector2 EndScaleForViewController()
        {
            if (transitionDirection == Direction.TransitionOnScreen)
            {
                return Vector2.one;
            }

            return offScreenScale;
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.UI;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class HomeNavigationBarBackButton : MonoBehaviour
    {
        public CanvasGroup canvasGroup;
        public Image raycast;
        public AnimationCurve alphaTransitionCurve;

        private NavigationCanvasController navigationController;

        public void Configure(CanvasController navigationController)
        {
            this.navigationController = (NavigationCanvasController)navigationController;
        }

        public void PopViewController()
        {
            navigationController.Pop();
        }

        public void TrackNavigationTransition(CanvasControllerTransition transition)
        {
            CanvasControllerTransitionContext context = transition.Context;
            if (context.Animated)
            {
                // In an animated transition, track the transition to crossfade the button, if necessary.
                if (TryGetFadeIconValuesForTransition(transition, out float fromAlpha, out float toAlpha))
                {
                    transition.OnTransitionProgressUpdated.AddListener((t, progress01) =>
                    {
                        FadeButton(fromAlpha, toAlpha, progress01);
                    });
                }
            }
        }

        public void ReloadAfterTransition(CanvasControllerTransition transition, bool completed)
        {
            // After a transition, update the canvas group's interactibility.
            var context = transition.Context;
            var navigationController = (NavigationCanvasController)context.OwnerViewController;
            bool backButtonEnabled = (navigationController.ViewControllers.Length > navigationController.LowestAllowedStackCount);
            canvasGroup.interactable = backButtonEnabled;
            canvasGroup.alpha = (backButtonEnabled) ? 1f : 0f;
            raycast.raycastTarget = backButtonEnabled;
        }

        private bool TryGetFadeIconValuesForTransition(CanvasControllerTransition transition, out float fromAlpha, out float toAlpha)
        {
            CanvasControllerTransitionContext context = transition.Context;
            var navigationController = (NavigationCanvasController)context.OwnerViewController;
            var rootViewController = navigationController.RootViewController;
            ViewControllerTransitionIdentifier transitionIdentifier = context.Identifier;

            // If transitioning from the root view controller, fade the canvas group in.
            if ((rootViewController != null) && rootViewController.Equals(context.FromViewController))
            {
                fromAlpha = 0f;
                toAlpha = 1f;
                return true;
            }
            // If transitioning to the root view controller, fade the canvas group out.
            else if ((rootViewController != null) && rootViewController.Equals(context.ToViewController))
            {
                fromAlpha = 1f;
                toAlpha = 0f;
                return true;
            }

            fromAlpha = 0f;
            toAlpha = 0f;
            return false;
        }

        private void FadeButton(float startAlpha, float endAlpha, float progress01)
        {
            float easedProgress01 = alphaTransitionCurve.Evaluate(progress01);
            float alpha = Mathf.Lerp(startAlpha, endAlpha, easedProgress01);
            canvasGroup.alpha = alpha;
        }
    }
}
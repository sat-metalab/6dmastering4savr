﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class CameraPan : MonoBehaviour
    {
        public Transform target;
        public new Camera camera;
        public Vector2 angleSpread;

        private Quaternion defaultRotation;

        private void Awake()
        {
            defaultRotation = target.rotation;
        }

        void Update()
        {
            Vector3 mousePosition = Input.mousePosition;
            Vector3 mousePosition01 = new Vector3(mousePosition.x / Screen.width, mousePosition.y / Screen.height);

            float xAngle = Mathf.Lerp(-angleSpread.x, angleSpread.x, mousePosition01.x);
            float yAngle = Mathf.Lerp(angleSpread.y, -angleSpread.y, mousePosition01.y);

            Quaternion xRotation = Quaternion.AngleAxis(xAngle, Vector3.up);
            Quaternion yRotation = Quaternion.AngleAxis(yAngle, Vector3.right);
            target.rotation = defaultRotation * xRotation * yRotation;
        }
    }
}
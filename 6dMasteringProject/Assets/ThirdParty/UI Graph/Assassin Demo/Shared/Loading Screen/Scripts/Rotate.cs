﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class Rotate : MonoBehaviour
    {
        public Vector3 axis;
        public float degreesPerSecond;

        private void Update()
        {
            Quaternion rotation = Quaternion.AngleAxis(Time.deltaTime * degreesPerSecond, axis);
            transform.rotation *= rotation;
        }
    }
}
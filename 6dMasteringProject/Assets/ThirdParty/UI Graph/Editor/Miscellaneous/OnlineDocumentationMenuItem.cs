﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    internal static class OnlineDocumentationMenuItem
    {
        private const string OnlineDocumentationURL = "https://uigraph.xyz/";

        [MenuItem("Window/UI Graph/Online Documentation")]
        private static void OpenOnlineDocumentation()
        {
            Application.OpenURL(OnlineDocumentationURL);
        }
    }
}
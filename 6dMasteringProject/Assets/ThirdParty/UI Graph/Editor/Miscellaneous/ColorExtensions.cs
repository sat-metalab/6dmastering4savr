﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class ColorUtility
    {
        public static readonly Color GraphDownstreamColorTransparent = (EditorGUIUtility.isProSkin) ? new Color(0.22f, 0.702f, 0.871f, 0.9f) : new Color(0.035f, 0.725f, 0.965f, 0.9f);
        public static readonly Color GraphUpstreamColorTransparent = new Color(0.87f, 0.39f, 0.22f, 0.9f);
        public static readonly Color GraphEmbedColorTransparent = (EditorGUIUtility.isProSkin)? new Color(0.871f, 0.714f, 0.22f, 0.9f) : new Color(0.965f, 0.792f, 0.235f, 0.9f);
    }
}
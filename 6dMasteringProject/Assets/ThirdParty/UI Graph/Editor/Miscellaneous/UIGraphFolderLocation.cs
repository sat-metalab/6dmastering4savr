﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public class UIGraphFolderLocation : ScriptableObject
    {
        private const string UIGraphPackageRootFolderGuid = "b294e51a94afc7d419aa1283794985cc";

        public static UIGraphFolderLocation UIGraphPackageRootFolder
        {
            get
            {
                return FolderWithGuid(UIGraphPackageRootFolderGuid);
            }
        }

        public static UIGraphFolderLocation FolderWithGuid(string guid)
        {
            return AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<UIGraphFolderLocation>(guid);
        }

        public string Directory
        {
            get
            {
                var assetPath = AssetDatabase.GetAssetPath(this);
                return Path.GetDirectoryName(assetPath);
            }
        }

        public string FullDirectory
        {
            get
            {
                return Path.Combine(DataPathWithoutAssets, Directory);
            }
        }

        private string DataPathWithoutAssets
        {
            get
            {
                string assetsString = "Assets";
                StringBuilder stringBuilder = new StringBuilder(Application.dataPath);
                stringBuilder.Remove(stringBuilder.Length - assetsString.Length, assetsString.Length);
                return stringBuilder.ToString();
            }
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public abstract class SequenceProviderAnimatorDataPropertyDrawerBase<T> : PropertyDrawer
    {
        private static string[] viewControllerTransitionIdentifierGUIDs;

        private const float UnityLineHeight = 16f;
        private const float UnityLineSpacing = 2f;
        private const float UnityTotalLineHeight = UnityLineHeight + UnityLineSpacing;
        private const int LineCount = 5;

        private string[] ViewControllerTransitionIdentifierGUIDs
        {
            get
            {
                if (viewControllerTransitionIdentifierGUIDs == null)
                {
                    viewControllerTransitionIdentifierGUIDs = FindAllViewControllerTransitionIdentifierGUIDs();
                }

                return viewControllerTransitionIdentifierGUIDs;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            int lineCount = LineCount;
            SerializedProperty useAlternateAnimatorForInteractiveTransition = property.FindPropertyRelative("useAlternateAnimatorForInteractiveTransition");
            if (useAlternateAnimatorForInteractiveTransition.boolValue == false)
            {
                lineCount -= 1;
            }

            return UnityLineHeight + ((UnityTotalLineHeight * (lineCount - 1)) * ((property.isExpanded) ? 1 : 0));
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            Rect rect = new Rect(position.x, position.y, position.width, UnityLineHeight);
            property.isExpanded = EditorGUI.Foldout(rect, property.isExpanded, label, true);
            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;

                DrawTransitionIdentifierGuidProperty(ref rect, property);

                rect.y += UnityTotalLineHeight;
                SerializedProperty animator = property.FindPropertyRelative("animator");
                EditorGUI.PropertyField(rect, animator);

                rect.y += UnityTotalLineHeight;
                SerializedProperty useAlternateAnimatorForInteractiveTransition = property.FindPropertyRelative("useAlternateAnimatorForInteractiveTransition");
                position.y += UnityTotalLineHeight;
                EditorGUI.PropertyField(rect, useAlternateAnimatorForInteractiveTransition);

                if (useAlternateAnimatorForInteractiveTransition.boolValue)
                {
                    rect.y += UnityTotalLineHeight;
                    SerializedProperty interactiveAnimator = property.FindPropertyRelative("interactiveAnimator");
                    position.y += UnityTotalLineHeight;
                    EditorGUI.PropertyField(rect, interactiveAnimator);
                }

                EditorGUI.indentLevel--;
            }

            EditorGUI.EndProperty();
        }

        private void DrawTransitionIdentifierGuidProperty(ref Rect rect, SerializedProperty property)
        {
            rect.y += UnityTotalLineHeight;
            SerializedProperty guid = property.FindPropertyRelative("transitionGuid");
            float rectSplit01 = 0.86f;
            float splitPadding = 4f;
            var leftRect = new Rect(rect.x, rect.y, (rect.width * rectSplit01) - splitPadding, rect.height);
            var rightRect = new Rect(rect.x + leftRect.width + splitPadding, rect.y, rect.width * (1 - rectSplit01), rect.height);
            EditorGUI.BeginChangeCheck();
            int selectedIndex = Array.IndexOf(ViewControllerTransitionIdentifierGUIDs, guid.stringValue);
            selectedIndex = EditorGUI.Popup(leftRect, guid.displayName, selectedIndex, ViewControllerTransitionIdentifierGUIDs);
            if (EditorGUI.EndChangeCheck())
            {
                guid.serializedObject.Update();
                guid.stringValue = ViewControllerTransitionIdentifierGUIDs[selectedIndex];
                guid.serializedObject.ApplyModifiedProperties();
            }

            if (GUI.Button(rightRect, "Refresh"))
            {
                RefreshViewControllerTransitionIdentifierGUIDs();
            }
        }

        private void RefreshViewControllerTransitionIdentifierGUIDs()
        {
            viewControllerTransitionIdentifierGUIDs = FindAllViewControllerTransitionIdentifierGUIDs();
        }

        private string[] FindAllViewControllerTransitionIdentifierGUIDs()
        {
            var transitionIdentifierGuidsList = new List<string>();
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (typeof(T).IsAssignableFrom(type))
                    {
                        FieldInfo[] fieldInfos = type.GetFields(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                        foreach (FieldInfo fieldInfo in fieldInfos)
                        {
                            if (typeof(ViewControllerTransitionIdentifier).IsAssignableFrom(fieldInfo.FieldType))
                            {
                                var transitionIdentifier = fieldInfo.GetValue(null) as ViewControllerTransitionIdentifier;
                                transitionIdentifierGuidsList.Add(transitionIdentifier.Guid);
                            }
                        }
                    }
                }
            }

            string[] transitionIdentifierGuids = transitionIdentifierGuidsList.ToArray();
            Array.Sort(transitionIdentifierGuids);
            return transitionIdentifierGuids;
        }
    }
}
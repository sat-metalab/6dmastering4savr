%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!114 &11400000
MonoBehaviour:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_GameObject: {fileID: 0}
  m_Enabled: 1
  m_EditorHideFlags: 0
  m_Script: {fileID: 11500000, guid: 50be309929285204d80054b5da04933c, type: 3}
  m_Name: Default Split Canvas Controller Sequence Provider
  m_EditorClassIdentifier: 
  animators:
  - transitionGuid: SplitCanvasController.SetMain
    animator: {fileID: 11400000, guid: 27b4fb05fd456594a9b7897cff435fb9, type: 2}
    useAlternateAnimatorForInteractiveTransition: 0
    interactiveAnimator: {fileID: 0}
  - transitionGuid: SplitCanvasController.SetSecondary
    animator: {fileID: 11400000, guid: 27b4fb05fd456594a9b7897cff435fb9, type: 2}
    useAlternateAnimatorForInteractiveTransition: 0
    interactiveAnimator: {fileID: 0}
  - transitionGuid: SplitCanvasController.ShowSecondary
    animator: {fileID: 11400000, guid: 6b7ba591341f59941a78c1f39e58436a, type: 2}
    useAlternateAnimatorForInteractiveTransition: 1
    interactiveAnimator: {fileID: 11400000, guid: 0bbb9446b7bf18945b3b7bf2953d32bd,
      type: 2}
  - transitionGuid: SplitCanvasController.HideSecondary
    animator: {fileID: 11400000, guid: 6b7ba591341f59941a78c1f39e58436a, type: 2}
    useAlternateAnimatorForInteractiveTransition: 1
    interactiveAnimator: {fileID: 11400000, guid: 0bbb9446b7bf18945b3b7bf2953d32bd,
      type: 2}
  fallbackProvider: {fileID: 0}

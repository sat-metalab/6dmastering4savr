﻿using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class AssetDatabaseExtensions
    {
        public static string[] FindAssetsOfType<T>()
            where T : Object
        {
            return AssetDatabase.FindAssets(string.Format("t: {0}", typeof(T).FullName));
        }

        public static string[] FindAssetsOfType(System.Type type)
        {
            return AssetDatabase.FindAssets(string.Format("t: {0}", type.FullName));
        }

        public static string FindFirstAssetOfType<T>()
            where T : Object
        {
            string guid = string.Empty;

            string[] guids = FindAssetsOfType<T>();
            if ((guids != null) && (guids.Length > 0))
            {
                guid = guids[0];
            }

            return guid;
        }

        public static string FindFirstAssetOfType(System.Type type)
        {
            string guid = string.Empty;

            string[] guids = FindAssetsOfType(type);
            if ((guids != null) && (guids.Length > 0))
            {
                guid = guids[0];
            }

            return guid;
        }

        public static T LoadFirstAssetOfType<T>()
            where T : Object
        {
            T asset = null;

            string guid = FindFirstAssetOfType<T>();
            if (string.IsNullOrEmpty(guid) == false)
            {
                var assetPath = AssetDatabase.GUIDToAssetPath(guid);
                asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
            }

            return asset;
        }

        public static Object LoadFirstAssetOfType(System.Type type)
        {
            Object asset = null;

            string guid = FindFirstAssetOfType(type);
            if (string.IsNullOrEmpty(guid) == false)
            {
                var assetPath = AssetDatabase.GUIDToAssetPath(guid);
                asset = AssetDatabase.LoadAssetAtPath(assetPath, type);
            }

            return asset;
        }

        public static T[] LoadAllAssetsOfType<T>()
            where T : Object
        {
            string[] guids = FindAssetsOfType<T>();
            T[] assets = new T[guids.Length];
            for (int i = 0; i < guids.Length; i++)
            {
                string guid = guids[i];
                string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                assets[i] = AssetDatabase.LoadAssetAtPath<T>(assetPath);
            }

            return assets;
        }

        public static Object[] LoadAllAssetsOfType(System.Type type)
        {
            string[] guids = FindAssetsOfType(type);
            Object[] assets = new Object[guids.Length];
            for (int i = 0; i < guids.Length; i++)
            {
                string guid = guids[i];
                string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                assets[i] = AssetDatabase.LoadAssetAtPath(assetPath, type);
            }

            return assets;
        }

        public static T LoadAssetOfTypeWithGUID<T>(string assetGUID)
            where T : Object
        {
            var assetPath = AssetDatabase.GUIDToAssetPath(assetGUID);
            return AssetDatabase.LoadAssetAtPath<T>(assetPath);
        }

        public static string AssetGUID(Object asset)
        {
            var assetPath = AssetDatabase.GetAssetPath(asset);
            return AssetDatabase.AssetPathToGUID(assetPath);
        }
    }
}
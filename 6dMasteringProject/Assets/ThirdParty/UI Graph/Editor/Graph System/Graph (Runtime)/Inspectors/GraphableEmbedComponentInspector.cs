﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;

namespace Pelican7.UIGraph.Editor
{
    [CustomEditor(typeof(GraphableEmbedComponent))]
    public class GraphableEmbedComponentInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty container = serializedObject.FindProperty("container");
            EditorGUILayout.PropertyField(container);

            SerializedProperty displayName = serializedObject.FindProperty("displayName");
            EditorGUILayout.PropertyField(displayName);

            serializedObject.ApplyModifiedProperties();
        }
    }
}
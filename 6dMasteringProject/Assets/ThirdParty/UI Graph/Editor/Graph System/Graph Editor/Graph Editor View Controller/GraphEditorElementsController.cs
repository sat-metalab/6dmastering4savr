// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class GraphEditorElementsController : ElementsEditorController
    {
        private const string TransitionListViewControllerAssetGuid = "774a3f87d1878f54bb95385006779995";
        private const string DarkStyleSheetAssetGuid = "da9a60d755e0d734aa03161525aa0b17";
        private const string LightStyleSheetAssetGuid = "466f982da5a453245b6af7024ce39a46";

        [ViewReference] protected VisualElement graphViewport;
        [ViewReference] protected VisualElement graphContainer;
        [ViewReference] protected ToolbarSpacer toolbarSpacer;
        [ViewReference] protected ToolbarMenu optionsMenu;

        private GraphResource graphResource;
        private GraphEditorWindow.PersistentWindowData persistentWindowData;
        private readonly List<NodeViewController> nodeViewControllers = new List<NodeViewController>();

        public void InitializeWithGraph(GraphResource graph, GraphEditorWindow.PersistentWindowData persistentWindowData)
        {
            graphResource = graph;
            this.persistentWindowData = persistentWindowData;
        }

        protected override void ViewDidLoad()
        {
            base.ViewDidLoad();
            toolbarSpacer.flex = true;

            ConfigureSkinSpecificStyleSheet();
            ConfigureGraphContainerTransform();
            ConfigureDragManipulator();
            ConfigureScaleManipulator();
            ConfigureDragAndDropManipulator();
            ConfigureContextualMenuManipulator();
            ConfigureToolbarButtons();
            SubscribeToGraphEvents();
            CreateNodeViewControllersForAllNodes();
            CreateEdgeWireDrawer();
            SubscribeToAssetPostProcessor();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (IsAsset)
            {
                return;
            }

            UnsubscribeFromGraphEvents();
        }

        private void ConfigureSkinSpecificStyleSheet()
        {
            string skinSpecificStyleSheetGuid = (EditorGUIUtility.isProSkin) ? DarkStyleSheetAssetGuid : LightStyleSheetAssetGuid;
            StyleSheet skinSpecificStyleSheet = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(skinSpecificStyleSheetGuid);
            View.styleSheets.Add(skinSpecificStyleSheet);
        }

        private void ConfigureGraphContainerTransform()
        {
            graphContainer.transform.position = persistentWindowData.graphContainerPosition;
            graphContainer.transform.scale = persistentWindowData.graphContainerScale;
        }

        private void ConfigureDragManipulator()
        {
            var dragTargetManipulator = new DragTargetManipulator(graphContainer, new ManipulatorActivationFilter[]
            {
                new ManipulatorActivationFilter()
                {
                    button = MouseButton.MiddleMouse
                },
                new ManipulatorActivationFilter()
                {
                    button = MouseButton.LeftMouse,
                    modifiers = EventModifiers.Alt
                }
            });
            dragTargetManipulator.OnDraggedTargetToPosition += OnDraggedGraphContainerToPosition;
            graphViewport.AddManipulator(dragTargetManipulator);
        }

        private void ConfigureScaleManipulator()
        {
            var scaleTargetManipulator = new ScaleTargetManipulator(graphContainer);
            scaleTargetManipulator.OnScaledTargetToScaleAndPosition += OnScaledGraphContainerToScaleAndPosition;
            graphViewport.AddManipulator(scaleTargetManipulator);
        }

        private void ConfigureDragAndDropManipulator()
        {
            var dragAndDropManipulator = new DragAndDropManipulator(new List<System.Type>()
            {
                typeof(IGraphable)
            });
            dragAndDropManipulator.OnObjectAccepted += OnDragAndDropObjectAccepted;
            graphViewport.AddManipulator(dragAndDropManipulator);
        }

        private void ConfigureContextualMenuManipulator()
        {
            graphViewport.AddManipulator(new ContextualMenuManipulator((evt) =>
            {
                evt.menu.AppendAction("Add View Controller", AddExistingViewControllerToGraph, (a) => DropdownMenuAction.Status.Normal);
            }));
        }

        private void ConfigureToolbarButtons()
        {
            optionsMenu.menu.AppendAction("Select Asset In Project", SelectAsset, (a) => DropdownMenuAction.Status.Normal);
            optionsMenu.menu.AppendAction("Show \u2215 Hide Asset Sub-Objects", ToggleGraphAssetSubObjectVisibility, (a) => DropdownMenuAction.Status.Normal);
            optionsMenu.menu.AppendSeparator();
            optionsMenu.menu.AppendAction("Add Graph To Current Scene", AddGraphToCurrentScene, (a) => DropdownMenuAction.Status.Normal);
        }

        private void SubscribeToGraphEvents()
        {
            graphResource.OnNodeAdded.AddListener(OnNodeAddedToGraph);
            graphResource.OnNodeRemoved.AddListener(OnNodeRemovedFromGraph);
            graphResource.OnEdgeAdded.AddListener(OnEdgeAddedToGraph);
            graphResource.OnEdgeRemoved.AddListener(OnEdgeRemovedFromGraph);
            graphResource.OnOriginChanged.AddListener(OnGraphOriginChanged);
        }

        private void UnsubscribeFromGraphEvents()
        {
            graphResource.OnNodeAdded.RemoveListener(OnNodeAddedToGraph);
            graphResource.OnNodeRemoved.RemoveListener(OnNodeRemovedFromGraph);
            graphResource.OnEdgeAdded.RemoveListener(OnEdgeAddedToGraph);
            graphResource.OnEdgeRemoved.RemoveListener(OnEdgeRemovedFromGraph);
            graphResource.OnOriginChanged.RemoveListener(OnGraphOriginChanged);
        }

        private void CreateNodeViewControllersForAllNodes()
        {
            foreach (NodeData node in graphResource.nodes)
            {
                CreateNodeViewControllerForNode(node);
            }
        }

        private void CreateNodeViewControllerForNode(NodeData node)
        {
            NodeViewController nodeViewController = NodeViewControllerFactory.CreateNodeViewControllerForNode(node);
            AddNodeViewControllerToGraphContainer(nodeViewController);
            nodeViewControllers.Add(nodeViewController);

            if (nodeViewController is ViewControllerNodeViewController viewControllerNode)
            {
                viewControllerNode.OnMadeFieldConnectionToPosition.AddListener(OnViewControllerNodeMadeFieldConnectionToPosition);
                viewControllerNode.OnMadeViewObjectEmbedConnectionToPosition.AddListener(OnViewControllerNodeMadeViewObjectEmbedConnectionToPosition);
                viewControllerNode.OnMadeViewObjectTransitionConnectionToPosition.AddListener(OnViewControllerNodeMadeViewObjectTransitionConnectionToPosition);
                viewControllerNode.OnMadeManualTransitionConnectionToPosition.AddListener(OnViewControllerNodeMadeManualTransitionConnectionToPosition);
            }
        }

        private void AddNodeViewControllerToGraphContainer(NodeViewController nodeViewController)
        {
            AddChild(nodeViewController);

            var childView = nodeViewController.View;
            graphContainer.Add(childView);
        }

        private void DestroyNodeViewControllerForNode(NodeData node)
        {
            var nodeViewController = NodeViewControllerWithNode(node, out int viewControllerIndex);
            if (nodeViewController != null)
            {
                nodeViewControllers.RemoveAt(viewControllerIndex);
                nodeViewController.Destroy();
            }
            else
            {
                Error.Log(string.Format("No view controller found for node '{0}'", node.guid));
            }
        }

        private void ReloadNodeViewControllerContentForNode(NodeData node)
        {
            var nodeViewController = NodeViewControllerWithNode(node, out _);
            nodeViewController?.ReloadData();
        }

        private NodeViewController NodeViewControllerWithNode(NodeData node, out int index)
        {
            NodeViewController nodeViewController = null;
            index = -1;

            for (int i = 0; i < nodeViewControllers.Count; i++)
            {
                var nodeVC = nodeViewControllers[i];
                if (nodeVC.Node.Equals(node))
                {
                    nodeViewController = nodeVC;
                    index = i;
                    break;
                }
            }

            return nodeViewController;
        }

        private NodeViewController NodeViewControllerAtPoint(Vector2 position, NodeViewController excludedViewController)
        {
            NodeViewController nodeViewController = null;

            List<NodeViewController> intersectedNodes = new List<NodeViewController>();
            foreach (NodeViewController nvc in nodeViewControllers)
            {
                if (nvc.Equals(excludedViewController) == false)
                {
                    VisualElement view = nvc.View;
                    Vector2 localPosition = view.WorldToLocal(position);
                    if (view.ContainsPoint(localPosition))
                    {
                        intersectedNodes.Add(nvc);
                    }
                }
            }

            if (intersectedNodes.Count > 0)
            {
                int highestIndex = -1;
                NodeViewController top = null;
                foreach (NodeViewController nvc in intersectedNodes)
                {
                    int index = graphContainer.IndexOf(nvc.View);
                    if (index > highestIndex)
                    {
                        top = nvc;
                        highestIndex = index;
                    }
                }

                nodeViewController = top;
            }

            return nodeViewController;
        }

        private void CreateEdgeWireDrawer()
        {
            EdgeWireDrawer edgeWireDrawer = new EdgeWireDrawer();
            edgeWireDrawer.onGUIHandler = () =>
            {
                foreach (EdgeData edge in graphResource.edges)
                {
                    NodeData sourceNode = edge.sourceNode;
                    NodeViewController sourceViewController = NodeViewControllerWithNode(sourceNode, out _);

                    NodeData destinationNode = edge.destinationNode;
                    NodeViewController destinationViewController = NodeViewControllerWithNode(destinationNode, out _);

                    Vector2 wireSourcePosition = sourceViewController.SourceWirePositionForEdge(edge);
                    wireSourcePosition = edgeWireDrawer.WorldToLocal(wireSourcePosition);
                    Vector2 wireDestinationPosition = destinationViewController.DestinationWirePositionForEdge(edge);
                    wireDestinationPosition = edgeWireDrawer.WorldToLocal(wireDestinationPosition);

                    Rect wireRect = Rect.MinMaxRect(wireSourcePosition.x, wireSourcePosition.y, wireDestinationPosition.x, wireDestinationPosition.y);
                    Rect viewportRect = graphViewport.contentRect;
                    if (graphViewport.contentRect.Overlaps(wireRect, true))
                    {
                        float containerScale = graphContainer.transform.scale.x;
                        edgeWireDrawer.DrawEdgeWire(edge, wireSourcePosition, wireDestinationPosition, containerScale);
                    }
                }
            };

            View.Add(edgeWireDrawer);
        }

        private void SubscribeToAssetPostProcessor()
        {
            GraphEditorElementsControllerAssetPostProcessor.ProcessImportedAssetsHandler = ProcessImportedAssets;
        }

        private void OnDraggedGraphContainerToPosition(Vector2 position, DragTargetManipulator.DragState state)
        {
            if (state == DragTargetManipulator.DragState.Ended)
            {
                PersistGraphContainerTransform();
            }
        }

        private void OnScaledGraphContainerToScaleAndPosition(Vector3 scale, Vector3 position)
        {
            PersistGraphContainerTransform();
        }

        private void PersistGraphContainerTransform()
        {
            persistentWindowData.graphContainerPosition = graphContainer.transform.position;
            persistentWindowData.graphContainerScale = graphContainer.transform.scale;
        }

        private void OnDragAndDropObjectAccepted(Object obj, Vector2 position)
        {
            string assetPath = AssetDatabase.GetAssetPath(obj);
            if (string.IsNullOrEmpty(assetPath) == false)
            {
                Vector2 localPosition = graphContainer.WorldToLocal(position);
                graphResource.TryAddNodeForAssetAtPath(assetPath, localPosition);
            }
        }

        private void AddExistingViewControllerToGraph(DropdownMenuAction action)
        {
            Vector2 localPosition = graphContainer.WorldToLocal(action.eventInfo.mousePosition);
            string fullPath = EditorUtility.OpenFilePanel("Select an existing view controller", "Assets", "asset");
            string assetPath = fullPath.Replace(Application.dataPath, "Assets");
            if (string.IsNullOrEmpty(assetPath) == false)
            {
                graphResource.TryAddNodeForAssetAtPath(assetPath, localPosition);
            }
        }

        private void AddGraphToCurrentScene(DropdownMenuAction action)
        {
            graphResource.AddToCurrentScene();
        }

        private void SelectAsset(DropdownMenuAction action)
        {
            EditorGUIUtility.PingObject(graphResource);
            EditorWindowUtility.ShowProjectBrowserEditorWindow();
            Selection.activeObject = graphResource;
        }

        private void ToggleGraphAssetSubObjectVisibility(DropdownMenuAction action)
        {
            graphResource.ToggleSubObjectVisibility();
        }

        private void OnNodeAddedToGraph(NodeData node)
        {
            CreateNodeViewControllerForNode(node);
        }

        private void OnNodeRemovedFromGraph(NodeData node)
        {
            DestroyNodeViewControllerForNode(node);
        }

        private void OnEdgeAddedToGraph(EdgeData edge)
        {
            ReloadNodeViewControllerContentForNode(edge.sourceNode);
            ReloadNodeViewControllerContentForNode(edge.destinationNode);
            if (edge.sourceNode.Equals(edge.targetNode) == false)
            {
                ReloadNodeViewControllerContentForNode(edge.targetNode);
            }
        }

        private void OnEdgeRemovedFromGraph(EdgeData edge)
        {
            ReloadNodeViewControllerContentForNode(edge.sourceNode);
        }

        private void OnGraphOriginChanged(NodeData origin, NodeData previousOrigin)
        {
            if (previousOrigin != null)
            {
                NodeViewController nodeViewController = NodeViewControllerWithNode(previousOrigin, out _);
                nodeViewController.SetOriginArrowVisible(false);
            }

            if (origin != null)
            {
                NodeViewController nodeViewController = NodeViewControllerWithNode(origin, out _);
                nodeViewController.SetOriginArrowVisible(true);
            }
        }

        private void OnViewControllerNodeMadeFieldConnectionToPosition(ViewControllerNodeViewController viewController, GraphableFieldPortData fieldPort, Vector2 mousePosition)
        {
            NodeViewController destinationViewController = NodeViewControllerAtPoint(mousePosition, viewController);
            if (destinationViewController != null)
            {
                string fieldPortName = fieldPort.fieldName;
                ViewControllerNodeData sourceNode = viewController.ViewControllerNode;
                NodeData destinationNode = destinationViewController.Node;
                if (graphResource.TryCreateFieldEdge(fieldPortName, sourceNode, destinationNode, out Error error) == false)
                {
                    error.LogWithPrefix(string.Format("Cannot create connection from field '{0}'.", fieldPortName));
                }
            }
        }

        private void OnViewControllerNodeMadeViewObjectEmbedConnectionToPosition(ViewControllerNodeViewController viewController, GraphableEmbedViewObjectPortData viewObjectPort, Vector2 mousePosition)
        {
            NodeViewController destinationViewController = NodeViewControllerAtPoint(mousePosition, viewController);
            if (destinationViewController != null)
            {
                string embedViewObjectGuid = viewObjectPort.embedViewObjectGuid;
                ViewControllerNodeData sourceNode = viewController.ViewControllerNode;
                NodeData destinationNode = destinationViewController.Node;
                if (graphResource.TryCreateEmbedViewObjectEdge(embedViewObjectGuid, sourceNode, destinationNode, out Error error) == false)
                {
                    error.LogWithPrefix(string.Format("Cannot create connection from embed view object '{0}'.", viewObjectPort.embedViewObjectDisplayName));
                }
            }
        }

        private void OnViewControllerNodeMadeViewObjectTransitionConnectionToPosition(ViewControllerNodeViewController viewController, GraphableTransitionViewObjectPortData viewObjectPort, GraphableTransitionViewObjectTriggerData viewObjectTrigger, Vector2 mousePosition)
        {
            NodeViewController destinationViewController = NodeViewControllerAtPoint(mousePosition, viewController);
            if (destinationViewController != null)
            {
                ViewControllerNodeData sourceNode = viewController.ViewControllerNode;
                NodeData destinationNode = destinationViewController.Node;
                if (graphResource.TryBuildTransitionProviderListBetweenNodes(sourceNode, destinationNode, out List<TransitionProvider> transitionProviders, out Error error))
                {
                    string destinationName = destinationNode.resource.Name;
                    PresentTransitionListWithSelectionHandler(transitionProviders, destinationName, mousePosition, (targetNode, transitionIdentifier) =>
                    {
                        string viewObjectGuid = viewObjectPort.transitionViewObjectGuid;
                        string triggerGuid = viewObjectTrigger.TriggerGuid;
                        ViewControllerNodeData targetVCNode = (ViewControllerNodeData)targetNode;
                        graphResource.CreateTransitionViewObjectEdge(viewObjectGuid, triggerGuid, sourceNode, destinationNode, targetVCNode, transitionIdentifier);
                    });
                }
                else
                {
                    error.LogWithPrefix("Cannot create transition connection from view object.");
                }
            }
        }

        private void OnViewControllerNodeMadeManualTransitionConnectionToPosition(ViewControllerNodeViewController viewController, Vector2 mousePosition)
        {
            NodeViewController destinationViewController = NodeViewControllerAtPoint(mousePosition, viewController);
            if (destinationViewController != null)
            {
                ViewControllerNodeData sourceNode = viewController.ViewControllerNode;
                NodeData destinationNode = destinationViewController.Node;
                if (graphResource.TryBuildTransitionProviderListBetweenNodes(sourceNode, destinationNode, out List<TransitionProvider> transitionProviders, out Error error))
                {
                    string destinationName = destinationNode.resource.Name;
                    PresentTransitionListWithSelectionHandler(transitionProviders, destinationName, mousePosition, (targetNode, transitionIdentifier) =>
                    {
                        ViewControllerNodeData targetVCNode = (ViewControllerNodeData)targetNode;
                        graphResource.CreateManualEdge(sourceNode, destinationNode, targetVCNode, transitionIdentifier);
                    });
                }
                else
                {
                    error.LogWithPrefix("Cannot create manual transition connection.");
                }
            }
        }

        private void PresentTransitionListWithSelectionHandler(List<TransitionProvider> transitionProviders, string destinationName, Vector2 mousePosition, UnityAction<NodeData, ViewControllerTransitionIdentifier> onSelection)
        {
            var transitionListViewControllerTemplate = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<TransitionListViewController>(TransitionListViewControllerAssetGuid);
            TransitionListViewController transitionListViewController = Instantiate(transitionListViewControllerTemplate);

            transitionListViewController.Initialize(transitionProviders, destinationName, mousePosition);
            transitionListViewController.OnTransitionSelected.AddListener((targetNode, transitionIdentifier) =>
            {
                onSelection.Invoke(targetNode, transitionIdentifier);
                transitionListViewController.Dismiss(false);
            });

            Present(transitionListViewController, false);
        }

        private void ProcessImportedAssets(string[] importedAssets)
        {
            foreach (string importedAssetPath in importedAssets)
            {
                foreach (NodeViewController nodeViewController in nodeViewControllers)
                {
                    NodeData node = nodeViewController.Node;
                    if (node.resource.TryGetValue(out IGraphable graphable))
                    {
                        if (graphable.RequiresRefreshForUpdatedAsset(importedAssetPath))
                        {
                            node.Refresh();
                            nodeViewController.ReloadData();
                        }
                    }
                }
            }
        }

        private class GraphEditorElementsControllerAssetPostProcessor : AssetPostprocessor
        {
            public static System.Action<string[]> ProcessImportedAssetsHandler;

            private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
            {
                ProcessImportedAssetsHandler?.Invoke(importedAssets);
            }
        }
    }
}
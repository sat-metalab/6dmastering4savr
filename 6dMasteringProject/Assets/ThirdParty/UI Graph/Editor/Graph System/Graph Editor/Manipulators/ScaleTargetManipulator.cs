﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class ScaleTargetManipulator : MouseManipulator
    {
        private const float scaleSensitivity = 0.05f;
        private const float MinimumScaleComponent = 0.1f;
        private static readonly Vector3 MinimumScale = new Vector3(MinimumScaleComponent, MinimumScaleComponent, MinimumScaleComponent);
        private const float MaximumScaleComponent = 1f;
        private static readonly Vector3 MaximumScale = new Vector3(MaximumScaleComponent, MaximumScaleComponent, MaximumScaleComponent);

        private readonly VisualElement scaleTarget;

        public ScaleTargetManipulator(VisualElement scaleTarget)
        {
            this.scaleTarget = scaleTarget;
        }

        public Action<Vector3, Vector3> OnScaledTargetToScaleAndPosition;

        protected override void RegisterCallbacksOnTarget()
        {
            target.RegisterCallback<WheelEvent>(OnWheelEvent);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback<WheelEvent>(OnWheelEvent);
        }

        private void OnWheelEvent(WheelEvent evt)
        {
            ScaleTarget(evt);
        }

        private void ScaleTarget(WheelEvent evt)
        {
            Vector2 mousePosition = evt.mousePosition;
            Vector2 localMousePositionInTargetBeforeScale = scaleTarget.WorldToLocal(mousePosition);

            float scaleIncrement = -evt.delta.y * scaleSensitivity;
            Vector3 scale = scaleTarget.transform.scale + new Vector3(scaleIncrement, scaleIncrement, scaleIncrement);
            scale = Vector3.Max(Vector3.Min(scale, MaximumScale), MinimumScale);
            scaleTarget.transform.scale = scale;

            Vector2 localMousePositionInTargetAfterScale = scaleTarget.WorldToLocal(mousePosition);
            Vector3 adjustment = Vector3.Scale(localMousePositionInTargetAfterScale - localMousePositionInTargetBeforeScale, scale);
            Vector3 position = scaleTarget.transform.position + adjustment;
            scaleTarget.transform.position = position;

            OnScaledTargetToScaleAndPosition(scale, position);
        }
    }
}
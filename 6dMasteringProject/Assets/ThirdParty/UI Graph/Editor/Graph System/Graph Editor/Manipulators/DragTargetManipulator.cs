﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class DragTargetManipulator : MouseManipulator
    {
        private readonly VisualElement dragTarget;
        private Vector2 startPosition;
        private bool active;
        private Vector2 dragTargetStartPosition;

        public DragTargetManipulator(VisualElement dragTarget, ManipulatorActivationFilter[] activationFilters)
        {
            this.dragTarget = dragTarget;
            foreach (ManipulatorActivationFilter activationFilter in activationFilters)
            {
                activators.Add(activationFilter);
            }
            active = false;
        }

        public System.Action<Vector2, DragState> OnDraggedTargetToPosition;

        public enum DragState
        {
            Began,
            Moved,
            Ended
        }

        protected override void RegisterCallbacksOnTarget()
        {
            target.RegisterCallback<MouseDownEvent>(OnMouseDown);
            target.RegisterCallback<MouseMoveEvent>(OnMouseMove);
            target.RegisterCallback<MouseUpEvent>(OnMouseUp);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback<MouseDownEvent>(OnMouseDown);
            target.UnregisterCallback<MouseMoveEvent>(OnMouseMove);
            target.UnregisterCallback<MouseUpEvent>(OnMouseUp);
        }

        protected void OnMouseDown(MouseDownEvent e)
        {
            if (active)
            {
                active = false;
                target.ReleaseMouse();
                e.StopImmediatePropagation();
                return;
            }

            if (CanStartManipulation(e))
            {
                startPosition = e.mousePosition;
                dragTargetStartPosition = dragTarget.transform.position;

                active = true;
                target.CaptureMouse();
                e.StopPropagation();

                DragTarget(Vector2.zero, DragState.Began);
            }
        }

        protected void OnMouseMove(MouseMoveEvent e)
        {
            if ((active == false) || (target.HasMouseCapture() == false))
            {
                return;
            }

            e.StopPropagation();

            Vector2 movement = e.mousePosition - startPosition;
            DragTarget(movement, DragState.Moved);
        }

        protected void OnMouseUp(MouseUpEvent e)
        {
            if ((active == false) || (target.HasMouseCapture() == false) || (CanStopManipulation(e) == false))
            {
                return;
            }

            active = false;
            target.ReleaseMouse();
            e.StopPropagation();

            Vector2 movement = e.mousePosition - startPosition;
            DragTarget(movement, DragState.Ended);
        }

        private void DragTarget(Vector2 movement, DragState state)
        {
            Vector2 position = dragTargetStartPosition + movement;
            dragTarget.transform.position = position;

            OnDraggedTargetToPosition?.Invoke(position, state);
        }
    }
}
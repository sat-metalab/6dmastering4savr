﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class DragAndDropManipulator : MouseManipulator
    {
        private readonly List<System.Type> acceptedTypes;

        public DragAndDropManipulator(List<System.Type> acceptedTypes)
        {
            this.acceptedTypes = acceptedTypes;
        }

        public System.Action<Object, Vector2> OnObjectAccepted;

        protected override void RegisterCallbacksOnTarget()
        {
            target.RegisterCallback<DragUpdatedEvent>(OnDragUpdated);
            target.RegisterCallback<DragPerformEvent>(OnDragPerform);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback<DragUpdatedEvent>(OnDragUpdated);
            target.UnregisterCallback<DragPerformEvent>(OnDragPerform);
        }

        private void OnDragUpdated(DragUpdatedEvent e)
        {
            bool draggedObjectsContainAcceptedType = ObjectsContainAcceptedType(DragAndDrop.objectReferences);
            DragAndDrop.visualMode = (draggedObjectsContainAcceptedType) ? DragAndDropVisualMode.Copy : DragAndDropVisualMode.Rejected;
        }

        private void OnDragPerform(DragPerformEvent e)
        {
            foreach (Object obj in DragAndDrop.objectReferences)
            {
                if (ObjectIsAcceptedType(obj))
                {
                    OnObjectAccepted?.Invoke(obj, e.mousePosition);
                    break;
                }
            }
        }

        private bool ObjectsContainAcceptedType(Object[] objects)
        {
            bool objectsContainsAcceptedType = false;
            foreach (Object obj in objects)
            {
                if (ObjectIsAcceptedType(obj))
                {
                    objectsContainsAcceptedType = true;
                    break;
                }
            }

            return objectsContainsAcceptedType;
        }

        private bool ObjectIsAcceptedType(Object obj)
        {
            System.Type objectType = obj.GetType();
            return acceptedTypes.Any((acceptedType) => acceptedType.IsAssignableFrom(objectType));
        }
    }
}
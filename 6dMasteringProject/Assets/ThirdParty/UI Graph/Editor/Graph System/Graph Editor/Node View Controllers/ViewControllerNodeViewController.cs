// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class ViewControllerNodeViewController : NodeViewController
    {
        [ViewReference] protected Label titleLabel;
        [ViewReference] protected ToolbarButton editAssetButton;
        [ViewReference] protected ToolbarButton editScriptButton;
        [ViewReference] protected ToolbarButton editViewButton;
        [ViewReference] protected VisualElement fieldPortsSection;
        [ViewReference] protected Foldout fieldPortsVisibilityFoldout;
        [ViewReference] protected VisualElement fieldPortsContainer;
        [ViewReference] protected VisualElement viewObjectPortsSection;
        [ViewReference] protected Foldout viewObjectPortsVisibilityFoldout;
        [ViewReference] protected VisualElement viewObjectPortsContainer;
        [ViewReference] protected VisualElement embedViewObjectPortsContainer;
        [ViewReference] protected VisualElement transitionViewObjectPortsContainer;
        [ViewReference] protected VisualElement manualTransitionsSection;
        [ViewReference] protected Foldout manualTransitionsVisibilityFoldout;
        [ViewReference] protected NodeConnectorView manualTransitionNodeConnectorView;
        [ViewReference] protected VisualElement manualTransitionsContainer;
        [ViewReference] protected Label manualTransitionsEmptyLabel;

        private List<PropertyField> fieldPortPropertyFields;
        private List<PropertyField> embedViewObjectPortPropertyFields;
        private List<PropertyField> transitionViewObjectPortPropertyFields;
        private List<PropertyField> manualTransitionPropertyFields;

        public MadeFieldConnectionEvent OnMadeFieldConnectionToPosition = new MadeFieldConnectionEvent();
        public MadeViewObjectEmbedConnectionEvent OnMadeViewObjectEmbedConnectionToPosition = new MadeViewObjectEmbedConnectionEvent();
        public MadeViewObjectTransitionConnectionEvent OnMadeViewObjectTransitionConnectionToPosition = new MadeViewObjectTransitionConnectionEvent();
        public MadeManualTransitionConnectionEvent OnMadeManualTransitionConnectionToPosition = new MadeManualTransitionConnectionEvent();

        public ViewControllerNodeData ViewControllerNode
        {
            get
            {
                return (ViewControllerNodeData)node;
            }
        }

        public override void ReloadData()
        {
            SerializedObject serializedNode = new SerializedObject(node);
            ReloadFieldPortsList(serializedNode);
            ReloadViewObjectPortsList(serializedNode);
            ReloadManualTransitionsList(serializedNode);

            View.Bind(serializedNode);
            BindToFieldPortConnectorViews();
            BindToViewObjectConnectorViews();
        }

        public override Vector2 SourceWirePositionForEdge(EdgeData edge)
        {
            if (node.HasValidResource() == false)
            {
                return SourceWirePositionForEdgeOnElement(edge, View);
            }

            List<GraphableFieldPortData> fieldPorts = ViewControllerNode.fieldPorts;
            for (int i = 0; i < fieldPorts.Count; i++)
            {
                GraphableFieldPortData fieldPort = fieldPorts[i];
                if (fieldPort.edges.Contains(edge))
                {
                    PropertyField propertyField = fieldPortPropertyFields[i];
                    VisualElement targetElement = (ViewControllerNode.fieldPortsVisible) ? propertyField : fieldPortsSection;
                    return SourceWirePositionForEdgeOnElement(edge, targetElement);
                }
            }

            List<GraphableEmbedViewObjectPortData> embedViewObjectPorts = ViewControllerNode.embedViewObjectPorts;
            for (int i = 0; i < embedViewObjectPorts.Count; i++)
            {
                GraphableEmbedViewObjectPortData embedViewObjectPort = embedViewObjectPorts[i];
                if (edge.Equals(embedViewObjectPort.edge))
                {
                    PropertyField propertyField = embedViewObjectPortPropertyFields[i];
                    VisualElement targetElement = (ViewControllerNode.viewObjectPortsVisible) ? propertyField : viewObjectPortsSection;
                    return SourceWirePositionForEdgeOnElement(edge, targetElement);
                }
            }

            List<GraphableTransitionViewObjectPortData> transitionViewObjectPorts = ViewControllerNode.transitionViewObjectPorts;
            for (int i = 0; i < transitionViewObjectPorts.Count; i++)
            {
                GraphableTransitionViewObjectPortData transitionViewObjectPort = transitionViewObjectPorts[i];
                PropertyField viewObjectPortPropertyField = transitionViewObjectPortPropertyFields[i];

                List<GraphableTransitionViewObjectTriggerData> triggers = transitionViewObjectPort.triggers;
                List<VisualElement> triggerPropertyFields = viewObjectPortPropertyField.Query(className: "view-object-trigger").ToList();
                for (int j = 0; j < triggers.Count; j++)
                {
                    GraphableTransitionViewObjectTriggerData trigger = triggers[j];
                    if (edge.Equals(trigger.edge))
                    {
                        VisualElement triggerPropertyField = triggerPropertyFields[j];
                        VisualElement targetElement = (ViewControllerNode.viewObjectPortsVisible) ? triggerPropertyField : viewObjectPortsSection;
                        return SourceWirePositionForEdgeOnElement(edge, targetElement);
                    }
                }
            }

            List<ManualTransitionEdgeData> manualEdges = ViewControllerNode.manualEdges;
            for (int i = 0; i < manualEdges.Count; i++)
            {
                ManualTransitionEdgeData manualEdge = manualEdges[i];
                if (edge.Equals(manualEdge))
                {
                    PropertyField propertyField = manualTransitionPropertyFields[i];
                    VisualElement targetElement = (ViewControllerNode.manualTransitionsVisible) ? propertyField : manualTransitionsSection;
                    return SourceWirePositionForEdgeOnElement(edge, targetElement);
                }
            }

            return Vector2.zero;
        }

        public override Vector2 DestinationWirePositionForEdge(EdgeData edge)
        {
            Rect worldBound = View.worldBound;
            if (edge.type == EdgeData.EdgeType.Dismissal)
            {
                return new Vector2(worldBound.xMax, worldBound.center.y);
            }
            else
            {
                return new Vector2(worldBound.xMin, worldBound.center.y);
            }
        }

        protected override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (node.HasValidResource() == false)
            {
                return;
            }

            GraphableReference resource = node.resource;
            titleLabel.text = resource.Name;

            BindToolbarButtons();
            ConfigureFoldoutButtons();
            ConfigureManualTransitionNodeConnectorView();
            ConfigureContextualMenuManipulator();

            ReloadData();
        }

        protected override void RefreshPorts(DropdownMenuAction action)
        {
            ViewControllerNode.Refresh();
            ReloadData();
        }

        private void BindToolbarButtons()
        {
            editAssetButton.clickable.clicked += EditAsset;
            editScriptButton.clickable.clicked += EditScript;
            editViewButton.clickable.clicked += EditView;
        }

        private void ConfigureFoldoutButtons()
        {
            ConfigureVisibilityFoldout("fieldPortsVisible", fieldPortsVisibilityFoldout, fieldPortsContainer);
            ConfigureVisibilityFoldout("viewObjectPortsVisible", viewObjectPortsVisibilityFoldout, viewObjectPortsContainer);
            ConfigureVisibilityFoldout("manualTransitionsVisible", manualTransitionsVisibilityFoldout, manualTransitionsContainer);
        }

        private void EditAsset()
        {
            ScriptableObject asset = ViewControllerNode.resource.RawValue;
            EditorGUIUtility.PingObject(asset);
            Selection.activeObject = asset;
            EditorWindowUtility.ShowInspectorEditorWindow();
            EditorWindowUtility.ShowProjectBrowserEditorWindow();
        }

        private void EditScript()
        {
            ScriptableObject asset = ViewControllerNode.resource.RawValue;
            MonoScript script = MonoScript.FromScriptableObject(asset);
            AssetDatabase.OpenAsset(script);
        }

        private void EditView()
        {
            ScriptableObject asset = ViewControllerNode.resource.RawValue;
            IViewController viewController = asset as IViewController;
            IViewResource viewResource = viewController.ViewResource;
            viewResource.OpenView();
        }

        private void ConfigureManualTransitionNodeConnectorView()
        {
            manualTransitionNodeConnectorView.AllowNewConnections = true;
            manualTransitionNodeConnectorView.OnMakingConnection.AddListener((ncv, connectionState, mousePosition) =>
            {
                OnManualTransitionConnectorViewMakingConnection(connectionState, mousePosition);
            });
        }

        private void ConfigureContextualMenuManipulator()
        {
            View.AddManipulator(new ContextualMenuManipulator((evt) =>
            {
                evt.menu.AppendSeparator();
                evt.menu.AppendAction("Make All View Elements Graphable", MakeAllViewElementsGraphable, (a) => DropdownMenuAction.Status.Normal);
            }));
        }

        private void MakeAllViewElementsGraphable(DropdownMenuAction action)
        {
            ScriptableObject asset = ViewControllerNode.resource.RawValue;
            IViewController viewController = asset as IViewController;
            IViewResource viewResource = viewController.ViewResource;
            viewResource.AddGraphablesToViewElements();
        }

        private void ReloadFieldPortsList(SerializedObject serializedNode)
        {
            DestroyPropertyFieldsInCollectionIfNecessary(fieldPortPropertyFields);
            fieldPortPropertyFields = CreatePropertyFieldsForNodePropertyArrayInContainer(serializedNode, "fieldPorts", fieldPortsContainer);

            bool visible = (fieldPortPropertyFields.Count != 0);
            SetContainerVisibility(fieldPortsSection, visible);
        }

        private void ReloadViewObjectPortsList(SerializedObject serializedNode)
        {
            DestroyPropertyFieldsInCollectionIfNecessary(embedViewObjectPortPropertyFields);
            embedViewObjectPortPropertyFields = CreatePropertyFieldsForNodePropertyArrayInContainer(serializedNode, "embedViewObjectPorts", embedViewObjectPortsContainer);

            DestroyPropertyFieldsInCollectionIfNecessary(transitionViewObjectPortPropertyFields);
            transitionViewObjectPortPropertyFields = CreatePropertyFieldsForNodePropertyArrayInContainer(serializedNode, "transitionViewObjectPorts", transitionViewObjectPortsContainer);

            bool visible = ((embedViewObjectPortPropertyFields.Count != 0) || (transitionViewObjectPortPropertyFields.Count != 0));
            SetContainerVisibility(viewObjectPortsSection, visible);
        }

        private void ReloadManualTransitionsList(SerializedObject serializedNode)
        {
            DestroyPropertyFieldsInCollectionIfNecessary(manualTransitionPropertyFields);
            manualTransitionPropertyFields = CreatePropertyFieldsForNodePropertyArrayInContainer(serializedNode, "manualEdges", manualTransitionsContainer);
            manualTransitionsEmptyLabel.EnableInClassList("hidden", (manualTransitionPropertyFields.Count != 0));
        }

        private void BindToFieldPortConnectorViews()
        {
            UQueryBuilder<NodeConnectorView> query = fieldPortsContainer.Query<NodeConnectorView>();

            int i = 0;
            query.ForEach((nodeConnectorView) =>
            {
                int index = i;
                nodeConnectorView.OnMakingConnection.AddListener((ncv, connectionState, mousePosition) =>
                {
                    OnFieldPortConnectorViewMakingConnection(connectionState, mousePosition, index);
                });

                i++;
            });
        }

        private void BindToViewObjectConnectorViews()
        {
            BindToEmbedViewObjectConnectorViews();
            BindToTransitionViewObjectTriggerConnectorViews();
        }

        private void BindToEmbedViewObjectConnectorViews()
        {
            BindToNodeConnectorViewsInContainer(embedViewObjectPortsContainer, (nodeConnectorView, index) =>
            {
                nodeConnectorView.OnMakingConnection.AddListener((ncv, connectionState, mousePosition) =>
                {
                    OnEmbedViewObjectPortConnectorViewMakingConnection(connectionState, mousePosition, index);
                });
            });
        }

        private void BindToTransitionViewObjectTriggerConnectorViews()
        {
            BindToNodeConnectorViewsInContainer(transitionViewObjectPortsContainer, (nodeConnectorView, index) =>
            {
                var tuple = TransitionViewObjectPortAndTriggerForConnectorViewIndex(index);
                GraphableTransitionViewObjectPortData viewObjectPort = tuple.Item1;
                GraphableTransitionViewObjectTriggerData viewObjectTrigger = tuple.Item2;
                nodeConnectorView.OnMakingConnection.AddListener((ncv, connectionState, mousePosition) =>
                {
                    OnTransitionViewObjectPortConnectorViewMakingConnection(connectionState, mousePosition, viewObjectPort, viewObjectTrigger);
                });
            });
        }

        private void BindToNodeConnectorViewsInContainer(VisualElement container, System.Action<NodeConnectorView, int> bindingAction)
        {
            UQueryBuilder<NodeConnectorView> query = container.Query<NodeConnectorView>();

            int i = 0;
            query.ForEach((nodeConnectorView) =>
            {
                bindingAction(nodeConnectorView, i);
                i++;
            });
        }

        private System.Tuple<GraphableTransitionViewObjectPortData, GraphableTransitionViewObjectTriggerData> TransitionViewObjectPortAndTriggerForConnectorViewIndex(int index)
        {
            System.Tuple<GraphableTransitionViewObjectPortData, GraphableTransitionViewObjectTriggerData> portAndTrigger = null;

            int i = 0;
            foreach (GraphableTransitionViewObjectPortData transitionViewObjectPort in ViewControllerNode.transitionViewObjectPorts)
            {
                foreach (GraphableTransitionViewObjectTriggerData trigger in transitionViewObjectPort.triggers)
                {
                    if (i == index)
                    {
                        portAndTrigger = System.Tuple.Create(transitionViewObjectPort, trigger);
                    }

                    i++;
                }
            }

            return portAndTrigger;
        }

        private List<PropertyField> CreatePropertyFieldsForNodePropertyArrayInContainer(SerializedObject serializedNode, string property, VisualElement container)
        {
            SerializedProperty arrayProperty = serializedNode.FindProperty(property);
            int arraySize = arrayProperty.arraySize;

            List<PropertyField> fields = new List<PropertyField>(arraySize);
            for (int i = 0; i < arrayProperty.arraySize; i++)
            {
                SerializedProperty itemProperty = arrayProperty.GetArrayElementAtIndex(i);
                PropertyField propertyField = new PropertyField(itemProperty);
                container.Add(propertyField);
                fields.Add(propertyField);
            }

            return fields;
        }

        private void DestroyPropertyFieldsInCollectionIfNecessary(List<PropertyField> propertyFields)
        {
            if ((propertyFields != null) && (propertyFields.Count > 0))
            {
                foreach (PropertyField propertyField in propertyFields)
                {
                    propertyField.RemoveFromHierarchy();
                }

                propertyFields.Clear();
            }
        }

        private void OnFieldPortConnectorViewMakingConnection(NodeConnectorView.MakeConnectionState connectionState, Vector2 mousePosition, int index)
        {
            if (connectionState == NodeConnectorView.MakeConnectionState.Ended)
            {
                GraphableFieldPortData fieldPort = ViewControllerNode.fieldPorts[index];
                OnMadeFieldConnectionToPosition.Invoke(this, fieldPort, mousePosition);
            }
        }

        private void OnEmbedViewObjectPortConnectorViewMakingConnection(NodeConnectorView.MakeConnectionState connectionState, Vector2 mousePosition, int index)
        {
            if (connectionState == NodeConnectorView.MakeConnectionState.Ended)
            {
                GraphableEmbedViewObjectPortData viewObjectPort = ViewControllerNode.embedViewObjectPorts[index];
                OnMadeViewObjectEmbedConnectionToPosition.Invoke(this, viewObjectPort, mousePosition);
            }
        }

        private void OnTransitionViewObjectPortConnectorViewMakingConnection(NodeConnectorView.MakeConnectionState connectionState, Vector2 mousePosition, GraphableTransitionViewObjectPortData viewObjectPort, GraphableTransitionViewObjectTriggerData viewObjectTrigger)
        {
            if (connectionState == NodeConnectorView.MakeConnectionState.Ended)
            {
                OnMadeViewObjectTransitionConnectionToPosition.Invoke(this, viewObjectPort, viewObjectTrigger, mousePosition);
            }
        }

        private void OnManualTransitionConnectorViewMakingConnection(NodeConnectorView.MakeConnectionState connectionState, Vector2 mousePosition)
        {
            if (connectionState == NodeConnectorView.MakeConnectionState.Ended)
            {
                OnMadeManualTransitionConnectionToPosition.Invoke(this, mousePosition);
            }
        }

        private Vector2 SourceWirePositionForEdgeOnElement(EdgeData edge, VisualElement element)
        {
            Rect propertyFieldRect = element.worldBound;
            float wirePositionX = (edge.type == EdgeData.EdgeType.Dismissal) ? propertyFieldRect.xMin : propertyFieldRect.xMax;
            Vector2 wirePosition = new Vector2(wirePositionX, propertyFieldRect.center.y);
            return wirePosition;
        }

        public class MadeFieldConnectionEvent : UnityEvent<ViewControllerNodeViewController, GraphableFieldPortData, Vector2> { }
        public class MadeViewObjectEmbedConnectionEvent : UnityEvent<ViewControllerNodeViewController, GraphableEmbedViewObjectPortData, Vector2> { }
        public class MadeViewObjectTransitionConnectionEvent : UnityEvent<ViewControllerNodeViewController, GraphableTransitionViewObjectPortData, GraphableTransitionViewObjectTriggerData, Vector2> { }
        public class MadeManualTransitionConnectionEvent : UnityEvent<ViewControllerNodeViewController, Vector2> { }
    }
}
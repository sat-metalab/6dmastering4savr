// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class TransitionListViewController : ElementsEditorController
    {
        private const float PopoverPadding = 8f;
        private const string DarkStyleSheetAssetGuid = "088277da807eadf41b26538364cede52";
        private const string LightStyleSheetAssetGuid = "c6e99ca4157c75c42bd0a169de198627";

        [ViewReference] protected VisualElement background;
        [ViewReference] protected Label titleLabel;
        [ViewReference] protected VisualElement listContainer;
        [ViewReference] protected ScrollView scrollView;

        private List<TransitionProvider> transitionProviders;
        private string destinationName;
        private Vector2 worldPopoverPosition;
        private Vector2 localPopoverPosition;

        public TransitionListSelectionEvent OnTransitionSelected = new TransitionListSelectionEvent();

        public void Initialize(List<TransitionProvider> transitionProviders, string destinationName, Vector2 popoverPosition)
        {
            this.transitionProviders = transitionProviders;
            this.destinationName = destinationName;
            worldPopoverPosition = popoverPosition;
        }

        protected override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ConfigureSkinSpecificStyleSheet();
            ReloadData();
            ConfigureBackgroundDismissHandler();
            View.RegisterCallback<GeometryChangedEvent>(OnGeometryChanged);
        }

        protected override void ViewWillAppear()
        {
            base.ViewWillAppear();
            localPopoverPosition = View.WorldToLocal(worldPopoverPosition);
        }

        private void ConfigureSkinSpecificStyleSheet()
        {
            string skinSpecificStyleSheetGuid = (EditorGUIUtility.isProSkin) ? DarkStyleSheetAssetGuid : LightStyleSheetAssetGuid;
            StyleSheet skinSpecificStyleSheet = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(skinSpecificStyleSheetGuid);
            View.styleSheets.Add(skinSpecificStyleSheet);
        }

        private void ReloadData()
        {
            foreach (TransitionProvider transitionProvider in transitionProviders)
            {
                Label providerTitleLabel = new Label(transitionProvider.Name);
                providerTitleLabel.AddToClassList("list-section-header");
                scrollView.Add(providerTitleLabel);

                foreach (ViewControllerTransitionIdentifier transitionIdentifier in transitionProvider.transitionIdentifiers)
                {
                    var itemView = new TransitionListItemView();
                    itemView.transitionIdentifierLabel.text = transitionIdentifier.DisplayName;
                    itemView.destinationNameLabel.text = destinationName;
                    itemView.OnSelected += () =>
                    {
                        OnTransitionSelected.Invoke(transitionProvider.targetNode, transitionIdentifier);
                    };
                    scrollView.Add(itemView);
                }
            }
        }

        private void ConfigureBackgroundDismissHandler()
        {
            background.RegisterCallback<MouseDownEvent>((e) =>
            {
                Dismiss(false);
            });
        }

        private void OnGeometryChanged(GeometryChangedEvent evt)
        {
            if (evt.oldRect.size == evt.newRect.size)
            {
                return;
            }

            PositionListContainer();
        }

        private void PositionListContainer()
        {
            Rect rect = listContainer.contentRect;
            Vector2 extents = new Vector2(rect.width * 0.5f, rect.height * 0.5f);
            Vector2 position = new Vector2(localPopoverPosition.x - extents.x, localPopoverPosition.y - extents.y);

            Vector2 viewSize = View.contentRect.size;
            position.x = Mathf.Clamp(position.x, PopoverPadding, viewSize.x - rect.width - PopoverPadding);
            position.y = Mathf.Clamp(position.y, PopoverPadding, viewSize.y - rect.height - PopoverPadding);

            listContainer.transform.position = position;
        }

        public class TransitionListSelectionEvent : UnityEvent<NodeData, ViewControllerTransitionIdentifier> { }
    }
}
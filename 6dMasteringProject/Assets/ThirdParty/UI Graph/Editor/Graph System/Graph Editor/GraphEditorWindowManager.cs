﻿using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public class GraphEditorWindowManager
    {
        [OnOpenAsset]
        private static bool OpenAssetHandler(int instanceID, int line)
        {
            Object objectToOpen = EditorUtility.InstanceIDToObject(instanceID);
            if (objectToOpen is GraphResource graph)
            {
                OpenGraphInEditor(graph);
                return true;
            }

            return false;
        }

        private static void OpenGraphInEditor(GraphResource graph)
        {
            GraphEditorWindow graphEditorWindow = null;

            var existingGraphEditorWindows = Resources.FindObjectsOfTypeAll<GraphEditorWindow>();
            foreach (var existingGraphEditorWindow in existingGraphEditorWindows)
            {
                if (graph.Equals(existingGraphEditorWindow.Graph))
                {
                    graphEditorWindow = existingGraphEditorWindow;
                    break;
                }
            }

            if (graphEditorWindow == null)
            {
                graphEditorWindow = CreateGraphEditorWindowForGraph(graph);
            }

            graphEditorWindow.Show();
            graphEditorWindow.Focus();
        }

        private static GraphEditorWindow CreateGraphEditorWindowForGraph(GraphResource graph)
        {
            var graphEditorWindow = ScriptableObject.CreateInstance<GraphEditorWindow>();
            graphEditorWindow.Initialize(graph);
            return graphEditorWindow;
        }
    }
}
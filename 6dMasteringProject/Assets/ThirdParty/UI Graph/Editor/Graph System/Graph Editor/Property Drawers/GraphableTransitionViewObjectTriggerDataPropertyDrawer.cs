// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(GraphableTransitionViewObjectTriggerData))]
    public class GraphableTransitionViewObjectTriggerDataPropertyDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualElement container = new VisualElement();
            container.AddToClassList("property-panel");
            container.AddToClassList("view-object-trigger");

            VisualElement header = new VisualElement()
            {
                style =
                {
                    flexDirection = FlexDirection.Row
                }
            };
            header.AddToClassList("header");
            container.Add(header);

            SerializedProperty trigger = property.FindPropertyRelative("trigger");
            SerializedProperty displayName = trigger.FindPropertyRelative("displayName");
            Label displayLabel = new Label(displayName.stringValue)
            {
                style =
                {
                    flexGrow = 1
                }
            };
            header.Add(displayLabel);

            SerializedProperty edgeProperty = property.FindPropertyRelative("edge");
            TransitionViewObjectEdgeData edge = (TransitionViewObjectEdgeData)edgeProperty.objectReferenceValue;

            NodeConnectorView nodeConnectorView = new NodeConnectorView
            {
                AllowNewConnections = (edge == null)
            };
            header.Add(nodeConnectorView);

            PropertyField edgePropertyField = new PropertyField(edgeProperty);
            container.Add(edgePropertyField);

            return container;
        }
    }
}
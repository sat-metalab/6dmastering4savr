// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(GraphableTransitionViewObjectPortData))]
    public class GraphableTransitionViewObjectPortDataPropertyDrawer : PropertyDrawer
    {
        private const string UxmlGuid = "490d0784001223741b63608149f0a0a6";
        private const string UssGuid = "6df81072ac0f73245b2e361e556cb538";
        private const string DarkUssGuid = "297904a3f0862d443bab97216cb05cf1";
        private const string LightUssGuid = "fd8dcc8fee8b8e84dbef4c30f675fd9c";

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualTreeAsset uxml = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<VisualTreeAsset>(UxmlGuid);
            TemplateContainer container = uxml.CloneTree();
            StyleSheet uss = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(UssGuid);
            container.styleSheets.Add(uss);

            AddSkinSpecificStyleSheetToContainer(container);

            Label titleLabel = (Label)container.Q("titleLabel");
            SerializedProperty displayNameProperty = property.FindPropertyRelative("transitionViewObjectDisplayName");
            titleLabel.text = displayNameProperty.stringValue;

            VisualElement content = container.Q("content");
            SerializedProperty triggersProperty = property.FindPropertyRelative("triggers");
            for (int i = 0; i < triggersProperty.arraySize; i++)
            {
                SerializedProperty triggerProperty = triggersProperty.GetArrayElementAtIndex(i);
                var propertyField = new PropertyField(triggerProperty);
                content.Add(propertyField);
            }

            return container;
        }

        private void AddSkinSpecificStyleSheetToContainer(VisualElement container)
        {
            string skinSpecificStyleSheetGuid = (EditorGUIUtility.isProSkin) ? DarkUssGuid : LightUssGuid;
            StyleSheet skinSpecificStyleSheet = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(skinSpecificStyleSheetGuid);
            container.styleSheets.Add(skinSpecificStyleSheet);
        }
    }
}
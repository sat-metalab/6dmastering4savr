// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using Pelican7.UIGraph;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(EmbedViewObjectEdgeData))]
    public class EmbedViewObjectEdgeDataPropertyDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualElement container = new VisualElement();
            container.AddToClassList("embed-view-object-port-edge");

            EmbedViewObjectEdgeData edge = property.objectReferenceValue as EmbedViewObjectEdgeData;
            if (edge != null)
            {
                NodeData destinationNode = edge.destinationNode;
                string destinationName = destinationNode.resource.Name;
                container.Add(new Label(destinationName));

                SerializedObject nodeSerializedObject = property.serializedObject;
                NodeData node = (NodeData)nodeSerializedObject.targetObject;
                Button deleteButton = new Button(() =>
                {
                    GraphResource graph = node.graph;
                    graph.DeleteEmbedViewObjectEdge(edge);
                })
                {
                    text = "Delete"
                };
                container.Add(deleteButton);
            }
            else
            {
                Label emptyLabel = new Label("Not connected.");
                container.Add(emptyLabel);
            }

            return container;
        }
    }
}
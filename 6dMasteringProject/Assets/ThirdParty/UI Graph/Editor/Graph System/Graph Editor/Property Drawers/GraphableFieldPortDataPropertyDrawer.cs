// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(GraphableFieldPortData))]
    public class GraphableFieldPortDataPropertyDrawer : PropertyDrawer
    {
        private const string UxmlGuid = "c3b96607ade7e3744ac05d1fe4af26d1";
        private const string UssGuid = "992a21537fd3b1645b071c03f49d407f";
        private const string DarkUssGuid = "92ba7ccaff04f0949b49e789cd1afcde";
        private const string LightUssGuid = "bea0879b670049d4fbd7d0d96cee21da";

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualTreeAsset uxml = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<VisualTreeAsset>(UxmlGuid);
            TemplateContainer container = uxml.CloneTree();
            StyleSheet uss = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(UssGuid);
            container.styleSheets.Add(uss);

            AddSkinSpecificStyleSheetToContainer(container);

            Label titleLabel = (Label)container.Q("titleLabel");
            SerializedProperty fieldNameProperty = property.FindPropertyRelative("fieldName");
            string fieldName = fieldNameProperty.stringValue;
            titleLabel.text = fieldName;

            SerializedProperty edgesProperty = property.FindPropertyRelative("edges");
            ConfigureNodeConnectorView(container, property, edgesProperty);

            VisualElement content = container.Q("content");
            int edgesCount = edgesProperty.arraySize;
            if (edgesCount > 0)
            {
                for (int i = 0; i < edgesCount; i++)
                {
                    SerializedProperty edgeProperty = edgesProperty.GetArrayElementAtIndex(i);
                    var propertyField = new PropertyField(edgeProperty);
                    content.Add(propertyField);
                }
            }
            else
            {
                Label emptyLabel = new Label("No nodes connected.");
                content.Add(emptyLabel);
            }

            return container;
        }

        private void AddSkinSpecificStyleSheetToContainer(VisualElement container)
        {
            string skinSpecificStyleSheetGuid = (EditorGUIUtility.isProSkin) ? DarkUssGuid : LightUssGuid;
            StyleSheet skinSpecificStyleSheet = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(skinSpecificStyleSheetGuid);
            container.styleSheets.Add(skinSpecificStyleSheet);
        }

        private void ConfigureNodeConnectorView(VisualElement container, SerializedProperty property, SerializedProperty edgesProperty)
        {
            NodeConnectorView nodeConnectorView = (NodeConnectorView)container.Q("nodeConnectorView");

            SerializedProperty allowsMultipleConnectionsProperty = property.FindPropertyRelative("allowsMultipleConnections");
            bool allowsMultipleConnections = allowsMultipleConnectionsProperty.boolValue;
            if (allowsMultipleConnections)
            {
                nodeConnectorView.AllowNewConnections = true;
            }
            else
            {
                bool hasNoEdge = (edgesProperty.arraySize == 0);
                nodeConnectorView.AllowNewConnections = hasNoEdge;
            }
        }
    }
}
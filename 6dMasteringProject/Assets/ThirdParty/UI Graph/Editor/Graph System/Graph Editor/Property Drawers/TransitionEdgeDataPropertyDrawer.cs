// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using Pelican7.UIGraph;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public abstract class TransitionEdgeDataPropertyDrawer : PropertyDrawer
    {
        private const string UssGuid = "ad7ac55e293fd1148a4108bd0b0e988c";
        private const string DarkUssGuid = "c2bc109242966544e8b3179f7055e1c7";
        private const string LightUssGuid = "666b00fc8501a76418a43617fd8bb820";

        protected void CreateDrawerForEdgeInContainer(TransitionEdgeData edge, VisualElement container)
        {
            SerializedObject serializedEdge = new SerializedObject(edge);

            CreateDividerInContainer(container);

            VisualElement transitionBar = CreateTransitionBar(edge);
            container.Add(transitionBar);

            NodeData destinationNode = edge.destinationNode;
            string destinationName = destinationNode.resource.Name;
            Label destinationLabel = new Label(destinationName);
            destinationLabel.AddToClassList("destination-label");
            container.Add(destinationLabel);

            CreateDividerInContainer(container);
            CreatePropertyFieldsInContainer(serializedEdge, container);
            ApplyStyles(container);
        }

        private void CreateDividerInContainer(VisualElement container)
        {
            VisualElement divider = new VisualElement();
            divider.AddToClassList("divider");
            container.Add(divider);
        }

        private VisualElement CreateTransitionBar(TransitionEdgeData edge)
        {
            bool isDismissalEdge = edge.type == EdgeData.EdgeType.Dismissal;
            VisualElement transitionBar = new VisualElement()
            {
                style =
                {
                    backgroundColor = (isDismissalEdge ? ColorUtility.GraphUpstreamColorTransparent : ColorUtility.GraphDownstreamColorTransparent)
                }
            };
            transitionBar.AddToClassList("transition-bar");

            Label transitionLabel = new Label(edge.transitionDisplayName);
            transitionBar.Add(transitionLabel);

            return transitionBar;
        }

        private void CreatePropertyFieldsInContainer(SerializedObject serializedEdge, VisualElement container)
        {
            // Not using PropertyFields here because they appear to behave incorrectly for nested bindings (2019.1.0f2). For example, the userIdentifier property fields gets bound to the parent node's userIdentifier rather than the edges. Changes are also not propagated to objects.
            VisualElement propertyList = new VisualElement();
            propertyList.AddToClassList("property-list");
            container.Add(propertyList);

            SerializedProperty animatedProperty = serializedEdge.FindProperty("animated");
            Toggle animatedToggle = new Toggle(animatedProperty.displayName)
            {
                value = animatedProperty.boolValue
            };
            animatedToggle.RegisterValueChangedCallback((ChangeEvent<bool> evt) =>
            {
                serializedEdge.Update();
                serializedEdge.FindProperty("animated").boolValue = evt.newValue;
                serializedEdge.ApplyModifiedProperties();
            });
            propertyList.Add(animatedToggle);

            SerializedProperty interactiveProperty = serializedEdge.FindProperty("interactive");
            Toggle interactiveToggle = new Toggle(interactiveProperty.displayName)
            {
                value = interactiveProperty.boolValue
            };
            interactiveToggle.RegisterValueChangedCallback((ChangeEvent<bool> evt) =>
            {
                serializedEdge.Update();
                serializedEdge.FindProperty("interactive").boolValue = evt.newValue;
                serializedEdge.ApplyModifiedProperties();
            });
            propertyList.Add(interactiveToggle);

            SerializedProperty userIdentifierProperty = serializedEdge.FindProperty("userIdentifier");
            TextField userIdentifierTextField = new TextField(userIdentifierProperty.displayName)
            {
                value = userIdentifierProperty.stringValue
            };
            userIdentifierTextField.RegisterValueChangedCallback((ChangeEvent<string> evt) =>
            {
                serializedEdge.Update();
                serializedEdge.FindProperty("userIdentifier").stringValue = evt.newValue;
                serializedEdge.ApplyModifiedProperties();
            });
            propertyList.Add(userIdentifierTextField);
        }

        private PropertyField CreatePropertyField(SerializedObject serializedEdge, string propertyPath)
        {
            SerializedProperty property = serializedEdge.FindProperty(propertyPath);
            PropertyField propertyField = new PropertyField(property);
            return propertyField;
        }

        private void ApplyStyles(VisualElement container)
        {
            container.AddToClassList("transition-edge");

            StyleSheet uss = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(UssGuid);
            container.styleSheets.Add(uss);

            string skinSpecificStyleSheetGuid = (EditorGUIUtility.isProSkin) ? DarkUssGuid : LightUssGuid;
            StyleSheet skinSpecificStyleSheet = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(skinSpecificStyleSheetGuid);
            container.styleSheets.Add(skinSpecificStyleSheet);
        }
    }
}
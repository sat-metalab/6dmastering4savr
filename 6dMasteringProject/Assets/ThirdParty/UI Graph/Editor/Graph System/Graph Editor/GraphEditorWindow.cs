﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class GraphEditorWindow : EditorWindow
    {
        private const string GraphEditorElementsControllerAssetGuid = "999bb169f61c58e4b8f4ed4ed394abfc";
        private const string WindowStyleSheetAssetGuid = "27649ae06e61744468e03ab4c045491c";

        [SerializeField] private bool isInitialized;
        [SerializeField] private PersistentWindowData persistentWindowData;
        [SerializeField] private GraphResource graph;

        public GraphResource Graph
        {
            get
            {
                return graph;
            }

            private set
            {
                graph = value;
            }
        }

        private ElementsWindow Window { get; set; }

        public void Initialize(GraphResource graph)
        {
            Graph = graph;
            graph.RefreshData();
            persistentWindowData = new PersistentWindowData();
            isInitialized = true;

            BuildWindow();
            SubscribeToGlobalEngineEvents();
        }

        private void OnEnable()
        {
            // Unity calls OnEnable on the EditorWindow when it is created for the first time, before we get a chance to call Initialize(). Return early in this case and build the window in Initialize().
            if (isInitialized == false)
            {
                return;
            }

            BuildWindow();
            SubscribeToGlobalEngineEvents();
        }

        private void OnDisable()
        {
            DestroyAllViewControllersInWindowIfNecessary();
            UnsubscribeFromGlobalEngineEvents();
        }

        private void OnProjectChange()
        {
            RefreshWindowTitle();
        }

        private void BuildWindow()
        {
            DestroyAllViewControllersInWindowIfNecessary();

            RefreshWindowTitle();
            CreateWindowAndRootViewController();
        }

        private void RefreshWindowTitle()
        {
            titleContent = new GUIContent(Graph.name);
        }

        private void CreateWindowAndRootViewController()
        {
            Window = new ElementsWindow();
            rootVisualElement.Add(Window);

            StyleSheet styleSheet = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(WindowStyleSheetAssetGuid);
            Window.styleSheets.Add(styleSheet);

            GraphEditorElementsController graphEditorControllerTemplate = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<GraphEditorElementsController>(GraphEditorElementsControllerAssetGuid);
            GraphEditorElementsController graphEditorController = Instantiate(graphEditorControllerTemplate);
            graphEditorController.InitializeWithGraph(Graph, persistentWindowData);

            Window.RootViewController = graphEditorController;
            Window.Present();
        }

        private void OnUndoRedoPerformed()
        {
            rootVisualElement.Clear();
            BuildWindow();
        }

        private void SubscribeToGlobalEngineEvents()
        {
            Undo.undoRedoPerformed += OnUndoRedoPerformed;
            EditorSceneManager.activeSceneChangedInEditMode += OnActiveSceneChanged;
        }

        private void UnsubscribeFromGlobalEngineEvents()
        {
            Undo.undoRedoPerformed -= OnUndoRedoPerformed;
            EditorSceneManager.activeSceneChangedInEditMode -= OnActiveSceneChanged;
        } 

        // This is also called when exiting play mode (which also doesn't cause a domain reload).
        private void OnActiveSceneChanged(UnityEngine.SceneManagement.Scene arg0, UnityEngine.SceneManagement.Scene arg1)
        {
            BuildWindow();
        }

        private void DestroyAllViewControllersInWindowIfNecessary()
        {
            if (Window != null)
            {
                var viewController = Window.RootViewController;
                while (viewController != null)
                {
                    var viewControllerToDestroy = viewController;
                    viewController = viewController.PresentedViewController;
                    viewControllerToDestroy.Destroy();
                }
            }
        }

        // [Developer Note] When UIElements exposes the viewDataKey API for custom Visual Elements, we can use it to replace manually storing UI-specific data on the window instance.
        [System.Serializable]
        public class PersistentWindowData
        {
            public Vector3 graphContainerPosition = Vector3.zero;
            public Vector3 graphContainerScale = Vector3.one;
        }
    }
}
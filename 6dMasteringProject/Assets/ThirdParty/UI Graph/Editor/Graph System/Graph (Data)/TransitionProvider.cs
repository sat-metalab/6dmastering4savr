﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph.Editor
{
    public class TransitionProvider
    {
        public NodeData targetNode;
        public List<ViewControllerTransitionIdentifier> transitionIdentifiers;

        public TransitionProvider(NodeData targetNode, List<ViewControllerTransitionIdentifier> transitionIdentifiers)
        {
            this.targetNode = targetNode;
            this.transitionIdentifiers = transitionIdentifiers;
        }

        public string Name
        {

            get
            {
                return targetNode.resource.Name;
            }
        }
    }
}
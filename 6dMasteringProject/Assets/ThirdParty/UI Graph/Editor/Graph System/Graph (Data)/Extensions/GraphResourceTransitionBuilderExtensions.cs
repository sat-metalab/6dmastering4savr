﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class GraphResourceTransitionBuilderExtensions
    {
        public static bool TryBuildTransitionProviderListBetweenNodes(this GraphResource graph, NodeData source, NodeData destination, out List<TransitionProvider> transitionProviders, out Error error)
        {
            transitionProviders = null;
            error = null;

            GraphResourcePathTraversalExtensions.TransitionEdgePathDirection transitionEdgePathDirection = graph.TransitionEdgePathDirectionFromNodeToNode(source, destination);
            if (transitionEdgePathDirection == GraphResourcePathTraversalExtensions.TransitionEdgePathDirection.Presentation)
            {
                graph.TryGetTransitionProvidersForPresentationPath(source, destination, out transitionProviders, out error);
            }
            else if (transitionEdgePathDirection == GraphResourcePathTraversalExtensions.TransitionEdgePathDirection.Dismissal)
            {
                graph.TryGetTransitionProvidersForDismissalPath(source, destination, out transitionProviders, out error);
            }
            else if (transitionEdgePathDirection == GraphResourcePathTraversalExtensions.TransitionEdgePathDirection.Replacement)
            {
                graph.TryGetTransitionProvidersForReplacementPath(source, destination, out transitionProviders, out error);
            }
            else if (transitionEdgePathDirection == GraphResourcePathTraversalExtensions.TransitionEdgePathDirection.NotConnected)
            {
                graph.TryGetTransitionProvidersForNotConnectedPath(source, destination, out transitionProviders, out error);
            }

            return (error == null);
        }

        private static void TryGetTransitionProvidersForReplacementPath(this GraphResource graph, NodeData source, NodeData destination, out List<TransitionProvider> transitionProviders, out Error error)
        {
            // When a 'Replacement' transition path exists between the nodes, they can only be connected if they are adjacent in the direction source-->destination.
            if (TryGetReplacementEdgeFromNodeToNode(source, destination, out TransitionEdgeData replacementEdge))
            {
                transitionProviders = graph.TransitionProvidersForExistingEdge(replacementEdge);
                error = null;
            }
            else
            {
                transitionProviders = null;
                error = new Error("These nodes contain a replacement transition between them.");
            }
        }

        private static void TryGetTransitionProvidersForPresentationPath(this GraphResource graph, NodeData source, NodeData destination, out List<TransitionProvider> transitionProviders, out Error error)
        {
            // When a 'Presentation' transition path exists between the nodes, they can only be connected if they are adjacent.
            if (NodesAreAdjacentViaTransitionEdge(source, destination, out TransitionEdgeData adjacentTransitionEdge))
            {
                graph.TryGetTransitionProvidersForNodesAdjacentViaTransitionEdge(source, destination, adjacentTransitionEdge, out transitionProviders, out error);
            }
            else
            {
                transitionProviders = null;
                error = new Error("These nodes are not connectable via transition.");
            }
        }

        private static void TryGetTransitionProvidersForDismissalPath(this GraphResource graph, NodeData source, NodeData destination, out List<TransitionProvider> transitionProviders, out Error error)
        {
            transitionProviders = null;

            // When a 'Dismissal' transition path exists between the nodes, they can be connected if they are adjacent, they have a lowest common ancestor AND the destination has a lower ancestor depth than the source, or the destination has no parent.
            if (NodesAreAdjacentViaTransitionEdge(source, destination, out TransitionEdgeData adjacentTransitionEdge))
            {
                graph.TryGetTransitionProvidersForNodesAdjacentViaTransitionEdge(source, destination, adjacentTransitionEdge, out transitionProviders, out error);
            }
            else
            {
                if ((destination.AncestorDepth() <= source.AncestorDepth()) && TryFindLowestCommonAncestor(destination, source, out NodeData lowestCommonAncestor))
                {
                    NodeData target = lowestCommonAncestor;
                    transitionProviders = graph.CollectTransitionProvidersForNonAdjacentDismissalPathWithLowestCommonAncestor(target, out error);
                }
                else if (destination.IsOrphan())
                {
                    NodeData target = destination;
                    transitionProviders = graph.CollectTransitionProvidersForNonAdjacentDismissalPathBetweenOrphans(target, out error);
                }
                else
                {
                    error = new Error("These nodes are not connectable via transition.");
                }
            }
        }

        private static void TryGetTransitionProvidersForNodesAdjacentViaTransitionEdge(this GraphResource graph, NodeData source, NodeData destination, TransitionEdgeData adjacentTransitionEdge, out List<TransitionProvider> transitionProviders, out Error error)
        {
            TransitionEdgeData alignedAdjacentTransitionEdge = TransitionEdgeFromNodeToNode(source, destination);
            if (alignedAdjacentTransitionEdge != null)
            {
                // An adjacent edge exists from source to destination. Offer the same transition identifier only.
                transitionProviders = graph.TransitionProvidersForExistingEdge(alignedAdjacentTransitionEdge);
                error = null;
            }
            else
            {
                // An adjacent edge does not exist from source to destination, meaning adjacentTransitionEdge points from destination to source. Offer the reverse transition identifier only.
                graph.TryGetTransitionProvidersOnTargetToReverseEdge(adjacentTransitionEdge, out transitionProviders, out error);
            }
        }

        private static List<TransitionProvider> TransitionProvidersForExistingEdge(this GraphResource graph, TransitionEdgeData transitionEdge)
        {
            NodeData target = transitionEdge.targetNode;
            ViewControllerTransitionIdentifier transitionIdentifier = transitionEdge.TransitionIdentifier;
            return new List<TransitionProvider>()
            {
                new TransitionProvider(target, new List<ViewControllerTransitionIdentifier>()
                {
                    transitionIdentifier
                })
            };
        }

        private static void TryGetTransitionProvidersOnTargetToReverseEdge(this GraphResource graph, TransitionEdgeData transitionEdge, out List<TransitionProvider> transitionProviders, out Error error)
        {
            NodeData target = transitionEdge.targetNode;
            GraphableTransitionIdentifier graphableTransitionIdentifier = transitionEdge.GraphableTransitionIdentifier;
            if (graphableTransitionIdentifier.HasReverseTransition)
            {
                transitionProviders = new List<TransitionProvider>()
                {
                    new TransitionProvider(target, new List<ViewControllerTransitionIdentifier>()
                    {
                        graphableTransitionIdentifier.ReverseTransitionIdentifier
                    })
                };
                error = null;
            }
            else
            {
                transitionProviders = null;
                error = new Error("No transitions were offered between these nodes.");
            }
        }

        private static void TryGetTransitionProvidersForNotConnectedPath(this GraphResource graph, NodeData source, NodeData destination, out List<TransitionProvider> transitionProviders, out Error error)
        {
            // Nodes that are 'Not Connected' can always be connected if the graph contains no existing edges to the destination node.
            if (graph.HasAnyEdgeWithDestinationNode(destination) == false)
            {
                NodeData target = source;
                transitionProviders = graph.CollectTransitionProvidersForNotConnectedPathFromParentHierarchy(source, destination, target, out error);
            }
            else
            {
                // 'Not Connected' nodes that are the desintation of existing edges can be connected if they are siblings.
                if (NodesAreSiblings(source, destination))
                {
                    NodeData target = source.parent;
                    transitionProviders = graph.CollectTransitionProvidersForNotConnectedSiblings(target, out error);
                }
                else
                {
                    transitionProviders = null;
                    error = new Error("The destination node is already connected to.");
                }
            }
        }

        private static List<TransitionProvider> CollectTransitionProvidersForNotConnectedPathFromParentHierarchy(this GraphResource graph, NodeData source, NodeData destination, NodeData target, out Error error)
        {
            return graph.CollectTransitionProvidersOnTarget(target, (targetNode, graphableTransitionIdentifier) =>
            {
                // Allow 'Presentation' and 'Replacement' transition types for not connected paths.
                bool isDownstreamTransition = ((graphableTransitionIdentifier.TransitionType == ViewControllerTransitionIdentifier.TransitionType.Presentation) || (graphableTransitionIdentifier.TransitionType == ViewControllerTransitionIdentifier.TransitionType.Replacement));

                // Only the target node provides non-containment transitions (present/dismiss).
                bool excludePresentDismissTransitions = (targetNode.Equals(target) == false);
                if (excludePresentDismissTransitions)
                {
                    bool isPresentDismissTransition = graphableTransitionIdentifier.TransitionIdentifier.IsNotContainmentTransition();
                    return (isDownstreamTransition && (isPresentDismissTransition == false));
                }

                return isDownstreamTransition;
            }, out error);
        }

        private static List<TransitionProvider> CollectTransitionProvidersForNonAdjacentDismissalPathWithLowestCommonAncestor(this GraphResource graph, NodeData target, out Error error)
        {
            return graph.CollectTransitionProvidersOnTarget(target, (targetNode, graphableTransitionIdentifier) =>
            {
                // Allow 'Dismissal' transition types for LCA dismissal paths.
                bool isDismissal = (graphableTransitionIdentifier.TransitionType == ViewControllerTransitionIdentifier.TransitionType.Dismissal);

                // Allow containment transitions only (not present/dismiss) for non-adjacent dismissal paths with LCA.
                bool isContainmentTransition = graphableTransitionIdentifier.TransitionIdentifier.IsContainmentTransition();

                return isDismissal && isContainmentTransition;
            }, out error);
        }

        private static List<TransitionProvider> CollectTransitionProvidersForNonAdjacentDismissalPathBetweenOrphans(this GraphResource graph, NodeData target, out Error error)
        {
            return graph.CollectTransitionProvidersOnTarget(target, (targetNode, graphableTransitionIdentifier) =>
            {
                // Allow 'Dismissal' transition types for LCA dismissal paths.
                bool isDismissal = (graphableTransitionIdentifier.TransitionType == ViewControllerTransitionIdentifier.TransitionType.Dismissal);

                // Allow non-containment transitions only (present/dismiss) for non-adjacent dismissal paths between orphans.
                bool isNotContainmentTransition = graphableTransitionIdentifier.TransitionIdentifier.IsNotContainmentTransition();

                return isDismissal && isNotContainmentTransition;
            }, out error);
        }

        private static List<TransitionProvider> CollectTransitionProvidersForNotConnectedSiblings(this GraphResource graph, NodeData target, out Error error)
        {
            return graph.CollectTransitionProvidersOnTarget(target, (targetNode, graphableTransitionIdentifier) =>
            {
                // Allow 'Downstream' containment transition types for not connected siblings.
                bool isDownstreamContainmentTransition = (graphableTransitionIdentifier.TransitionIdentifier.IsDownstreamContainmentTransition());
                return isDownstreamContainmentTransition;

            }, out error);
        }

        private static List<TransitionProvider> CollectTransitionProvidersOnTarget(this GraphResource graph, NodeData target, System.Func<NodeData, GraphableTransitionIdentifier, bool> transitionsFilter, out Error error)
        {
            List<TransitionProvider> transitionProviders = new List<TransitionProvider>();
            error = null;

            NodeData targetNode = target;
            while (targetNode != null)
            {
                IGraphable graphable = targetNode.resource.Value;
                List<GraphableTransitionIdentifier> graphableTransitionIdentifiers = graphable.GraphableTransitionIdentifiers();
                graphableTransitionIdentifiers = graphableTransitionIdentifiers.Where((gti) => transitionsFilter(targetNode, gti)).ToList();

                if (graphableTransitionIdentifiers.Count > 0)
                {
                    List<ViewControllerTransitionIdentifier> transitionIdentifiers = graphableTransitionIdentifiers.Select(gti => gti.TransitionIdentifier).ToList();
                    transitionProviders.Add(new TransitionProvider(targetNode, transitionIdentifiers));
                }

                targetNode = targetNode.parent;
            }

            if (transitionProviders.Count == 0)
            {
                error = new Error("No transitions were offered between these nodes.");
            }

            return transitionProviders;
        }

        private static bool NodesAreAdjacentViaTransitionEdge(NodeData source, NodeData destination, out TransitionEdgeData adjacentTransitionEdge)
        {
            foreach (EdgeData edge in source.EdgesWithTypes(EdgeData.EdgeType.AllTransitionTypes))
            {
                if (edge.destinationNode.Equals(destination))
                {
                    adjacentTransitionEdge = (TransitionEdgeData)edge;
                    return true;
                }
            }

            foreach (EdgeData edge in destination.EdgesWithTypes(EdgeData.EdgeType.AllTransitionTypes))
            {
                if (edge.destinationNode.Equals(source))
                {
                    adjacentTransitionEdge = (TransitionEdgeData)edge;
                    return true;
                }
            }

            adjacentTransitionEdge = null;
            return false;
        }

        private static TransitionEdgeData TransitionEdgeFromNodeToNode(NodeData source, NodeData destination)
        {
            TransitionEdgeData transitionEdge = null;
            foreach (EdgeData edge in source.EdgesWithTypes(EdgeData.EdgeType.AllTransitionTypes))
            {
                if (edge.destinationNode.Equals(destination))
                {
                    transitionEdge = (TransitionEdgeData)edge;
                    break;
                }
            }

            return transitionEdge;
        }

        private static bool TryFindLowestCommonAncestor(NodeData nodeA, NodeData nodeB, out NodeData lca)
        {
            lca = null;

            HashSet<NodeData> nodeAHierarchy = new HashSet<NodeData>();
            NodeData nodeAAncestor = nodeA;
            while (nodeAAncestor != null)
            {
                nodeAHierarchy.Add(nodeAAncestor);
                nodeAAncestor = nodeAAncestor.parent;
            }

            NodeData nodeBAncestor = nodeB;
            while (nodeBAncestor != null)
            {
                if (nodeAHierarchy.Contains(nodeBAncestor))
                {
                    lca = nodeBAncestor;
                    break;
                }

                nodeBAncestor = nodeBAncestor.parent;
            }

            return (lca != null);
        }

        private static bool TryGetReplacementEdgeFromNodeToNode(NodeData source, NodeData destination, out TransitionEdgeData replacementEdge)
        {
            foreach (EdgeData edge in source.EdgesWithTypes(EdgeData.EdgeType.Replacement))
            {
                if (edge.destinationNode.Equals(destination))
                {
                    replacementEdge = (TransitionEdgeData)edge;
                    return true;
                }
            }

            replacementEdge = null;
            return false;
        }

        private static bool NodesAreSiblings(NodeData source, NodeData destination)
        {
            bool nodesAreSiblings = false;

            NodeData sourceParent = source.parent;
            NodeData destinationParent = destination.parent;
            if ((sourceParent != null) && (destinationParent != null))
            {
                nodesAreSiblings = sourceParent.Equals(destinationParent);
            }

            return nodesAreSiblings;
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class EdgeDataEditorExtensions
    {
        public static bool InvolvesNode(this EdgeData edge, NodeData node)
        {
            return (edge.sourceNode.Equals(node) || edge.destinationNode.Equals(node) || edge.targetNode.Equals(node));
        }

        public static void ConfigureWithUndo(this EdgeData edge, string undoGroupName)
        {
            // Add the edge to the source node.
            NodeData sourceNode = edge.sourceNode;
            Undo.RecordObject(sourceNode, undoGroupName);
            edge.AddToSourceNode();

            // Set the destination node's parent, if necessary.
            NodeData destinationNode = edge.destinationNode;
            Undo.RecordObject(destinationNode, undoGroupName);
            edge.AssignDestinationNodeParentIfNecessary();
        }

        public static void PrepareForDeletionWithUndo(this EdgeData edge, string undoGroupName)
        {
            // Remove the edge from the source node.
            NodeData sourceNode = edge.sourceNode;
            Undo.RecordObject(sourceNode, undoGroupName);
            edge.RemoveFromSourceNode();

            // Nullify the destination node's parent, if necessary.
            NodeData destinationNode = edge.destinationNode;
            Undo.RecordObject(destinationNode, undoGroupName);
            edge.NullifyDestinationNodeParentIfNecessary();
        }
    }
}
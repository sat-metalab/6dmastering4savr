﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class GraphResourceDataModificationExtensions
    {
        public static void RefreshData(this GraphResource graph)
        {
            foreach (NodeData node in graph.nodes)
            {
                node.Refresh();
            }
        }

        public static void TryAddNodeForAssetAtPath(this GraphResource graph, string assetPath, Vector2 position)
        {
            if (TryCreateNodeForAsset(assetPath, out IGraphable graphable, out NodeData node, out Error error) == false)
            {
                error.LogWithPrefix("Cannot add node.");
                return;
            }

            string undoGroupName = "Create Node";
            Undo.SetCurrentGroupName(undoGroupName);
            int undoGroupIndex = Undo.GetCurrentGroup();

            node.Initialize(graph, graphable, position);
            graph.AddNodeObjectAndMakeSubObject(node, undoGroupName);

            // Set the graph's origin, if necessary.
            Undo.RecordObject(graph, undoGroupName);
            if (graph.nodes.Count == 1)
            {
                graph.origin = node;
            }

            Undo.CollapseUndoOperations(undoGroupIndex);
            graph.OnNodeAdded.Invoke(node);
        }

        public static void DeleteNode(this GraphResource graph, NodeData node)
        {
            int nodeIndex = graph.nodes.IndexOf(node);
            if (nodeIndex < 0)
            {
                Error.Log("Cannot delete node. It was not found in the graph.");
                return;
            }

            string undoGroupName = "Delete Node";
            Undo.SetCurrentGroupName(undoGroupName);
            int undoGroupIndex = Undo.GetCurrentGroup();

            bool isOrigin = node.Equals(graph.origin);

            // Remove the node from the graph's list.
            Undo.RecordObject(graph, undoGroupName);
            graph.nodes.RemoveAt(nodeIndex);

            // Set the graph's origin, if necessary.
            if (isOrigin)
            {
                graph.origin = null;
            }

            // Destroy all edges that involve this node.
            // Take a copy of the array to avoid mutating whilst iterating (which causes issues when undo-ing the operation).
            List<EdgeData> edges = new List<EdgeData>(graph.edges);
            foreach (EdgeData edge in edges)
            {
                if (edge.InvolvesNode(node))
                {
                    graph.DeleteEdge(edge, undoGroupName);
                }
            }

            // Destroy the node object.
            Undo.DestroyObjectImmediate(node);
            Undo.CollapseUndoOperations(undoGroupIndex);

            graph.OnNodeRemoved.Invoke(node);
        }

        public static bool TrySetOrigin(this GraphResource graph, NodeData origin, out Error error)
        {
            if (graph.CanSetOriginWithNode(origin, out error) == false)
            {
                return false;
            }

            NodeData previousOrigin = graph.origin;
            Undo.RecordObject(graph, "Set Origin");
            graph.origin = origin;

            graph.OnOriginChanged.Invoke(origin, previousOrigin);

            return true;
        }

        public static bool TryCreateFieldEdge(this GraphResource graph, string fieldPortName, ViewControllerNodeData sourceNode, NodeData destinationNode, out Error error)
        {
            if (graph.CanCreateFieldEdgeWithParameters(fieldPortName, sourceNode, destinationNode, out error) == false)
            {
                return false;
            }

            string undoGroupName = "Create Edge";
            int undoGroupIndex = Undo.GetCurrentGroup();

            // Create the edge sub-object in the graph.
            FieldEdgeData fieldEdge = ScriptableObject.CreateInstance<FieldEdgeData>();
            fieldEdge.Initialize(graph, fieldPortName, sourceNode, destinationNode);
            graph.AddEdgeObjectAndMakeSubObject(fieldEdge, undoGroupName);
            fieldEdge.ConfigureWithUndo(undoGroupName);

            Undo.CollapseUndoOperations(undoGroupIndex);
            graph.OnEdgeAdded.Invoke(fieldEdge);

            error = null;
            return true;
        }

        public static void DeleteFieldEdge(this GraphResource graph, FieldEdgeData fieldEdge, string undoGroupName = null)
        {
            graph.DeleteEdge(fieldEdge, undoGroupName);
        }

        public static bool TryCreateEmbedViewObjectEdge(this GraphResource graph, string fieldPortName, ViewControllerNodeData sourceNode, NodeData destinationNode, out Error error)
        {
            if (graph.CanCreateEmbedViewObjectEdgeWithParameters(fieldPortName, sourceNode, destinationNode, out error) == false)
            {
                return false;
            }

            string undoGroupName = "Create Edge";
            int undoGroupIndex = Undo.GetCurrentGroup();

            // Create the edge sub-object in the graph.
            EmbedViewObjectEdgeData embedViewObjectEdge = ScriptableObject.CreateInstance<EmbedViewObjectEdgeData>();
            embedViewObjectEdge.Initialize(graph, fieldPortName, sourceNode, destinationNode);
            graph.AddEdgeObjectAndMakeSubObject(embedViewObjectEdge, undoGroupName);
            embedViewObjectEdge.ConfigureWithUndo(undoGroupName);

            Undo.CollapseUndoOperations(undoGroupIndex);
            graph.OnEdgeAdded.Invoke(embedViewObjectEdge);

            error = null;
            return true;
        }

        public static void DeleteEmbedViewObjectEdge(this GraphResource graph, EmbedViewObjectEdgeData embedViewObjectEdge, string undoGroupName = null)
        {
            graph.DeleteEdge(embedViewObjectEdge, undoGroupName);
        }

        public static void CreateTransitionViewObjectEdge(this GraphResource graph, string viewObjectGuid, string triggerGuid, ViewControllerNodeData sourceNode, NodeData destinationNode, ViewControllerNodeData targetNode, ViewControllerTransitionIdentifier transitionIdentifier)
        {
            string undoGroupName = "Create Edge";
            int undoGroupIndex = Undo.GetCurrentGroup();

            // Create the edge sub-object in the graph.
            TransitionViewObjectEdgeData viewObjectEdge = ScriptableObject.CreateInstance<TransitionViewObjectEdgeData>();
            viewObjectEdge.Initialize(graph, sourceNode, destinationNode, targetNode, transitionIdentifier, viewObjectGuid, triggerGuid);
            graph.AddEdgeObjectAndMakeSubObject(viewObjectEdge, undoGroupName);
            viewObjectEdge.ConfigureWithUndo(undoGroupName);

            Undo.CollapseUndoOperations(undoGroupIndex);
            graph.OnEdgeAdded.Invoke(viewObjectEdge);
        }

        public static void DeleteTransitionViewObjectEdge(this GraphResource graph, TransitionViewObjectEdgeData transitionViewObjectEdge, string undoGroupName = null)
        {
            graph.DeleteEdge(transitionViewObjectEdge, undoGroupName);
        }

        public static void CreateManualEdge(this GraphResource graph, ViewControllerNodeData sourceNode, NodeData destinationNode, ViewControllerNodeData targetNode, ViewControllerTransitionIdentifier transitionIdentifier)
        {
            string undoGroupName = "Create Edge";
            int undoGroupIndex = Undo.GetCurrentGroup();

            ManualTransitionEdgeData manualTransitionEdge = ScriptableObject.CreateInstance<ManualTransitionEdgeData>();
            manualTransitionEdge.Initialize(graph, sourceNode, destinationNode, targetNode, transitionIdentifier);
            graph.AddEdgeObjectAndMakeSubObject(manualTransitionEdge, undoGroupName);
            manualTransitionEdge.ConfigureWithUndo(undoGroupName);

            Undo.CollapseUndoOperations(undoGroupIndex);
            graph.OnEdgeAdded.Invoke(manualTransitionEdge);
        }

        public static void DeleteManualEdge(this GraphResource graph, ManualTransitionEdgeData manualTransitionEdge)
        {
            string undoGroupName = "Delete Edge";
            graph.DeleteEdge(manualTransitionEdge, undoGroupName);
        }

        private static bool TryCreateNodeForAsset(string assetPath, out IGraphable graphable, out NodeData node, out Error error)
        {
            if (TryLoadGraphableAssetAtPath(assetPath, out graphable, out error) == false)
            {
                node = null;
                return false;
            }

            return NodeDataFactory.TryCreateNodeForGraphable(graphable, out node, out error);
        }

        private static bool TryLoadGraphableAssetAtPath(string assetPath, out IGraphable graphable, out Error error)
        {
            Object asset = AssetDatabase.LoadAssetAtPath<Object>(assetPath);
            graphable = asset as IGraphable;

            bool success = (graphable != null);
            error = (success == false) ? new Error("The selected asset is not graphable.") : null;

            return success;
        }

        private static void AddNodeObjectAndMakeSubObject(this GraphResource graph, NodeData node, string undoGroupName)
        {
            graph.AddSubObject(node, undoGroupName);

            Undo.RecordObject(graph, undoGroupName);
            graph.nodes.Add(node);
        }

        private static void AddEdgeObjectAndMakeSubObject(this GraphResource graph, EdgeData edge, string undoGroupName)
        {
            graph.AddSubObject(edge, undoGroupName);

            Undo.RecordObject(graph, undoGroupName);
            graph.edges.Add(edge);
        }

        private static void AddSubObject(this GraphResource graph, Object subObject, string undoGroupName)
        {
            HideFlags hideFlags = HideFlags.HideInHierarchy;
            if (graph.SubObjectsAreVisible())
            {
                hideFlags = HideFlags.None;
            }

            Undo.RegisterCreatedObjectUndo(subObject, undoGroupName);
            AssetDatabase.AddObjectToAsset(subObject, graph);
            subObject.hideFlags = hideFlags;
        }

        private static void DeleteEdge(this GraphResource graph, EdgeData edge, string undoGroupName = null)
        {
            if (string.IsNullOrEmpty(undoGroupName))
            {
                undoGroupName = "Delete Edge";
            }
            Undo.SetCurrentGroupName(undoGroupName);
            int undoGroupIndex = Undo.GetCurrentGroup();

            // Remove the edge from the graph's list.
            Undo.RecordObject(graph, undoGroupName);
            graph.edges.Remove(edge);

            // Destroy the edge.
            edge.PrepareForDeletionWithUndo(undoGroupName);
            Undo.DestroyObjectImmediate(edge);

            Undo.CollapseUndoOperations(undoGroupIndex);
            graph.OnEdgeRemoved.Invoke(edge);
        }

        private static bool CanCreateFieldEdgeWithParameters(this GraphResource graph, string fieldPortName, ViewControllerNodeData sourceNode, NodeData destinationNode, out Error error)
        {
            if (destinationNode.HasParent())
            {
                error = new Error("The destination node already has a parent.");
                return false;
            }

            if (graph.HasAnyEdgeOfTypeWithDestinationNode(EdgeData.EdgeType.AllDownstreamTypes, destinationNode))
            {
                error = new Error("The destination node is already connected to.");
                return false;
            }

            if (graph.TryGetAnyPathBetweenNodesViaEdgesWithTypes(sourceNode, destinationNode, EdgeData.EdgeType.AllTypes, out _))
            {
                error = new Error("The nodes are already connected.");
                return false;
            }

            error = null;
            return true;
        }

        private static bool CanCreateEmbedViewObjectEdgeWithParameters(this GraphResource graph, string embedViewObjectGuid, ViewControllerNodeData sourceNode, NodeData destinationNode, out Error error)
        {
            if (destinationNode.HasParent())
            {
                error = new Error("The destination node already has a parent.");
                return false;
            }

            if (graph.HasAnyEdgeWithDestinationNode(destinationNode))
            {
                error = new Error("The destination node is already connected to.");
                return false;
            }

            if (graph.TryGetAnyPathBetweenNodesViaEdgesWithTypes(sourceNode, destinationNode, EdgeData.EdgeType.AllTypes, out _))
            {
                error = new Error("The nodes are already connected.");
                return false;
            }

            error = null;
            return true;
        }

        private static bool CanSetOriginWithNode(this GraphResource graph, NodeData origin, out Error error)
        {
            if (origin.HasParent())
            {
                error = new Error("An origin node cannot be contained within a parent.");
                return false;
            }

            if (graph.HasAnyEdgeOfTypeWithDestinationNode(EdgeData.EdgeType.AllDownstreamTypes, origin))
            {
                error = new Error("An origin node must have no downstream edges pointing to it.");
                return false;
            }

            error = null;
            return true;
        }
    }
}
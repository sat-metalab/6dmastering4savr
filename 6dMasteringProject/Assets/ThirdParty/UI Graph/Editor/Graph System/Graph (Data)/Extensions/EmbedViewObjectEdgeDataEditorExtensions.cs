﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph.Editor
{
    public static class EmbedViewObjectEdgeDataEditorExtensions
    {
        public static void Initialize(this EmbedViewObjectEdgeData embedViewObjectEdge, GraphResource graph, string embedViewObjectGuid, NodeData sourceNode, NodeData destinationNode)
        {
            embedViewObjectEdge.guid = System.Guid.NewGuid().ToString();
            embedViewObjectEdge.graph = graph;
            embedViewObjectEdge.embedViewObjectGuid = embedViewObjectGuid;
            embedViewObjectEdge.sourceNode = sourceNode;
            embedViewObjectEdge.destinationNode = destinationNode;
            embedViewObjectEdge.targetNode = sourceNode; // An embed view object edge's target is always its source.
            embedViewObjectEdge.type = EdgeData.EdgeType.Embed; // A embed view object edge's type is always embed.

            embedViewObjectEdge.name = string.Format("[EmbedViewObjectEdge] {0} --> {1} ({2})", sourceNode.resource.Name, destinationNode.resource.Name, embedViewObjectEdge.guid);
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class NodeDataEditorExtensions
    {
        public static void Initialize(this NodeData node, GraphResource graph, IGraphable graphable, Vector2 position)
        {
            node.guid = System.Guid.NewGuid().ToString();
            node.graph = graph;
            node.resource = new GraphableReference(graphable);
            node.SetPosition(position);

            node.name = string.Format("[Node] {0} ({1})", node.resource.Name, node.guid);

            node.Refresh();
        }

        public static void SetPosition(this NodeData node, Vector2 position)
        {
            Undo.SetCurrentGroupName("Move Node");

            var serializedNode = new SerializedObject(node);
            var serializedProperty = serializedNode.FindProperty("position");
            serializedProperty.vector2Value = position;
            serializedNode.ApplyModifiedProperties();
        }

        public static void Refresh(this NodeData node)
        {
            if (node is ViewControllerNodeData viewControllerNode)
            {
                viewControllerNode.Refresh();
            }
            else
            {
                throw new System.Exception("Unknown node type.");
            }
        }

        public static bool HasParent(this NodeData node)
        {
            return (node.parent != null);
        }

        public static bool IsOrphan(this NodeData node)
        {
            return (node.HasParent() == false);
        }

        public static int AncestorDepth(this NodeData node)
        {
            int depth = 0;

            NodeData parent = node.parent;
            while (parent != null)
            {
                depth++;
                parent = parent.parent;
            }

            return depth;
        }

        public static bool HasValidResource(this NodeData node)
        {
            GraphableReference resource = node.resource;
            return (resource.RawValue != null);
        }
    }
}
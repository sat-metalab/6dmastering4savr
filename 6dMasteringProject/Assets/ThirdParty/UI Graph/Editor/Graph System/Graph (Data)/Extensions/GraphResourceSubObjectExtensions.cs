﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class GraphResourceSubObjectExtensions
    {
        public static void ToggleSubObjectVisibility(this GraphResource graph)
        {
            if (graph.SubObjectsAreVisible())
            {
                graph.SetSubObjectHideFlags(HideFlags.HideInHierarchy);
            }
            else
            {
                graph.SetSubObjectHideFlags(HideFlags.None);
            }
        }

        public static bool SubObjectsAreVisible(this GraphResource graph)
        {
            Object subObject = graph.FirstSubObject();
            if (subObject != null)
            {
                return subObject.hideFlags == HideFlags.None;
            }

            return false;
        }

        private static void SetSubObjectHideFlags(this GraphResource graph, HideFlags hideFlags)
        {
            string graphPath = AssetDatabase.GetAssetPath(graph);
            Object[] graphObjects = AssetDatabase.LoadAllAssetsAtPath(graphPath);
            foreach (Object graphObject in graphObjects)
            {
                if (graphObject is GraphResource == false)
                {
                    graphObject.hideFlags = hideFlags;
                    EditorUtility.SetDirty(graphObject);
                }
            }

            AssetDatabase.SaveAssets();
        }

        private static Object FirstSubObject(this GraphResource graph)
        {
            Object subObject = null;

            string graphPath = AssetDatabase.GetAssetPath(graph);
            Object[] graphObjects = AssetDatabase.LoadAllAssetsAtPath(graphPath);
            foreach (Object graphObject in graphObjects)
            {
                if (graphObject is GraphResource == false)
                {
                    subObject = graphObject;
                    break;
                }
            }

            return subObject;
        }
    }
}
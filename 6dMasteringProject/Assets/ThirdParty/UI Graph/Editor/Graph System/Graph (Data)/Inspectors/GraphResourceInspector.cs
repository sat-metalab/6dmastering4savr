﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomEditor(typeof(GraphResource))]
    public class GraphResourceInspector : UnityEditor.Editor
    {
        public override VisualElement CreateInspectorGUI()
        {
            var root = new VisualElement();

            root.Add(new IMGUIContainer(() =>
            {
                EditorGUILayout.HelpBox("Open Graph for full editing support", MessageType.Info);
            }));

            root.Add(new Button(OpenGraph)
            {
                text = "Open Graph"
            });

            return root;
        }

        private void OpenGraph()
        {
            AssetDatabase.OpenAsset(target);
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public class CanvasViewResourceOwnerBindingsUpdater : ViewResourceOwnerBindingsUpdater<CanvasViewResource, CanvasView, CanvasWindow>
    {
        public CanvasViewResourceOwnerBindingsUpdater(CanvasViewResource viewResource) : base(viewResource) { }

        public override void UpdateBindingsWithOwner(Object owner)
        {
            if ((viewResource == null) || (viewResource.prefab == null))
            {
                return;
            }

            // In case the prefab is open, save it before modifying it.
            PrefabUtility.SavePrefabAsset(viewResource.prefab.gameObject);

            string assetPath = AssetDatabase.GetAssetPath(viewResource.prefab);
            GameObject viewPrefab = PrefabUtility.LoadPrefabContents(assetPath);
            if (viewPrefab == null)
            {
                Error.Log(string.Format("Cannot update view owner bindings for owner '{0}'", owner));
                return;
            }

            CanvasViewOwnerBindings viewOwnerBindings = viewResource.ViewOwnerBindingsInPrefab(viewPrefab);
            if (viewOwnerBindings == null)
            {
                Error.Log(string.Format("Cannot update view owner bindings for owner '{0}'", owner));
                return;
            }

            UpdateBindingsOwnerData(owner, viewOwnerBindings);
            UpdateBindingsViewReferences(owner, viewOwnerBindings);
            UpdateBindingsViewCallbacks(owner, viewOwnerBindings);

            PrefabUtility.SaveAsPrefabAsset(viewPrefab, assetPath);
            PrefabUtility.UnloadPrefabContents(viewPrefab);

            // Error.Log(string.Format("Processed view controller bindings: {0}", owner.name), Error.Severity.Log);
        }

        private void UpdateBindingsOwnerData(Object owner, CanvasViewOwnerBindings bindings)
        {
            var ownerData = new CanvasViewOwnerBindings.OwnerData(owner as CanvasController);
            bindings.ownerData = ownerData;
        }

        private void UpdateBindingsViewReferences(Object owner, CanvasViewOwnerBindings bindings)
        {
            var viewReferences = new List<CanvasViewReference>();
            var referenceFields = ViewReferenceAttributeCollector.CollectViewReferenceAttributedFields(owner);
            foreach (FieldInfo referenceField in referenceFields)
            {
                string identifier = referenceField.Name;
                string assemblyQualifiedTypeName = referenceField.FieldType.AssemblyQualifiedName;

                // View references that already exist in the current bindings (both identifier and type match) get copied over.
                CanvasViewReference viewReferenceToAdd = null;
                if (bindings.TryGetViewReferenceWithIdentifier(identifier, out CanvasViewReference viewReference))
                {
                    System.Type referenceFieldType = referenceField.FieldType;
                    if (viewReference.MatchesType(referenceFieldType))
                    {
                        viewReferenceToAdd = viewReference;
                    }
                }

                // If no view reference was found, or the type of an existing reference has changed, create a new view reference.
                if (viewReferenceToAdd == null)
                {
                    viewReferenceToAdd = new CanvasViewReference(identifier, assemblyQualifiedTypeName);
                }

                viewReferences.Add(viewReferenceToAdd);
            }

            bindings.viewReferences = viewReferences.ToArray();
        }

        private void UpdateBindingsViewCallbacks(Object owner, CanvasViewOwnerBindings bindings)
        {
            var viewCallbacks = new List<CanvasViewCallback>();
            var existingViewCallbacksToDelete = new HashSet<CanvasViewCallback>(bindings.viewCallbacks);
            var callbackMethods = ViewCallbackAttributeCollector.CollectViewCallbackAttributedMethods(owner);
            foreach (MethodInfo callbackMethod in callbackMethods)
            {
                string identifier = callbackMethod.GenerateIdentifier();

                // View callbacks that already exist in the current bindings get copied over.
                if (bindings.TryGetViewCallbackWithIdentifier(identifier, out CanvasViewCallback viewCallback))
                {
                    existingViewCallbacksToDelete.Remove(viewCallback);
                }

                // If no view callback was found create a new view callback in the prefab.
                if (viewCallback == null)
                {
                    viewCallback = CreateViewCallbackForMethodUnderBindings(callbackMethod, bindings);
                }

                viewCallbacks.Add(viewCallback);
            }

            // Delete any existing view callbacks that have been removed.
            foreach (var existingViewCallbackToDelete in existingViewCallbacksToDelete)
            {
                if (existingViewCallbackToDelete != null)
                {
                    Object.DestroyImmediate(existingViewCallbackToDelete.gameObject);
                }
            }

            bindings.viewCallbacks = viewCallbacks.ToArray();
        }

        private CanvasViewCallback CreateViewCallbackForMethodUnderBindings(MethodInfo method, CanvasViewOwnerBindings bindings)
        {
            var viewCallback = CanvasViewCallbackFactory.Create(method, bindings.transform);
            return viewCallback;
        }
    }
}
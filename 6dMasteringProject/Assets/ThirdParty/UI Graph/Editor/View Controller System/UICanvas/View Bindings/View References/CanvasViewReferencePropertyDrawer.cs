﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(CanvasViewReference))]
    public class CanvasViewReferencePropertyDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualElement container = new VisualElement();
            CreateObjectReferenceFieldInContainer(property, container);

            return container;
        }

        private void CreateObjectReferenceFieldInContainer(SerializedProperty property, VisualElement container)
        {
            SerializedProperty assemblyQualifiedTypeNameProperty = property.FindPropertyRelative("assemblyQualifiedTypeName");
            var assemblyQualifiedTypeName = assemblyQualifiedTypeNameProperty.stringValue;
            System.Type objectType = TypeFromName(assemblyQualifiedTypeName);
            if (objectType == null)
            {
                Error.Log(string.Format("Unable to resolve type name '{0}' to type.", assemblyQualifiedTypeName));
                return;
            }

            SerializedProperty identifierProperty = property.FindPropertyRelative("identifier");
            SerializedProperty objectReferenceProperty = property.FindPropertyRelative("objectReference");
            var objectReferenceField = new ObjectField()
            {
                objectType = objectType,
                value = objectReferenceProperty.objectReferenceValue,
                label = identifierProperty.stringValue
            };
            objectReferenceField.RegisterValueChangedCallback((changeEvent) =>
            {
                OnObjectReferenceValueChanged(changeEvent, objectReferenceProperty);
            });
            container.Add(objectReferenceField);
        }

        private System.Type TypeFromName(string assemblyQualifiedTypeName)
        {
            return System.Type.GetType(assemblyQualifiedTypeName);
        }

        private void OnObjectReferenceValueChanged(ChangeEvent<Object> changeEvent, SerializedProperty property)
        {
            property.serializedObject.Update();
            property.objectReferenceValue = changeEvent.newValue;
            property.serializedObject.ApplyModifiedProperties();
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph.Editor
{
    public class CanvasControllerScriptChangeViewBindingsProcessor : ViewControllerScriptChangeViewBindingsProcessor<CanvasController, CanvasView, CanvasWindow, CanvasViewResource, CanvasControllerTransition, CanvasControllerTransitionContext, CanvasControllerTransitionAnimatorProvider, ICanvasControllerTransitionProgressProvider, CanvasControllerTransitionAnimationDriver, CanvasControllerTransitionProgressProvider, CanvasControllerInvokeTransitionData, CanvasGraph>
    {
        protected override void UpdateViewControllerBindings(CanvasController viewController)
        {
            var viewResource = viewController.ViewResource;
            var viewResourceBindingsUpdater = new CanvasViewResourceOwnerBindingsUpdater(viewResource);
            viewResourceBindingsUpdater.UpdateBindingsWithOwner(viewController);
        }
    }
}
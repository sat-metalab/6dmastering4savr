﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomEditor(typeof(CanvasViewOwnerBindings))]
    public class CanvasViewOwnerBindingsInspector : InspectorBase
    {
        private static readonly HashSet<string> ExcludedFromAutomaticDrawingPropertyPaths = new HashSet<string>()
        {
            "m_Script",
            "viewReferences",
            "viewCallbacks",
            "ownerData"
        };

        public override VisualElement CreateInspectorGUI()
        {
            var container = CreateBaseInspector();

            AddArrayPropertySectionToContainer("viewReferences", container);
            AddArrayPropertySectionToContainer("viewCallbacks", container);

            VisualElement propertySection = new VisualElement();
            propertySection.AddToClassList(VisualElementEditorStyles.SectionStyleClass);
            SerializedProperty property = serializedObject.GetIterator();
            property.NextVisible(true);
            do
            {
                if (ExcludedFromAutomaticDrawingPropertyPaths.Contains(property.propertyPath) == false)
                {
                    var propertyField = new PropertyField(property);
                    propertySection.Add(propertyField);
                }
            }
            while (property.NextVisible(false));

            AddRefreshOwnerToContainer(propertySection);
            container.Add(propertySection);

            container.Bind(new SerializedObject(target));

            return container;
        }

        private void AddPropertyToContainer(string propertyPath, VisualElement container)
        {
            VisualElement section = new VisualElement();
            section.AddToClassList(VisualElementEditorStyles.SectionStyleClass);
            var property = serializedObject.FindProperty(propertyPath);
            section.Add(new PropertyField(property));
            container.Add(section);
        }

        private void AddArrayPropertySectionToContainer(string arrayPropertyPath, VisualElement container)
        {
            VisualElement section = new VisualElement();
            section.AddToClassList(VisualElementEditorStyles.SectionStyleClass);
            var arrayProperty = serializedObject.FindProperty(arrayPropertyPath);

            if (arrayProperty.isArray)
            {
                Label headerLabel = new Label(arrayProperty.displayName);
                headerLabel.AddToClassList(VisualElementEditorStyles.HeaderLabelStyleClass);
                section.Add(headerLabel);
                if (arrayProperty.arraySize > 0)
                {
                    for (int i = 0; i < arrayProperty.arraySize; i++)
                    {
                        var property = arrayProperty.GetArrayElementAtIndex(i);
                        section.Add(new PropertyField(property));
                    }
                }
                else
                {
                    var text = string.Format("No {0} Declared In Owner", arrayProperty.displayName);
                    section.Add(new Label(text));
                }
            }

            container.Add(section);
        }

        private void AddRefreshOwnerToContainer(VisualElement container)
        {
            VisualElement section = new VisualElement();
            section.AddToClassList(VisualElementEditorStyles.SectionStyleClass);

            var button = new Button(RefreshBindings)
            {
                text = "Refresh Bindings"
            };
            section.Add(button);
            container.Add(section);
        }

        private void RefreshBindings()
        {
            var ownerDataProperty = serializedObject.FindProperty("ownerData");
            var assetProperty = ownerDataProperty.FindPropertyRelative("asset");
            Object asset = assetProperty.objectReferenceValue;
            var viewController = asset as CanvasController;
            if (viewController == null)
            {
                Error.Log("Cannot refresh bindings. Owner is not a canvas controller.");
                return;
            }

            var viewResource = viewController.ViewResource;
            if (viewResource == null)
            {
                Error.Log("Cannot refresh bindings. The owning canvas controller has no view resource.");
                return;
            }

            var viewResourceBindingsUpdater = new CanvasViewResourceOwnerBindingsUpdater(viewResource);
            viewResourceBindingsUpdater.UpdateBindingsWithOwner(viewController);
        }
    }
}
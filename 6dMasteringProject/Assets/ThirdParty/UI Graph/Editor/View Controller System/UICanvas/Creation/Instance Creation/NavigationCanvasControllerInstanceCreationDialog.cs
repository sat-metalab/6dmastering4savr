﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;

namespace Pelican7.UIGraph.Editor
{
    public class NavigationCanvasControllerInstanceCreationDialog : ViewControllerInstanceCreationDialog<CanvasController, CanvasViewResource, CanvasControllerTransitionAnimatorProvider>
    {
        [MenuItem("Assets/Create/UI Graph/UICanvas/Navigation Canvas Controller", false, MenuItemPriority.Group0)]
        private static void Present()
        {
            string title = "New Navigation Canvas Controller";
            string defaultName = "NewNavigationCanvasController";
            var template = UIGraphProjectSettings.Instance.createNavigationCanvasControllerInstanceTemplate;

            var createDialog = new NavigationCanvasControllerInstanceCreationDialog();
            createDialog.Present(title, defaultName, template);
        }
    }
}
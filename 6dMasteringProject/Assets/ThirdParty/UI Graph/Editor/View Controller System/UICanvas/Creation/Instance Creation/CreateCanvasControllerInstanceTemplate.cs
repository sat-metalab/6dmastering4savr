﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.IO;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    [System.Serializable]
    public class CreateCanvasControllerInstanceTemplate : CreateViewControllerInstanceTemplate<CanvasController, CanvasViewResource, CanvasControllerTransitionAnimatorProvider>
    {
        protected override void CreateViewAssetAndConfigureObjects(string viewDirectory, string viewName, CanvasController viewControllerClone, CanvasViewResource viewResource)
        {
            viewControllerClone.transitionAnimatorProvider = transitionAnimatorProvider;

            // The cloned view controller will provide the view template.
            CanvasViewResource cloneViewResource = viewControllerClone.ViewResource;
            CanvasView viewTemplate = cloneViewResource.prefab;

            string viewPath = Path.ChangeExtension(Path.Combine(viewDirectory, viewName), "prefab");
            GameObject viewPrefabInstance = (GameObject)PrefabUtility.InstantiatePrefab(viewTemplate.gameObject);
            GameObject viewPrefab = PrefabUtility.SaveAsPrefabAsset(viewPrefabInstance, viewPath);
            var view = viewPrefab.GetComponent<CanvasView>();
            viewResource.prefab = view;
            viewControllerClone.ViewResource = viewResource;

            Object.DestroyImmediate(viewPrefabInstance);
        }

        protected override void ProcessNewViewControllerAsset(CanvasController viewController)
        {
            var viewResource = viewController.ViewResource;
            var viewResourceBindingsUpdater = new CanvasViewResourceOwnerBindingsUpdater(viewResource);
            viewResourceBindingsUpdater.UpdateBindingsWithOwner(viewController);
        }

        protected override bool CanCreate(out Error error)
        {
            if (base.CanCreate(out error) == false)
            {
                return false;
            }

            if (viewControllerTemplate.ViewResource == null)
            {
                error = new Error("The specified view controller template has no view resource.");
                return false;
            }

            if (viewControllerTemplate.ViewResource.prefab == null)
            {
                error = new Error("The specified view controller template's view resource has no prefab.");
                return false;
            }

            error = null;
            return true;
        }
    }
}
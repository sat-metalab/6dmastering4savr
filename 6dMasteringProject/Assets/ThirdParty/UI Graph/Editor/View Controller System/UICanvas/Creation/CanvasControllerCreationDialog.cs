﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;

namespace Pelican7.UIGraph.Editor
{
    public class CanvasControllerCreationDialog : ViewControllerCreationDialog<CanvasControllerTransitionAnimatorProvider>
    {
        [MenuItem("Assets/Create/UI Graph/UICanvas/Canvas Controller", false, MenuItemPriority.Group0)]
        private static void Present()
        {
            string title = "New Canvas Controller";
            string defaultName = "NewCanvasController";
            var template = UIGraphProjectSettings.Instance.createCanvasControllerTemplate;

            var canvasControllerCreator = new CanvasControllerCreationDialog();
            canvasControllerCreator.Present(title, defaultName, template);
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.IO;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    [System.Serializable]
    public class CreateCanvasControllerTemplate : CreateViewControllerTemplate<CanvasControllerTransitionAnimatorProvider>
    {
        private const string ViewResourceYAMLConfigFormat = "prefab: {{fileID: {0}, guid: {1}, type: 3}}\r\n";

        public CanvasView viewTemplate;

        protected override string ViewResourceScriptGUID => "15ec9b7b1b0e85844a32cf09f9d51f29";

        protected override bool CanCreate(out Error error)
        {
            if (base.CanCreate(out error) == false)
            {
                return false;
            }

            if (viewTemplate == null)
            {
                error = new Error("No view template is specified.");
                return false;
            }

            error = null;
            return true;
        }

        protected override string CreateViewAssetAndGenerateViewResourceYAMLConfig(string viewDirectory, string viewName)
        {
            string viewPath = Path.ChangeExtension(Path.Combine(viewDirectory, viewName), "prefab");
            GameObject viewPrefabInstance = (GameObject)PrefabUtility.InstantiatePrefab(viewTemplate.gameObject);
            GameObject viewPrefab = PrefabUtility.SaveAsPrefabAsset(viewPrefabInstance, viewPath);
            Object.DestroyImmediate(viewPrefabInstance);

            var view = viewPrefab.GetComponent<CanvasView>();
            return GenerateViewResourceYAMLConfigWithView(view);
        }

        private string GenerateViewResourceYAMLConfigWithView(CanvasView view)
        {
            AssetDatabase.TryGetGUIDAndLocalFileIdentifier(view, out string viewGUID, out long viewFileId);
            return string.Format(ViewResourceYAMLConfigFormat, viewFileId, viewGUID);
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [System.Serializable]
    public class CreateElementsControllerTemplate : CreateViewControllerTemplate<ElementsControllerTransitionAnimatorProvider>
    {
        private const string ViewResourceYAMLConfigFormat = "uxml: {{fileID: {0}, guid: {1}, type: 3}}\r\n  styleSheet: {{fileID: {2}, guid: {3},\r\n    type: 3}}\r\n";

        public TextAsset uxmlTemplate;
        public TextAsset styleSheetTemplate;

        protected override string ViewResourceScriptGUID => "0df225a0e899a544089c4222814ffe5a";

        protected override bool CanCreate(out Error error)
        {
            if (base.CanCreate(out error) == false)
            {
                return false;
            }

            if (uxmlTemplate == null)
            {
                error = new Error("No uxml template is specified.");
                return false;
            }

            if (styleSheetTemplate == null)
            {
                error = new Error("No stylesheet template is specified.");
                return false;
            }

            error = null;
            return true;
        }

        protected override string CreateViewAssetAndGenerateViewResourceYAMLConfig(string viewDirectory, string viewName)
        {
            string uxmlContents = uxmlTemplate.text;
            string uxmlPath = Path.ChangeExtension(Path.Combine(viewDirectory, viewName + " (Uxml)"), "uxml");
            File.WriteAllText(uxmlPath, uxmlContents);

            string styleSheetContents = styleSheetTemplate.text;
            string styleSheetPath = Path.ChangeExtension(Path.Combine(viewDirectory, viewName + " (Uss)"), "uss");
            File.WriteAllText(styleSheetPath, styleSheetContents);

            AssetDatabase.ImportAsset(uxmlPath, ImportAssetOptions.ForceSynchronousImport);
            AssetDatabase.ImportAsset(styleSheetPath, ImportAssetOptions.ForceSynchronousImport);

            var uxml = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(uxmlPath);
            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>(styleSheetPath);
            return GenerateViewResourceYAMLConfig(uxml, styleSheet);
        }

        private string GenerateViewResourceYAMLConfig(VisualTreeAsset uxml, StyleSheet styleSheet)
        {
            AssetDatabase.TryGetGUIDAndLocalFileIdentifier(uxml, out string uxmlGUID, out long uxmlFileId);
            AssetDatabase.TryGetGUIDAndLocalFileIdentifier(styleSheet, out string styleGUID, out long styleFileId);
            return string.Format(ViewResourceYAMLConfigFormat, uxmlFileId, uxmlGUID, styleFileId, styleGUID);
        }
    }
}
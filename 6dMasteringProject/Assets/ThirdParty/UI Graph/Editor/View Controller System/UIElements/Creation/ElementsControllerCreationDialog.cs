﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;

namespace Pelican7.UIGraph.Editor
{
    // [Developer Note] Currently for internal use only.
    //public class ElementsControllerCreationDialog : ViewControllerCreationDialog<ElementsControllerTransitionAnimatorProvider>
    //{
    //    [MenuItem("Assets/Create/UI Graph/UIElements/Elements Controller", false, MenuItemPriority.Group0)]
    //    private static void Present()
    //    {
    //        string title = "New Elements Controller";
    //        string defaultName = "NewElementsController";
    //        var template = UIGraphProjectSettings.Instance.createElementsControllerTemplate;

    //        var elementsControllerCreator = new ElementsControllerCreationDialog();
    //        elementsControllerCreator.Present(title, defaultName, template);
    //    }
    //}
}
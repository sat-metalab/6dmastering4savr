﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(ElementsViewReferenceData))]
    public class ElementsViewReferenceDataPropertyDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualElement container = new VisualElement()
            {
                style =
                {
                    flexDirection = FlexDirection.Row
                }
            };

            SerializedProperty identifier = property.FindPropertyRelative("identifier");
            container.Add(new Label(identifier.stringValue));

            VisualElement spacer = new VisualElement()
            {
                style =
                {
                    flexGrow = 1
                }
            };
            container.Add(spacer);

            SerializedProperty canBind = property.FindPropertyRelative("canBind");
            PropertyField canBindProperty = new PropertyField(canBind);
            canBindProperty.label = string.Empty;
            canBindProperty.SetEnabled(false);
            container.Add(canBindProperty);

            return container;
        }
    }
}
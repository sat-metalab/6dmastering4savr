﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class ElementsViewResourceOwnerBindingsUpdater : ViewResourceOwnerBindingsUpdater<ElementsViewResource, ElementsView, ElementsWindow>
    {
        public ElementsViewResourceOwnerBindingsUpdater(ElementsViewResource viewResource) : base(viewResource) { }

        public override void UpdateBindingsWithOwner(Object owner)
        {
            if ((viewResource == null) || (viewResource.uxml == null))
            {
                return;
            }

            ElementsViewOwnerBindingsEditorData bindings = viewResource.ownerBindingsEditorData;
            UpdateBindingsOwnerData(owner, bindings);
            UpdateBindingsViewReferences(owner, bindings);
        }

        private void UpdateBindingsOwnerData(Object owner, ElementsViewOwnerBindingsEditorData bindings)
        {
            var ownerData = new ElementsViewOwnerBindingsEditorData.OwnerData(owner as ElementsController);
            bindings.ownerData = ownerData;
        }

        private void UpdateBindingsViewReferences(Object owner, ElementsViewOwnerBindingsEditorData bindings)
        {
            var binder = new ElementsViewOwnerBinder();
            VisualElement view = viewResource.CloneView();
            var viewReferenceDatas = new List<ElementsViewReferenceData>();
            var referenceFields = ViewReferenceAttributeCollector.CollectViewReferenceAttributedFields(owner);
            foreach (var referenceField in referenceFields)
            {
                string identifier = referenceField.Name;
                VisualElement target = view.Q(identifier);
                bool canBindToTarget = binder.CanBindFieldToElement(referenceField, target, out Error error);

                var viewReferenceData = new ElementsViewReferenceData(identifier, canBindToTarget);
                viewReferenceDatas.Add(viewReferenceData);
            }

            bindings.viewReferenceDatas = viewReferenceDatas.ToArray();
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.IO;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public abstract class CreateViewControllerInstanceTemplate<TViewController, TViewResource, TTransitionAnimatorProvider>
        where TViewController : ScriptableObject
        where TViewResource : ScriptableObject
        where TTransitionAnimatorProvider : Object
    {
        public TViewController viewControllerTemplate;
        public TTransitionAnimatorProvider transitionAnimatorProvider;

        public void Create(string directory, string assetName, string viewName)
        {
            if (CanCreate(out Error error) == false)
            {
                error.LogWithPrefix("Cannot create view controller.");
                return;
            }

            TViewController viewController = Object.Instantiate(viewControllerTemplate);
            viewController.name = assetName;

            TViewResource viewResource = ScriptableObject.CreateInstance<TViewResource>();
            viewResource.name = "View Resource";

            CreateViewAssetAndConfigureObjects(directory, viewName, viewController, viewResource);

            string assetPath = Path.ChangeExtension(Path.Combine(directory, assetName), "asset");
            AssetDatabase.CreateAsset(viewController, assetPath);
            AssetDatabase.AddObjectToAsset(viewResource, viewController);

            ProcessNewViewControllerAsset(viewController);
            AssetDatabase.SaveAssets();
        }

        protected abstract void CreateViewAssetAndConfigureObjects(string viewDirectory, string viewName, TViewController viewControllerClone, TViewResource viewResource);
        protected abstract void ProcessNewViewControllerAsset(TViewController viewController);
        protected virtual bool CanCreate(out Error error)
        {
            if (viewControllerTemplate == null)
            {
                error = new Error("Specify a view controller template in the project settings.");
                return false;
            }

            if (transitionAnimatorProvider == null)
            {
                error = new Error("Specify a default view controller transition animator provider in the project settings.");
                return false;
            }

            error = null;
            return true;
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public abstract class CreateViewControllerTemplate<TTransitionAnimatorProvider>
        where TTransitionAnimatorProvider : Object
    {
        private const string ViewControllerAssetTemplateGUID = "37db6d0b7c6380143a8914be331a6261";
        private const string ScriptGUIDAssetTemplateSymbol = "___SCRIPTGUID___";
        private const string AssetNameAssetTemplateSymbol = "___ASSETNAME___";
        private const string TransitionAnimatorProviderGUIDAssetTemplateSymbol = "___TRANSITIONANIMATORPROVIDERGUID___";
        private const string ViewResourceGUIDAssetTemplateSymbol = "___VIEWRESOURCESCRIPTGUID___";
        private const string ViewResourceYAMLConfigTemplateSymbol = "___VIEWRESOURCEYAMLCONFIG___";
        private const string ClassNameScriptTemplateSymbol = "___CLASSNAME___";

        public TextAsset scriptTemplate;
        public TTransitionAnimatorProvider transitionAnimatorProvider;

        protected abstract string ViewResourceScriptGUID { get; }

        private string DefaultTransitionAnimatorProviderGUID
        {
            get
            {
                string transitionAnimatorProviderGUID = AssetDatabaseExtensions.AssetGUID(transitionAnimatorProvider);
                return transitionAnimatorProviderGUID;
            }
        }

        public void Create(string directory, string assetName, string scriptName, string viewName)
        {
            if (CanCreate(out Error error) == false)
            {
                error.LogWithPrefix("Cannot create view controller.");
                return;
            }

            var scriptPath = CreateScriptAsset(directory, scriptName);
            string scriptGUID = AssetDatabase.AssetPathToGUID(scriptPath);
            string transitionAnimatorProviderGUID = DefaultTransitionAnimatorProviderGUID;
            string viewResourceGUID = ViewResourceScriptGUID;
            var viewResourceYAMLConfig = CreateViewAssetAndGenerateViewResourceYAMLConfig(directory, viewName);
            CreateViewControllerAsset(directory, assetName, scriptGUID, transitionAnimatorProviderGUID, viewResourceGUID, viewResourceYAMLConfig);
            AssetDatabase.SaveAssets();

            // The view controller script change observer won't pick up this script immediately as it hasn't been compiled into the current assembly. Therefore, add it manually to the processor queue to be processed once scripts have reloaded. This will ensure that the view's owner data is set automatically.
            var processorQueue = ViewControllerScriptChangeViewBindingsProcessorQueue.Instance;
            processorQueue.EnqueueScriptForProcessing(scriptPath);
        }

        protected virtual bool CanCreate(out Error error)
        {
            if (scriptTemplate == null)
            {
                error = new Error("Specify a script template in the project settings.");
                return false;
            }

            if (string.IsNullOrEmpty(DefaultTransitionAnimatorProviderGUID))
            {
                error = new Error("Specify a default transition animator provider in the project settings.");
                return false;
            }

            error = null;
            return true;
        }

        protected abstract string CreateViewAssetAndGenerateViewResourceYAMLConfig(string viewDirectory, string viewName);

        private string CreateScriptAsset(string directory, string scriptName)
        {
            string scriptPath = Path.ChangeExtension(Path.Combine(directory, scriptName), "cs");
            string scriptContents = GenerateScript(scriptName);
            File.WriteAllText(scriptPath, scriptContents);
            AssetDatabase.ImportAsset(scriptPath, ImportAssetOptions.ForceSynchronousImport);
            return scriptPath;
        }

        private string GenerateScript(string scriptName)
        {
            string scriptContents = scriptTemplate.text;
            scriptContents = scriptContents.Replace(ClassNameScriptTemplateSymbol, scriptName);
            return scriptContents;
        }

        private void CreateViewControllerAsset(string directory, string assetName, string scriptGUID, string transitionAnimatorProviderGUID, string viewResourceGUID, string viewResourceYAMLConfig)
        {
            string assetPath = Path.ChangeExtension(Path.Combine(directory, assetName), "asset");
            string assetContents = GenerateViewControllerAssetContents(assetName, scriptGUID, transitionAnimatorProviderGUID, viewResourceGUID, viewResourceYAMLConfig);
            File.WriteAllText(assetPath, assetContents);
            AssetDatabase.ImportAsset(assetPath, ImportAssetOptions.ForceSynchronousImport);
        }

        private string GenerateViewControllerAssetContents(string assetName, string scriptGUID, string transitionAnimatorProviderGUID, string viewResourceGUID, string viewResourceYAMLConfig)
        {
            var assetTemplate = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<TextAsset>(ViewControllerAssetTemplateGUID);
            string assetContents = assetTemplate.text;

            var stringBuilder = new StringBuilder(assetContents);
            stringBuilder.Replace(ScriptGUIDAssetTemplateSymbol, scriptGUID);
            stringBuilder.Replace(AssetNameAssetTemplateSymbol, assetName);
            stringBuilder.Replace(TransitionAnimatorProviderGUIDAssetTemplateSymbol, transitionAnimatorProviderGUID);
            stringBuilder.Replace(ViewResourceGUIDAssetTemplateSymbol, viewResourceGUID);
            stringBuilder.Replace(ViewResourceYAMLConfigTemplateSymbol, viewResourceYAMLConfig);

            return stringBuilder.ToString();
        }
    }
}
﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public class ViewResourceOpenAssetHandler
    {
        [OnOpenAsset]
        private static bool OpenAssetHandler(int instanceID, int line)
        {
            Object objectToOpen = EditorUtility.InstanceIDToObject(instanceID);
            if (objectToOpen is IViewResource viewResource)
            {
                viewResource.OpenView();
                return true;
            }

            return false;
        }
    }
}
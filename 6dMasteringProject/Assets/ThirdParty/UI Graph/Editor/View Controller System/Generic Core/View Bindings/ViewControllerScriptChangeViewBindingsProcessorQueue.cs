﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public class ViewControllerScriptChangeViewBindingsProcessorQueue : ScriptableObject, ISerializationCallbackReceiver
    {
        private static ViewControllerScriptChangeViewBindingsProcessorQueue instance;

        private Queue<string> queue = new Queue<string>();
        [SerializeField] private List<string> serializedQueue;

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            serializedQueue = new List<string>(queue);
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            queue = new Queue<string>(serializedQueue);
        }

        public int Count
        {
            get
            {
                return queue.Count;
            }
        }

        public static ViewControllerScriptChangeViewBindingsProcessorQueue Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = AssetDatabaseExtensions.LoadFirstAssetOfType<ViewControllerScriptChangeViewBindingsProcessorQueue>();
                }

                return instance;
            }
        }

        public void EnqueueScriptForProcessing(string path)
        {
            if (queue.Contains(path) == false)
            {
                queue.Enqueue(path);
            }
        }

        public string DequeueScriptForProcessing()
        {
            return queue.Dequeue();
        }

        public void Clear()
        {
            queue.Clear();
        }
    }
}
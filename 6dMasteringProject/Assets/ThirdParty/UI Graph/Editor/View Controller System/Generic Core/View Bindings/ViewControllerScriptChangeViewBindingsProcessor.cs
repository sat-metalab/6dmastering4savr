﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public abstract class ViewControllerScriptChangeViewBindingsProcessor<
        TViewController,
        TView,
        TWindow,
        TViewResource,
        TTransition,
        TTransitionContext,
        TTransitionAnimatorProvider,
        TTransitionProgressProvider,
        TTransitionAnimationDriver,
        TTransitionAnimationDefaultProgressProvider,
        TTransitionData,
        TGraph> : IViewControllerScriptChangeViewBindingsProcessor
        where TViewController : ViewController<
            TViewController,
            TView,
            TWindow,
            TViewResource,
            TTransition,
            TTransitionContext,
            TTransitionAnimatorProvider,
            TTransitionProgressProvider,
            TTransitionAnimationDriver,
            TTransitionAnimationDefaultProgressProvider,
            TTransitionData,
            TGraph>
        where TView : IView<TView, TWindow>
        where TWindow : TView
        where TViewResource : ViewResource<TView, TWindow>
        where TTransition : ViewControllerTransition<TViewController, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider>, new()
        where TTransitionContext : ViewControllerTransitionContext<TViewController>
        where TTransitionAnimatorProvider : class, IViewControllerTransitionAnimatorProvider<TTransitionContext>
        where TTransitionProgressProvider : class, IViewControllerTransitionProgressProvider<TTransitionContext>
        where TTransitionAnimationDriver : ViewControllerTransitionAnimationDriver<TTransitionContext, TTransitionAnimationDefaultProgressProvider>, new()
        where TTransitionAnimationDefaultProgressProvider : class, IViewControllerTransitionProgressProvider<TTransitionContext>, new()
        where TTransitionData : ViewControllerInvokeTransitionData<TViewController>, new()
        where TGraph : class
    {
        void IViewControllerScriptChangeViewBindingsProcessor.ProcessViewControllerScript(string viewControllerScriptPath)
        {
            if (TryLoadViewControllerScriptAtPath(viewControllerScriptPath, out MonoScript viewControllerScript, out System.Type viewControllerType))
            {
                Object[] viewControllerAssets = AssetDatabaseExtensions.LoadAllAssetsOfType(viewControllerType);
                if (viewControllerAssets.Length == 0)
                {
                    // [Developer Note] When a user changes the namespace of a script, the AssetDatabase cannot find any existing assets of that type until the assets are reimported. Here we assume that if a view controller script has been updated and we could not find any ScriptableObject instances of it, this scenario may have occurred. Therefore, we use FindObjectsOfTypeAll to find and re-import all view controller assets to resolve the issue. This is a known Unity issue (as of 2019.1) - https://issuetracker.unity3d.com/issues/cannot-find-scriptableobject-asset-by-search-in-project-window-when-namespace-of-corresponding-script-is-changed
                    FindAndReimportAllViewControllerAssets();
                    viewControllerAssets = Resources.FindObjectsOfTypeAll(viewControllerType);
                }

                foreach (var viewControllerAsset in viewControllerAssets)
                {
                    var viewController = viewControllerAsset as TViewController;
                    UpdateViewControllerBindings(viewController);
                }
            }
        }

        protected abstract void UpdateViewControllerBindings(TViewController viewController);

        private bool TryLoadViewControllerScriptAtPath(string path, out MonoScript viewControllerScript, out System.Type viewControllerType)
        {
            viewControllerScript = AssetDatabase.LoadAssetAtPath<MonoScript>(path);
            viewControllerType = viewControllerScript?.GetClass();
            if ((viewControllerScript == null) || (viewControllerType == null))
            {
                return false;
            }

            return viewControllerType.IsSubclassOf(typeof(TViewController));
        }

        private void FindAndReimportAllViewControllerAssets()
        {
            List<Object> viewControllerAssets = new List<Object>();
            viewControllerAssets.AddRange(Resources.FindObjectsOfTypeAll(typeof(CanvasController)));
            viewControllerAssets.AddRange(Resources.FindObjectsOfTypeAll(typeof(ElementsController)));

            AssetDatabase.StartAssetEditing();
            foreach (Object viewControllerAsset in viewControllerAssets)
            {
                string assetPath = AssetDatabase.GetAssetPath(viewControllerAsset);
                AssetDatabase.ImportAsset(assetPath, ImportAssetOptions.ForceSynchronousImport);
            }
            AssetDatabase.StopAssetEditing();
        }
    }
}
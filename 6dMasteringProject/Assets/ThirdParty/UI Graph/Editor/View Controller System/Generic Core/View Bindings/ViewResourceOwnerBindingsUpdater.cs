﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public abstract class ViewResourceOwnerBindingsUpdater<TViewResource, TView, TWindow>
        where TViewResource : ViewResource<TView, TWindow>
        where TView : IView<TView, TWindow>
        where TWindow : TView
    {
        protected TViewResource viewResource;

        public ViewResourceOwnerBindingsUpdater(TViewResource viewResource)
        {
            this.viewResource = viewResource;
        }

        public abstract void UpdateBindingsWithOwner(Object owner);
    }
}
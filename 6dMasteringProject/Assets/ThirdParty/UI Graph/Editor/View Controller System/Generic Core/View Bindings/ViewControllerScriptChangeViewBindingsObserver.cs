﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEditor;

namespace Pelican7.UIGraph.Editor
{
    public class ViewControllerScriptChangeViewBindingsObserver : AssetPostprocessor
    {
        protected virtual void OnViewControllerScriptChanged(string viewControllerScriptPath) { }

        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            ProcessImportedAssets(importedAssets);
        }

        private static void ProcessImportedAssets(string[] importedAssets)
        {
            List<string> viewControllerScripts = new List<string>();
            foreach (string importedAssetPath in importedAssets)
            {
                if (AssetAtPathIsViewControllerScript(importedAssetPath))
                {
                    viewControllerScripts.Add(importedAssetPath);
                }
            }

            AddViewControllerScriptsToProcessingQueueIfNecessary(viewControllerScripts);
        }

        private static bool AssetAtPathIsViewControllerScript(string path)
        {
            var script = AssetDatabase.LoadAssetAtPath<MonoScript>(path);
            if (script == null)
            {
                return false;
            }

            var scriptType = script.GetClass();
            if (scriptType == null)
            {
                // [Developer Note] GetClass will return null the first time a script's namespace is changed (as of 2019.1). Therefore, bindings will not be processed on this change.
                return false;
            }

            return (scriptType.IsSubclassOf(typeof(CanvasController)) || scriptType.IsSubclassOf(typeof(ElementsController)));
        }

        private static void AddViewControllerScriptsToProcessingQueueIfNecessary(List<string> viewControllerScripts)
        {
            if (viewControllerScripts.Count > 0)
            {
                var processorQueue = ViewControllerScriptChangeViewBindingsProcessorQueue.Instance;
                foreach (var viewControllerScript in viewControllerScripts)
                {
                    processorQueue.EnqueueScriptForProcessing(viewControllerScript);
                }
            }
        }
    }
}
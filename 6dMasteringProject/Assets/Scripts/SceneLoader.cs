﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    private bool loadScene = false;
    static bool singletonLoaded = false;


    public int sceneCount;
    public  int currentScene = 0;   // this will increment to 0 when this object is instantiated.


//    [SerializeField]
//    private int scene;
    [SerializeField]
    private Text loadingText;

    void Awake() {

        if (singletonLoaded) 
        {
            Debug.LogWarning(this.GetType()+": instance of GameRoot already loaded, aborting");
            Destroy(this);
            return;
        }
        singletonLoaded = true;
        DontDestroyOnLoad(transform.gameObject);
    } 

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void Start()
    {
        sceneCount = SceneManager.sceneCountInBuildSettings;   // don't count the gameRoot scene
        loadingText.text = "";

    }


    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Next Level Loaded");
        Debug.Log(scene.name);
        Debug.Log(mode);
        loadScene = false;
        loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, 1f);
        loadingText.text = "";
//        loadingText.text = "hit spacebar to load next scene";
        currentScene = 1 + ((currentScene + 1) % (sceneCount-1)); 
    }




    // Updates once per frame
    void Update() {

        // If the player has pressed the space bar and a new scene is not loading yet...
        if (Input.GetKeyUp(KeyCode.Space) && loadScene == false) {

            // ...set the loadScene boolean to true to prevent loading a new scene more than once...
            loadScene = true;

            // ...change the instruction text to read "Loading..."
            loadingText.text = "Loading scene: "+currentScene+".....";

            // ...and start a coroutine that will load the desired scene.
            StartCoroutine(LoadNewScene());

        }

        // If the new scene has started loading...
        if (loadScene == true) {

            // ...then pulse the transparency of the loading text to let the player know that the computer is still working.
            loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));

        }

    }


    // The coroutine runs on its own at the same time as Update() and takes an integer indicating which scene to load.
    IEnumerator LoadNewScene() {

        // This line waits for 3 seconds before executing the next line in the coroutine.
        // This line is only necessary for this demo. The scenes are so simple that they load too fast to read the "Loading..." text.
        yield return new WaitForSeconds(1);

        // Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
		AsyncOperation async = SceneManager.LoadSceneAsync(currentScene);   

//        // While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
//        while (!async.isDone) {
//            yield return null;
//        }

    }

}
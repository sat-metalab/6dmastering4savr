namespace Oz.Common
{
    /// <summary>
    /// Regular Singleton class. This one is not specific to Unity or MonoBehaviours
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericSingleton<T>
    {
        private static T instance;
        public static T Instance
        {
            get { return instance; }
            private set { instance = value; }
        }
    }
}
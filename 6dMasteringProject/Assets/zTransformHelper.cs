﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class zTransformHelper : MonoBehaviour
{

    public enum PosMapping { direct, offset }

    [Header("")]
    public bool Xenable = true;
    public float xscale = 1;
    public PosMapping Xmapping = PosMapping.direct;
    private float _xpos;

    [Header("")]
    public bool Yenable = true;
    public float yscale = 1;
    public PosMapping Ymapping = PosMapping.direct;
    private float _ypos;

    [Header("")]
    public bool Zenable = true;
    public float zscale = 1;
    public PosMapping Zmapping = PosMapping.direct;
    private float _zpos;

    private Vector3 currentPos = new Vector3();
    private Vector3 lastPos = new Vector3();

    public void OnEnable()
    {
        _xpos = transform.localPosition.x;
        _ypos = transform.localPosition.y;
        _zpos = transform.localPosition.z;
        lastPos = Vector3.one;

    }

    // expects one data:  y
    public void SetPositionY(float height)
    {
        if (!Yenable) return;

        print("SetPositionY: " + height);
        Vector3 newPos = transform.localPosition;

        float y = height;

        if (Yenable)
        {
            newPos.y = (Ymapping == PosMapping.direct) ? yscale * y : _ypos + yscale * y;
        }
        transform.localPosition = newPos;
    }

    // expects two data:  x z
    public void SetPositionXZ(Vector2 xz)
    {

        print("SetPositionXZ: " + xz);
        Vector3 newPos = transform.localPosition;
        float x = xz.x;
        float z = xz.y;

        if (Xenable)
        {
            newPos.x = (Xmapping == PosMapping.direct) ? xscale * x : _xpos + xscale * x;
        }
        if (Zenable)
        {
            newPos.z = (Zmapping == PosMapping.direct) ? zscale * z : _zpos + zscale * x;
        }
        transform.localPosition = newPos;
    }

    // expects three data:  x y z
    public void SetPosition(Vector3 pos)
    {
        Vector3 newPos = transform.localPosition;
        float x = pos.x;
        float y = pos.y;
        float z = pos.z;

        if (Xenable)
        {
            newPos.x = (Xmapping == PosMapping.direct) ? xscale * x : _xpos + xscale * x;
        }
        if (Yenable)
        {
            newPos.y = (Ymapping == PosMapping.direct) ? yscale * y : _ypos + yscale * y;
        }
        if (Zenable)
        {
            newPos.z = (Zmapping == PosMapping.direct) ? zscale * z : _zpos + zscale * x;
        }
        transform.localPosition = newPos;
    }
}

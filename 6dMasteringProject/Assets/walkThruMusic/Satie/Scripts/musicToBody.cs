﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicToBody : MonoBehaviour
{

    public float efolBoost = 5f;    // boost or cut scaler on input envelope

    [Tooltip("raise unit envelope to power: val < 1 == boost small values, val > 1 == compress small values")]
    public float envExp = 1f;

    public Vector3 efolXYZScaler = new Vector3(0.2f,1,0.2f);  // scale x y z by unit value to control shape

    public MeshFilter bodyMesh;

    private GameObject primitiveShape = null;
    private Vector3 primitiveShapeScale;
    public Color _myColor = Color.red;

    private bool initialized = false;


    // Start is called before the first frame update
    void Start()
    {

        if (!bodyMesh)
        {
            Debug.LogError(GetType() + ".Start()" + " " + name + ":  meshFilter field empty, aborting");
            Destroy(this);
        }

        primitiveShape = bodyMesh.gameObject;

        primitiveShapeScale = primitiveShape.transform.localScale;

        initialized = true;
    }


    // called by events
    // called by color event, possibly before this class' start() has been called. So care is taken to set state, and possibly set renderer state too.
    public void setColor(Color color)
    {
        // events can call non-active objects, so this is necessary.
        if (!gameObject.activeInHierarchy) return;

        _myColor = color;
        if (primitiveShape)  
        {
            Renderer renderer = primitiveShape.GetComponent<Renderer>();
            renderer.material.color = _myColor;
        }
        else
        {
            Debug.LogError(name+" "+GetType() + ".setColor()  :  Primitive Shape not defined, color event received too early, color not set to renderer");
        }
    }

    // called by events
    // should be unit scale amplitude
    public void scaleSize(float amplitude)
    {
        // events can call non-active objects, so this is necessary.
        if (!gameObject.activeInHierarchy) return;

        float adjustedScaler = Mathf.Pow(amplitude, envExp);  // compress or expand low values

        float gain = adjustedScaler * efolBoost;

        Vector3 scaleOffset = efolXYZScaler * gain;  // scale offset by desired x y z shape scalers

        primitiveShape.transform.localScale = primitiveShapeScale + scaleOffset;  
    }

}

/*************************************************************************
*
* Experiences Musique6D
* __________________
*
*  [2019] Experiences Musique6D Incorporated
*  All Rights Reserved.
*
* NOTICE:  All Original Software And Resources developed by Experiences Musique6D contained herein is, and remains
* the property of Experiences Musique6D Incorporated.  The intellectual and technical concepts contained
* herein are proprietary to Experiences Musique6D Incorporated
* and may be covered by U.S. and Canadian Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Experiences Musique6D Incorporated.
*/





using UnityEngine;
using System.Collections;

public class zFaceCamera : MonoBehaviour
{
    public Camera camera;

    void Start()
    {
        if (camera == null) camera = Camera.main;
    }

    void Update()
    {
        Transform tr = transform;

        tr.LookAt(transform.position + camera.transform.rotation * Vector3.forward,
            camera.transform.rotation * Vector3.left);


        Vector3 eulerAngles = tr.rotation.eulerAngles;
        eulerAngles = new Vector3(0, eulerAngles.y, 90);
//        eulerAngles = new Vector3(eulerAngles.x, eulerAngles.x, eulerAngles.z);
        transform.rotation = Quaternion. Euler(eulerAngles);
    }
}
﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(ParticleSystem))]

public class musicToParticles : MonoBehaviour
{
    [Tooltip("applied to envelope before exponential scaling")]
    public float envBoost = 1f;
    [Tooltip("raise unit envelope to power: val < 1 == boost small values, val > 1 == compress small values")]
    public float envExp = 1f;

    private ParticleSystem.MainModule main;
    private ParticleSystem.EmissionModule emission;
    

    private float _initialSize;
    private float _initialRate;




    public bool debug = false;



    //rivate bdotsPlayerManagerLite playerComponent;

    // Start is called before the first frame update
    void Awake()
    {

        ParticleSystem part = gameObject.GetComponent<ParticleSystem>();

        if (!part)
        {
            Debug.LogError(name + " " + GetType() + "  <Color=Red><b>Missing Particle component</b></Color> , aborting");
            Destroy(this);
            return;
        }

        main = gameObject.GetComponent<ParticleSystem>().main;
        _initialSize = main.startSize.constant;

        emission = gameObject.GetComponent<ParticleSystem>().emission;
        _initialRate = emission.rateOverTime.constant;

        scaleSize(0f);
        scaleRate(0f);
    }


    public void setColor(Color color)
    {
        // events can call non-active objects, so this is necessary.
        if (!gameObject.activeInHierarchy) return;

        var emitterMain = main;
        emitterMain.startColor = color;
    }


    // scaler range
    // 0 to 1
    public void scaleRate(float scaler)
    {
        // events can call non-active objects, so this is necessary.
        if (!gameObject.activeInHierarchy) return;

        var newEmission = emission;
        scaler = Mathf.Clamp01(scaler * envBoost);

        float adjustedScaler = Mathf.Pow(scaler, envExp);  // compress or expand low values

        newEmission.rateOverTime = adjustedScaler * _initialRate;
    }

    // scaler range
    // 0 to 1
    public void scaleSize(float scaler)
    {
        // events can call non-active objects, so this is necessary.
        if (!gameObject.activeInHierarchy) return;

        var newMain = main;
        scaler = Mathf.Clamp01(scaler * envBoost);

        float adjustedScaler = Mathf.Pow(scaler, envExp);  // compress or expand low values

        newMain.startSize = adjustedScaler * _initialSize;
    }
}

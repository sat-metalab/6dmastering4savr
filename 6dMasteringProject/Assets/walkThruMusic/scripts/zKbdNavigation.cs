﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zKbdNavigation : MonoBehaviour
{
    public enum mode{byTranslation, byHeading };

    public mode navigationMode = mode.byHeading;
    public float speed = 1f;
    //public float height = 1.4f; // usual AR height


    // Use this for initialization
    void Start()
    {
        //Vector3 pos = Vector3.zero;
        //pos.y = height;
        //transform.position = pos;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (navigationMode == mode.byHeading)
        {
            var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f * speed;
            var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f * speed;

            transform.Rotate(0, x, 0);
            transform.Translate(0, 0, z);
        }
        else
        {
            var x = Input.GetAxis("Horizontal") * Time.deltaTime * 3.0f * speed;
            var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f * speed;
            transform.Translate(x, 0, z);
        }
    }
}

This work provides the implementation of a 6DoF navigation in the [SAV+R Audio Dataset](https://archive.org/details/savr_audio_dataset), as described in the following paper:

```
@inproceedings{settel2021,
  TITLE = {{Building Navigable Listening Experiences Based on Spatial Soundfield Capture: the case of the Orchestre Symphonique de Montréal playing Beethoven's Symphony No. 6}},
  AUTHOR = {Zack Settel and Jean-Yves Munch and Gabriel Downs and Nicolas Bouillot},
  BOOKTITLE = {{18th Sound and Music Computing Conference (SMC)}},
  ADDRESS = {Online},
  YEAR = {2021},
  MONTH = Jun
}

```

# 6dmastering4savr

This project requires GIT LFS  https://git-lfs.github.com/

Download and install the Git command line extension.

For MAC OS

```bash
brew install git-lfs
```

Then, set up Git LFS and its respective hooks by running:

`git lfs install`

(You'll need to run this in your repository directory, once per repository)

and possibly do `git lfs pull` after cloning the repo

Select the file types you'd like Git LFS to manage (or directly edit your .gitattributes). You can configure additional file extensions at anytime.

`git lfs track "*.psd"`


Make sure .gitattributes is tracked

`git add .gitattributes`

There is no step three. Just commit and push to GitHub as you normally would.


Make sure .gitattributes is tracked

`git add .gitattributes`
There is no step three. Just commit and push to GitHub as you normally would.

```bash
git add file.psd
git commit -m "Add design file"
git push origin master
```
